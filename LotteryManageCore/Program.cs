﻿using System;
using System.Configuration;
using System.IO;
using Lottery.DataStorage.Data;
using Lottery.DataStorage.Repositories.ExcludedFromTheLotteryUsersRepository;
using Lottery.DataStorage.Repositories.LotteryParticipantsRepository;
using Lottery.DataStorage.Repositories.LotteryRepository;
using Lottery.DataStorage.Repositories.LotteryWinerRepository;
using Lottery.DataStorage.Repositories.PictureRepository;
using Lottery.DataStorage.Repositories.PrizeRepository;
using Lottery.DataStorage.Repositories.QuizRepository;
using Lottery.DataStorage.Repositories.UserRepository;
using Lottery.DataStorage.Services;
using Lottery.Manage.services;
using Lottery.Manage.services.InfoCollector;
using Lottery.Manage.services.LotteryManage;
using Lottery.Manage.services.LotteryQuiz;
using Lottery.Manage.services.Prize;
using Lottery.Manage.services.UserController;
using Lottery.Models.Models.DataStorageModels.Roles;
using Lottery.Models.Models.DataStorageModels.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.Azure.WebJobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LotteryManageCore
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            var configuration = new JobHostConfiguration();
            configuration.Queues.MaxPollingInterval = TimeSpan.FromSeconds(10);
            configuration.Queues.BatchSize = 1;
            configuration.JobActivator = new LotteryJobActivator(serviceCollection.BuildServiceProvider());
            configuration.UseTimers();
            var host = new JobHost(configuration);
            host.RunAndBlock();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();
            services.AddScoped<JobMethods, JobMethods>();

            services.AddDbContext<LotteryDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
            services.AddTransient<UserControllerService, UserControllerService>();

            //AutoMapperConfiguration.Configure();

            services.AddIdentity<ApplicationUser, UserRole>(options =>
                {
                    options.Lockout.AllowedForNewUsers = true;
                    options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                    options.Lockout.MaxFailedAccessAttempts = 5;
                    options.User.RequireUniqueEmail = true;
                    options.Tokens.ProviderMap.Add("EmailTwoFactor", new TokenProviderDescriptor(typeof(IUserTwoFactorTokenProvider<ApplicationUser>)));
                }).AddEntityFrameworkStores<LotteryDbContext>()
                .AddDefaultTokenProviders();
            // Add application services.
            services.AddScoped<SignInManager<ApplicationUser>>();
            services.AddScoped<UserManager<ApplicationUser>>();
            services.AddScoped<RoleManager<UserRole>>();
            services.AddScoped<UserControllerService>();
            services.AddScoped<ILotteryManageService,LotteryManageService>();
            services.AddScoped<ILotteryQuizService, LotteryQuizService>();
            services.AddScoped<IDataStorageService, DataStorageService>();
            services.AddScoped<IUserStorageService, UserStorageService>();
            services.AddScoped<ILotteryPrizeService, LotteryPrizeService>();
            services.AddScoped<EmailSender>();
            services.AddScoped<IUserTwoFactorTokenProvider<ApplicationUser>,DataProtectorTokenProvider<ApplicationUser>>();

            services.AddScoped<ILotteryRepository, LotteryRepository>();
            services.AddScoped<ILotteryWinerRepository, LotterWinnerRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
         
            services.AddScoped<ILotteryParticipantsRepository, LotteryParticipantsRepository>();
            services.AddScoped<IQuizRepository, QuizRepository>();
            services.AddScoped<IExcludedFromTheLotteryUsersRepository, ExcludedFromTheLotteryUsersRepository>();
            services.AddScoped<IPictureRepository, PictureRepository>();
            services.AddScoped<IPrizeRepository, PrizeRepository>();
            services.AddScoped<ILotteryInfoProviderService, LotteryInfoProviderService>();

            Environment.SetEnvironmentVariable("AzureWebJobsDashboard", configuration.GetConnectionString("AzureWebJobsDashboard"));
            Environment.SetEnvironmentVariable("AzureWebJobsStorage", configuration.GetConnectionString("AzureWebJobsStorage"));
        }
    }
}
