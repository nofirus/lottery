﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.DependencyInjection;

namespace LotteryManageCore
{
    public class LotteryJobActivator:IJobActivator
    {
        private readonly IServiceProvider _service;
        public LotteryJobActivator(IServiceProvider service)
        {
            _service = service;
        }
        public T CreateInstance<T>()
        {
            var service = _service.GetService<T>();
            return service;
        }
    }
}
