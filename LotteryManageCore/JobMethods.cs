﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Lottery.Manage.services.InfoCollector;
using Lottery.Manage.services.LotteryManage;

using Lottery.Manage.services.LotteryQuiz;
using Lottery.Manage.services.Prize;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.Prize;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Remotion.Linq.Clauses;
using Enumerable = System.Linq.Enumerable;

namespace LotteryManageCore
{
    public class JobMethods
    {
        private readonly ILotteryManageService _lotteryManageService;
        private readonly ILotteryInfoProviderService _lotteryInfoProviderService;
        private readonly ILotteryPrizeService _lotteryPrizeService;
        private string UserName { get; set; }
        private string Password { get; set; }
        private string MailTo { get; set; }
        private string TestUserEmail { get; set; }

        public JobMethods(ILotteryManageService lotteryManageService, ILotteryInfoProviderService lotteryInfoProviderService, ILotteryPrizeService lotteryPrizeService)
        {
            _lotteryManageService = lotteryManageService;
            _lotteryInfoProviderService = lotteryInfoProviderService;
            _lotteryPrizeService = lotteryPrizeService;

            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();
            UserName = configuration["UserName"];
            Password = configuration["Password"];
            MailTo = configuration["MailTo"];
            TestUserEmail = configuration["TestUserEmail"];
        }

        public async Task RandomaizeLottery([TimerTrigger("*/30 * * * * *")]TimerInfo timer, TextWriter log)
        {
            var lottery =  await _lotteryManageService.LotteryRandomazer();
            if (lottery.Description?.Length > 0)
            {
                var message = await _lotteryInfoProviderService.CollectInfoAboutLottery(lottery);
                SendEmail(message, MailTo);
                await SendResultsToLotteryParticipants(lottery);
            }
        }

       public async Task MonthLotteryStatistic([TimerTrigger("0 59 23 1 */1 *")]TimerInfo timer)
        {
            var message = await _lotteryInfoProviderService.CollescInfoAboutLotteryForLastMonth();
            SendEmail(message, MailTo);
        }

        public async Task SendResultsToLotteryParticipants(LotteryEntity lottery)
        {
            var lotteryWiners = await _lotteryInfoProviderService.GetLotteryWinners(lottery);
            var message = new StringBuilder();
            var lotteryParticipants = await _lotteryInfoProviderService.GetLotteryParticipants(lottery);
            var lotteryPrizies = await _lotteryPrizeService.GetLotteryPrizies(lottery.Id);
            for (var i = 0; i < lotteryParticipants.Count; i++)
            {
                if (lotteryWiners.Select(x => x.ApplicationUser).Contains(lotteryParticipants.ElementAt(i).Participants))
                {

                    var lotteryDescription = lottery.Description;
                    var place = lotteryWiners.ElementAt(0).Place;
                    var prize = new LotteryPrize();
                    if (lotteryPrizies.Any())
                    {
                        prize = lotteryPrizies.ElementAt(place - 1);
                    }
                    message.Append(string.Format("you place in lottery: {0} is {1} you prize is {2}",
                        lotteryDescription, place,
                        prize.Description));
                    lotteryWiners.RemoveAt(0);
                    //lotteryParticipants.ElementAt(i).ApplicationUser.Email <- user this instead of TestUserEmail if emails is real
                    SendEmail(message.ToString(), TestUserEmail);
                    message.Clear();
                }
                else
                {
                    message.Append(string.Format("sorry you are not a winner in the lottery {0}", lottery.Description));
                    //lotteryParticipants.ElementAt(i).ApplicationUser.Email <- user this instead of TestUserEmail if emails is real
                    SendEmail(message.ToString(),TestUserEmail);
                    message.Clear();
                }
            }

        }

        private void SendEmail(string text, string mailTo)
        {
            MailMessage message = new MailMessage();
            message.To.Add(new MailAddress(mailTo, "Pavel"));
            message.From = new MailAddress(UserName, "ssonic1");
            message.Subject = "Azure Web App Email";
            message.Body = text;
            message.IsBodyHtml = true;
            SmtpClient client = new SmtpClient
            {
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential(UserName, Password),
                Port = 587,
                Host = "smtp.gmail.com",
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = true
            };
            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }
    }
}
