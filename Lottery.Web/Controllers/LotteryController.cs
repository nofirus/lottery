﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Lottery.Manage.services.InfoCollector;
using Lottery.Manage.services.LotteryManage;
using Lottery.Manage.services.Prize;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryParticipants;
using Lottery.Models.Models.DataStorageModels.LotteryWiner;
using Lottery.Models.Models.DataStorageModels.Prize;
using Lottery.Models.Models.LotteryViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Lottery.Web.Controllers
{
    [Authorize]
    public class LotteryController : Controller
    {
        private readonly ILotteryManageService _lotteryManageService;
        private readonly ILotteryInfoProviderService _lotteryInfoProviderService;
        private readonly ILotteryPrizeService _lotteryPrizeService;
        private static readonly int pageSize = 10;

        public LotteryController(ILotteryManageService lotteryManageService, ILotteryInfoProviderService lotteryInfoProviderService, ILotteryPrizeService lotteryPrizeService)
        {
            _lotteryManageService = lotteryManageService;
            _lotteryInfoProviderService = lotteryInfoProviderService;
            _lotteryPrizeService = lotteryPrizeService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> MainPage()
        {
            var futureLotteries = await _lotteryInfoProviderService.GetFutureLotteries(); 
            var paginatedList = new PaginatedList<LotteryEntity>(futureLotteries.AsQueryable(),futureLotteries.Count(),1,pageSize);
            var closestLottery = await _lotteryInfoProviderService.GetClosestLotteryForUser(User);
            var lastLottery = await _lotteryInfoProviderService.GetLastLottery();
            var closestLotteryParticipants = closestLottery == null ? new List<LotteryParticipants>(): await _lotteryInfoProviderService.GetLotteryParticipants(closestLottery.Id);
            var closestLotteryPartisipantsPaginated = new PaginatedList<LotteryParticipants>(closestLotteryParticipants.AsQueryable(), closestLotteryParticipants.Count, 1, pageSize);
            var model = new LotteryListViewModel(closestLottery, paginatedList, lastLottery, closestLotteryPartisipantsPaginated, StatusMessage) ;
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ClosestLottery()
        {
            var closestLottery = await _lotteryInfoProviderService.GetClosestLottery();
            return PartialView(closestLottery);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Index(int? page,int? size)
        {
            var futureLotteries = await _lotteryInfoProviderService.GetFutureLotteries();
            var closestLottery = await _lotteryInfoProviderService.GetClosestLotteryForUser(User);
            var lastLottery = await _lotteryInfoProviderService.GetLastLottery();
            var pageNumber = page ?? 1;
            var currentPageSize = size ?? pageSize;
            var closestLotteryParticipants = await _lotteryInfoProviderService.GetLotteryParticipants(closestLottery.Id);
            var closestLotteryPartisipantsPaginated = new PaginatedList<LotteryParticipants>(closestLotteryParticipants.AsQueryable(),closestLotteryParticipants.Count,1,closestLotteryParticipants.Count==0?1: closestLotteryParticipants.Count);
            var paginatedList = new PaginatedList<LotteryEntity>(futureLotteries.AsQueryable(), futureLotteries.Count(), pageNumber,currentPageSize);
            var model = new LotteryListViewModel(closestLottery, paginatedList, lastLottery,closestLotteryPartisipantsPaginated, StatusMessage);
            return PartialView(model);
        }

        [HttpGet]
        [Authorize(Roles = "User,Manager")]
        public async Task<IActionResult> LotteryResult(int lotteryId)
        {
            var lottery = await _lotteryInfoProviderService.GetLotteryByIdAsync(lotteryId);
            var isUserWinner = await _lotteryInfoProviderService.IsUserWinner(User, lottery);
            var place = await _lotteryInfoProviderService.GetWinnerPlace(User, lottery);
            var allParticipants = await _lotteryInfoProviderService.GetLotteryParticipants(lotteryId);
            var lotteryWinners = await _lotteryInfoProviderService.GetAllLotteryWinners(lotteryId);
            var prize = place==0 ? new LotteryPrize() : await _lotteryPrizeService.GetPrizeForPlace(lottery, place);
            var model = new LotteryResultViewModel(){Lottery = lottery,IsUserWinner = isUserWinner, Place = place ,Prize = prize,LotteryParticipants = allParticipants,LotteryWinners = lotteryWinners};
            return PartialView(model);
        }

        [HttpGet]
        [Authorize(Roles = "User,Manager")]
        public async Task<IActionResult> AllResults(LotteryFilters filter)
        {
            if (filter.Page == 0)
            {
                filter.Page = 1;
                filter.LeftDate = DateTime.MinValue;
                filter.RightDate = DateTime.Now;
                filter.PageSize = pageSize;
            }
            var heldLotteryList = await _lotteryInfoProviderService.GetAllHeldLotteries(filter);
            var pageNumber = filter.Page;
            var currentPageSize = filter.PageSize;
            var paginatedList = new PaginatedList<LotteryEntity>(heldLotteryList.OrderByDescending(x => x.LotteryDate).AsQueryable(), heldLotteryList.Count(), pageNumber,Math.Abs(currentPageSize));
            filter.PaginatedList = paginatedList;
            return PartialView(filter);
        }

        [HttpPost]
        [Authorize(Roles = "User,Manager")]
        public async Task<IActionResult> AddParticapants(int lotteryId)
        {
            if (! await _lotteryInfoProviderService.IsAlreadyRegisteredOnTheLottery(lotteryId, User) && ! await _lotteryInfoProviderService.IsUserExcludeFromLottery(User,lotteryId))
            {
                var validationResult = await _lotteryManageService.CheckUserForLottery(User, lotteryId);
                if (validationResult.Success)
                {
                    if (await _lotteryInfoProviderService.IsLotteryHasQuiz(lotteryId))
                        return RedirectToAction(nameof(QuizController.QuizForLottery), "Quiz",
                            new {lotteryId = lotteryId});
                    await _lotteryManageService.AddParticipants(User, lotteryId);
                    StatusMessage = "Success";
                    return RedirectToAction(nameof(Index));
                }
                StatusMessage = validationResult.Description;
                return RedirectToAction(nameof(Index));
            }
            StatusMessage = "you already registered on this lottery";
            return RedirectToAction(nameof(Index));

        }

        [HttpGet]
        [Authorize(Roles = "User,Manager")]
        public async Task<string> LastLotteryWinners()
        {
            var lastHeldLottery = await _lotteryInfoProviderService.GetLastHeldLottery();
            Thread.Sleep(lastHeldLottery.Delay*1000);
            return  JsonConvert.SerializeObject(_lotteryInfoProviderService.GetAllLotteryWinners(lastHeldLottery.Id).Result.Select(x=>string.Format("Place {0} is {1}",x.Place, x.ApplicationUser.UserName)));
        }
        [TempData]
        public string StatusMessage { get; set; }
    }
}