﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Lottery.Manage.services.InfoCollector;
using Lottery.Manage.services.LotteryManage;
using Lottery.Manage.services.LotteryQuiz;
using Lottery.Models.Models.DataStorageModels.LotteryQuiz;
using Lottery.Models.Models.LotteryViewModels;
using Lottery.Models.Models.QuestionViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Lottery.Web.Controllers
{
    public class QuestionController : Controller
    {
        private readonly ILotteryManageService _lotteryManageService;
        private readonly ILotteryInfoProviderService _lotteryInfoProviderService;
        private readonly ILotteryQuizService _lotteryQuizService;
        private static readonly int pageSize = 10;

        public QuestionController(ILotteryInfoProviderService lotteryInfoProviderService, ILotteryManageService lotteryManageService, ILotteryQuizService lotteryQuizService)
        {
            _lotteryInfoProviderService = lotteryInfoProviderService;
            _lotteryManageService = lotteryManageService;
            _lotteryQuizService = lotteryQuizService;
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> AllQuestion(int? page, int? size)
        {
            ViewBag.StatusMessage = StatusMessage;
            var question = await _lotteryQuizService.GetAllQuestions();
            var pageNumer = page ?? 1;
            var currentPageSize = size ?? pageSize;
            var paginatedList = new PaginatedList<Question>(question.AsQueryable(), question.Count,pageNumer,currentPageSize);
            return PartialView(paginatedList);
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> CreateQuestion(int? amountOfAnswers)
        {
            int amount = amountOfAnswers ?? 1;
            var model = new QuestionCreateViewModel() {AmountOfAnswers = amount };
            return PartialView(model);
        }


        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> CreateQuestion(QuestionCreateViewModel model)
        {
            if (!model.Answers.Any() && model.QuestionType.Equals(QuestionType.Close))
            {
                return PartialView(model);
            }
            var result = await _lotteryManageService.AddNewQuestion(model.Question, model.QuestionType, model.Answers.ToList());
            if (!result.Success)
            {
                StatusMessage = result.Description;
                return RedirectToAction(nameof(AllQuestion));
            }
            StatusMessage = "Question successfully added";
            return RedirectToAction(nameof(AllQuestion));
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> DeleteQuestion(int questionId)
        {
            var result = await _lotteryManageService.DeleteQuestion(questionId);
            if (!result.Success)
            {
                StatusMessage = result.Description;
                return RedirectToAction(nameof(AllQuestion));
            }

            StatusMessage = "question deleted";
            return RedirectToAction(nameof(AllQuestion));
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> UpdateQuestion(int questionId, int? AmountOfAnswers)
        {
            
            var question = await _lotteryQuizService.GetQuestionById(questionId);
            var model = new QuestionCreateViewModel();
            model.Question = question.Text;
            model.Answers = (await _lotteryQuizService.GetAllAnwserOnTheQuestion(question)).Select(x=>x.Text);
            model.AmountOfAnswers = AmountOfAnswers?? model.Answers.Count();
            model.Id = question.Id;
            return PartialView(model);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> UpdateQuestion(QuestionCreateViewModel model)
        {
            //var question = Mapper.Map<Question>(model);
            var question = new Question();
            question.Id = model.Id;
            question.Text = model.Question;
            var result = await _lotteryManageService.UpdateQuestion(question, model.Answers.ToList());
            if (!result.Success)
            {
                StatusMessage = result.Description;
                return RedirectToAction(nameof(AllQuestion));
            }
            
            StatusMessage = "question updated";
            return RedirectToAction(nameof(AllQuestion));
        }

        [TempData]
        public string StatusMessage { get; set; }
    }
}