﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Lottery.Manage.services.InfoCollector;
using Lottery.Manage.services.LotteryManage;
using Lottery.Manage.services.LotteryQuiz;
using Lottery.Models.Models.DataStorageModels.LotteryQuiz;
using Lottery.Models.Models.LotteryViewModels;
using Lottery.Models.Models.QuizViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Lottery.Web.Controllers
{
    public class QuizController : Controller
    {

        private readonly ILotteryQuizService _lotteryQuizService;
        private readonly ILotteryManageService _lotteryManageService;
        private readonly ILotteryInfoProviderService _lotteryInfoProviderService;
        private static readonly int pageSize = 10;

        public QuizController(ILotteryQuizService lotteryQuizService, ILotteryManageService lotteryManageService, ILotteryInfoProviderService lotteryInfoProviderService)
        {
            _lotteryQuizService = lotteryQuizService;
            _lotteryManageService = lotteryManageService;
            _lotteryInfoProviderService = lotteryInfoProviderService;
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> QuizForLottery(int lotteryId)
        {
            var lottery = await _lotteryInfoProviderService.GetLotteryByIdAsync(lotteryId);
            var quiz = await _lotteryQuizService.GetQuizForLottery(lottery);
            var quizQuestion = await _lotteryQuizService.GetAllQuestionForQuiz(quiz);
            var questionAnswers = await _lotteryQuizService.GetAnswersOnTheQuestion(quizQuestion);
            var model = new QuizViewModel() { Quiz = quiz, QuizQuestionses = quizQuestion, AnswersToQuestion = questionAnswers, LotteryId = lotteryId };
            return PartialView(model);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> QuizForLottery(QuizViewModel model)
        {
            if (!ModelState.IsValid) return PartialView(model);
            await _lotteryQuizService.AddAnswersOnTheQuiz(model.QuizId, User, model.Answers, model.QuizQuestionsesId,
                model.CustomAnswers);
            await _lotteryManageService.AddParticipants(User, model.LotteryId);
            StatusMessage = "Success";
            return RedirectToAction(nameof(LotteryController.Index),"Lottery");
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> AllQuizzes(int? page, int? size)
        {
            var pageNumber = page ?? 1;
            var currentPageSize = size ?? pageSize;
            var allQuizzes = await _lotteryQuizService.GetAllQuizzes();
            var paginatedList = new PaginatedList<Quiz>(allQuizzes.AsQueryable(), allQuizzes.Count, pageNumber, currentPageSize);
            ViewBag.StatusMessage = StatusMessage;
            return PartialView(paginatedList);
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> CreateQuiz(int? amountOfQuestions)
        {
            var allQuestins = await _lotteryQuizService.GetAllQuestions();
            var model = new CreateQuizViewModel {AmountOfQuestions = amountOfQuestions ?? 5,AllQuestions = allQuestins };
            return PartialView(model);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> CreateQuiz(CreateQuizViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(model);
            }
            await _lotteryQuizService.AddNewQuiz(model.Description,model.SelectedQuestion);
            StatusMessage = "Quiz created";
            return RedirectToAction(nameof(AllQuizzes));
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> UpdateQuiz(int quizId, int? amountOfQuestions)
        {
            var targetQuiz = await _lotteryQuizService.GetQuizById(quizId);
            var model = Mapper.Map < CreateQuizViewModel > (targetQuiz);
            var questionFowQuiz = await _lotteryQuizService.GetAllQuestionForQuiz(targetQuiz);
            var allQuestions = await _lotteryQuizService.GetAllQuestions();
            var allAnswers = allQuestions.Select(x => _lotteryQuizService.GetAllAnwserOnTheQuestion(x).Result);
            model.AllQuestions = allQuestions;
            model.AllAsnwers = allAnswers.ToList();
            model.SelectedQuestion = questionFowQuiz.Select(x => x.Id);
            model.AmountOfQuestions = amountOfQuestions ?? model.SelectedQuestion.Count();
            return PartialView(model);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> UpdateQuiz(CreateQuizViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(model);
            }

            var quiz = Mapper.Map<Quiz>(model);
            var result = await _lotteryQuizService.UpdateQuiz(quiz, model.SelectedQuestion);
            if (!result.Success)
            {
                StatusMessage = result.Description;
                return RedirectToAction(nameof(AllQuizzes));
            }
            StatusMessage = result.Description;
            return RedirectToAction(nameof(AllQuizzes));
        }


        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> DeleteQuiz(int quizId)
        {
            var result = await _lotteryQuizService.DeleteQuiz(quizId);
            if (!result.Success)
            {
                StatusMessage = result.Description;
                return RedirectToAction(nameof(AllQuizzes));
            }
            StatusMessage = result.Description;
            return RedirectToAction(nameof(AllQuizzes));
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<ActionResult> AddQuizToLottery(int lotteryId,int? page, int? size)
        {
            var pageNumber = page ?? 1;
            var currentPageSize = size ?? pageSize; 
            var allQuizzes = await _lotteryQuizService.GetAllQuizzes();
            var paginatedList = new PaginatedList<Quiz>(allQuizzes.AsQueryable(),allQuizzes.Count,pageNumber,currentPageSize);
            var model = new AddQuizToLotteryViewModel {LotteryId = lotteryId,PaginatedQuizList = paginatedList};
            return PartialView(model);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> AddQuizToLottery(int lotteryId, int quizId)
        {
            await _lotteryQuizService.AddQuizToLottery(lotteryId, quizId);
            return RedirectToAction(nameof(LotteryManageController.LotteryManage), "LotteryManage");
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> QuizInfo(int quizId, int page, int size)
        {
            var quiz = await _lotteryQuizService.GetQuizById(quizId);
            var model = Mapper.Map<QuizInfoViewModel>(quiz);
            model.Questions = await _lotteryQuizService.GetAllQuestionForQuiz(quiz);
            model.Lotteries = await _lotteryQuizService.GetAllLotteriesForQuiz(quiz);
            return PartialView(model);
        }

        [TempData]
        public string StatusMessage { get; set; }

    }
}