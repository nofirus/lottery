﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Lottery.Manage.services.InfoCollector;
using Lottery.Manage.services.LotteryManage;
using Lottery.Manage.services.Prize;
using Lottery.Models.Models;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.Types;
using Lottery.Models.Models.DataStorageModels.User;
using Lottery.Models.Models.LotteryViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Lottery.Web.Controllers
{
    public class LotteryManageController : Controller
    {
        private readonly ILotteryManageService _lotteryManageService;
        private readonly ILotteryInfoProviderService _lotteryInfoProviderService;
        private readonly ILotteryPrizeService _lotteryPrizeService;
        private static readonly int pageSize = 10;

        public LotteryManageController(ILotteryManageService lotteryManageService, ILotteryInfoProviderService lotteryInfoProviderService, ILotteryPrizeService lotteryPrizeService)
        {
            _lotteryManageService = lotteryManageService;
            _lotteryInfoProviderService = lotteryInfoProviderService;
            _lotteryPrizeService = lotteryPrizeService;
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> LotteryManage(LotteryFilters filter)
        {
            if (filter.Page == 0)
            {
                filter.Page = 1;
                filter.LeftDate = DateTime.MinValue;
                filter.RightDate = DateTime.MaxValue;
                filter.PageSize = pageSize;
            }
            var allLotteries = await _lotteryInfoProviderService.GetAllLotteries(filter);
            var paginatedList = new PaginatedList<LotteryEntity>(allLotteries.OrderByDescending(x=>x.LotteryDate).AsQueryable(),allLotteries.Count(),filter.Page, Math.Abs(filter.PageSize));
            filter.PaginatedList = paginatedList;
            ViewBag.StatusMessage = StatusMessage;
            return PartialView(filter);
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> CreateLottery()
        {
            var model = new LotteryCreatingViewModel();
            return PartialView(model);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> CreateLottery(LotteryCreatingViewModel model)
        {
            if (!ModelState.IsValid) return PartialView(model);
            switch (model.OutputType)
            {
                case OutputType.AllWinners when (model.Delay<0 || model.Delay>60):
                    AddErrors("wrong Delay");
                    return PartialView(model);
                case OutputType.IndividualWinners when model.Delay!=0:
                    AddErrors("Delay must be 0 if output type is ");
                    return PartialView(model);
            }

            var lottery = Mapper.Map<LotteryEntity>(model);
            var result = await _lotteryManageService.AddLotteryAsync(lottery);
            if (result.Succsess) return RedirectToAction(nameof(LotteryManage));
            model.StatusMessage = result.Message;
            return PartialView(model);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> DeleteLottery(int lotteryId)
        {
            await _lotteryManageService.DeleteLottery(lotteryId);
            StatusMessage = "Lottery Delited";
            return RedirectToAction(nameof(LotteryManage));
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> UpdateLottery(int lotteryId)
        {
            var lottery = await _lotteryInfoProviderService.GetLotteryByIdNoTracking(lotteryId);
            var model = Mapper.Map<LotteryCreatingViewModel>(lottery);
            return PartialView(model);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> UpdateLottery(LotteryCreatingViewModel model)
        {
            var lottery = Mapper.Map<LotteryEntity>(model);
            var result = await _lotteryManageService.UpdateLotteryAsync(lottery);
            if (!result.Success)
            {
                model.StatusMessage = result.Description;
                return PartialView(model);
            }
            StatusMessage = string.Format("lottery {0} was updated", model.Description + ":" + model.LotteryDate);
            return RedirectToAction(nameof(LotteryManage));
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> AddParticapntsToCloseLottery(int lotteryId, int? page, int? size)
        {
            var unregistredUsers = await _lotteryInfoProviderService.GetAllUserUnregistredOnLottery(lotteryId);
            var pageNumber = page ?? 1;
            var currentPageSize = size ?? pageSize;
            var paginatedUbregistredUsers = new PaginatedList<ApplicationUser>(unregistredUsers.AsQueryable(),unregistredUsers.Count,pageNumber,currentPageSize);
            var model = new AddParticipantsToCloseLotterycs(){LotteryId = lotteryId,UnregistredUsers = paginatedUbregistredUsers };

            return PartialView(model);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> AddParticapntsToCloseLottery(int lotteryId, string email, int? page, int? size)
        {
            await _lotteryManageService.AddParticipantsByEmail(lotteryId, email);
            return RedirectToAction(nameof(AddParticapntsToCloseLottery), new { lotteryId ,page,size});
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> RemoveParticipant(int lotteryId, int? page, int? size)
        {
            var participants = await _lotteryInfoProviderService.GetAllParticipantsForLotteryAsync(lotteryId);
            var excludedParticipants = await _lotteryInfoProviderService.GetExcludedUsersForLottery(lotteryId);
            var pageNumer = page ?? 1;
            var currentPageSize = size ?? pageSize;
            var model = new RemoveParticipantsViewModel()
            {
                LotteryId = lotteryId
                ,Participants = new PaginatedList<ApplicationUser>(participants.AsQueryable(),participants.Count,pageNumer,currentPageSize)
                ,ExcludedParticapnts = excludedParticipants,
                PageSize = currentPageSize,
                PageNumer = pageNumer
            };
            return PartialView(model);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> RemoveUserFromExcluded(int lotteryId, string email)
        {
            await _lotteryManageService.RemoveUserFromExcluded(lotteryId, email);
            return  RedirectToAction(nameof(RemoveParticipant), new {lotteryId=lotteryId});
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> RemoveParticipant(RemoveParticipantsViewModel model)
        {
            if (!ModelState.IsValid)
            {
                var participants = await _lotteryInfoProviderService.GetAllParticipantsForLotteryAsync(model.LotteryId);
                model.Participants = new PaginatedList<ApplicationUser>(participants.AsQueryable(), participants.Count, model.PageNumer, model.PageSize);
                return PartialView(model);
            }
            await _lotteryManageService.RemoveLotteryParticipantsByEmail(model.LotteryId, model.Email, model.RemoveReason);
            StatusMessage = "User removed";
            return RedirectToAction(nameof(RemoveParticipant), new { lotteryId = model.LotteryId });
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> SetLotteryPrizies(int lotteryId,int? amountOfPrizeGroups)
        {
            var currentGrouPrizes = await _lotteryPrizeService.GetGroupLotteryPrizies(lotteryId, out var leftGroupBorder, out var rightGroupBorder);
            var allPrizies = await _lotteryPrizeService.GetAllPrizeis();
            var lottery = await _lotteryInfoProviderService.GetLotteryByIdAsync(lotteryId);
            var model = new SetPrizesViewModel
            {
                CurrentGroupPrizeList = currentGrouPrizes,
                AllPrizies = allPrizies,
                AmountOfWinners = lottery.AmountOfWinners,
                LotteryId = lotteryId,
                LeftGroupBorder = leftGroupBorder.AsEnumerable(),
                RightGtoupBorder = rightGroupBorder.AsEnumerable(),
                AmountOfPrizeGroups = amountOfPrizeGroups ?? (lottery.AmountOfPrizeGroups == 0
                                          ? lottery.AmountOfWinners
                                          : lottery.AmountOfPrizeGroups)
            };
            if (model.AmountOfPrizeGroups > model.AmountOfWinners)
            {
                model.AmountOfPrizeGroups = model.AmountOfWinners;
                model.StatusMessage = "amount of prize groups must be less or equal amount of winners";
            }
            return PartialView(model);
        }
        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> SetLotteryPrizies(SetPrizesViewModel model)
        {
            if (!ModelState.IsValid || model.AmountOfPrizeGroups > model.AmountOfWinners)
            {
                model.StatusMessage = "amount of prize groups must be less or equal amount of winners";
                return PartialView(model);
            }
            var result = await _lotteryManageService.SetLotteryPriziesAsync(model);
            if (result.Success) return RedirectToAction(nameof(LotteryManage));
            ModelState.AddModelError(string.Empty, result.Description);
            model.CurrentGroupPrizeList = (await _lotteryPrizeService.GetGroupLotteryPrizies(model.LotteryId, out var leftBorderGroup, out var rightBorderGropu));
            model.AllPrizies = await _lotteryPrizeService.GetAllPrizeis();
            model.LeftGroupBorder = leftBorderGroup.AsEnumerable();
            model.RightGtoupBorder = rightBorderGropu.AsEnumerable();
            return PartialView(model);
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> SetLotteryImage(int lotteryId)
        {
            var picture = await _lotteryInfoProviderService.GetLotteryPicture(lotteryId);
            var allPictures = await _lotteryInfoProviderService.GetAllPicturesAsync();
            var model = new SetLotteryImageViewModel(){LotteryId = lotteryId,Picture = picture,AllPictures = allPictures};
            return PartialView(model);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> SetLotteryImage(SetLotteryImageViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(model);
            }
            await _lotteryManageService.SetLotteryImageAsync(model.LotteryId, model.PictureId);
            return RedirectToAction(nameof(SetLotteryImage), new { lotteryId = model.LotteryId });
        }
        [TempData]
        public string StatusMessage { get; set; }
        private void AddErrors(string error)
        {
            ModelState.AddModelError(string.Empty, error);
        }
    }
}