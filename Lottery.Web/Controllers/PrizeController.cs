﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Lottery.Manage.services.InfoCollector;
using Lottery.Manage.services.LotteryManage;
using Lottery.Manage.services.Prize;
using Lottery.Models.Models.DataStorageModels.Prize;
using Lottery.Models.Models.LotteryViewModels;
using Lottery.Models.Models.PrizeViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Lottery.Web.Controllers
{
    public class PrizeController : Controller
    {
        private readonly ILotteryManageService _lotteryManageService;
        private readonly ILotteryInfoProviderService _lotteryInfoProviderService;
        private readonly ILotteryPrizeService _lotteryPrizeService;
        private static readonly int pageSize = 10;

        public PrizeController(ILotteryManageService lotteryManageService, ILotteryInfoProviderService lotteryInfoProviderService, ILotteryPrizeService lotteryPrizeService)
        {
            _lotteryManageService = lotteryManageService;
            _lotteryInfoProviderService = lotteryInfoProviderService;
            _lotteryPrizeService = lotteryPrizeService;
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> AllPrizies(int? page, int? size)
        {
            ViewBag.StatusMessage = StatusMessage;
            var prizies = await _lotteryPrizeService.GetAllPrizeis();
            var pageNumber = page ?? 1;
            var currentPageSize = size ?? pageSize;
            var paginatedList =
                new PaginatedList<LotteryPrize>(prizies.AsQueryable(),prizies.Count,pageNumber,currentPageSize);
            ViewBag.StatusMessage = StatusMessage;
            return PartialView(paginatedList);
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public  IActionResult CreatePrize()
        {
           return PartialView(new CreatePrizeViewModel());
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> CreatePrize(CreatePrizeViewModel model)
        {
           await _lotteryManageService.AddPrize(model.Description);
            StatusMessage = "Prize successfully added";
           return RedirectToAction(nameof(AllPrizies));
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> DeletePrize(int prizeId)
        {
            var result =  await _lotteryManageService.DeletePrize(prizeId);
            if (!result.Success)
            {
                StatusMessage = result.Description;
                return RedirectToAction(nameof(AllPrizies));
            }
            StatusMessage = "prize deleted";
            return RedirectToAction(nameof(AllPrizies));
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> UpdatePrize(int prizeId)
        {
            var prize = await _lotteryPrizeService.GetPrizeById(prizeId);
            var model = Mapper.Map<CreatePrizeViewModel>(prize);
            return PartialView(model);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> UpdatePrize(CreatePrizeViewModel model)
        {
            var prize = Mapper.Map<LotteryPrize>(model);
            var result = await _lotteryManageService.UpdatePrize(prize);
            if (!result.Success)
            {
                StatusMessage = result.Description;
                return RedirectToAction(nameof(AllPrizies));
            }
            StatusMessage = "prize updated";
            return RedirectToAction(nameof(AllPrizies));
        }

        [TempData]
        public string StatusMessage { get; set; }
    }
}