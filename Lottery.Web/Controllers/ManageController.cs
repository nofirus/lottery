﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using AutoMapper;
using Lottery.Manage.services;
using Lottery.Manage.services.UserController;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Lottery.Web.Services;
using Lottery.Models.Models.DataStorageModels.User;
using Lottery.Models.Models.ManageViewModels;
using Lottery.Models.Models.ManageViewModels.AdminManageViewModel;
using Lottery.Models.Models.DataStorageModels.Roles;
using Lottery.Models.Models.LotteryViewModels;

namespace Lottery.Web.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class ManageController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly EmailSender _emailSender;
        private static readonly int pageSize = 10;
      
        private readonly ILogger _logger;
        private readonly UrlEncoder _urlEncoder;
        private readonly IUserControllerService _userControllerService;
        private const string AuthenticatorUriFormat = "otpauth://totp/{0}:{1}?secret={2}&issuer={0}&digits=6";
        private const string RecoveryCodesKey = nameof(RecoveryCodesKey);

        public ManageController(
          UserManager<ApplicationUser> userManager,
          SignInManager<ApplicationUser> signInManager,
       
          ILogger<ManageController> logger,
          UrlEncoder urlEncoder, UserControllerService userControllerService, EmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
           
            _logger = logger;
            _urlEncoder = urlEncoder;
            _userControllerService = userControllerService;
            _emailSender = emailSender;
        }

        [TempData]
        public string StatusMessage { get; set; }

        [HttpGet]
        public async Task<IActionResult> UserInfo()
        {
            var user = await _userControllerService.GetUserAsync(User);
            var model = Mapper.Map<UserInfoViewModel>(user);
            if (await _userManager.IsInRoleAsync(user, "Admin"))
            {
                return await EditUser(user.Email);
            }
            model.StatusMessage = StatusMessage;
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UserInfo(UserInfoViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = Mapper.Map<ApplicationUser>(model);
            await _userControllerService.UpdateUserInfo(User, user);
            ViewBag.IsAdmin = User.IsInRole("Admin");
            StatusMessage = "Your profile has been updated";
            return RedirectToAction(nameof(UserInfo));
        }

       
        [HttpGet]
        [Authorize(Roles = "User,Manager,Admin")]
        public async Task<IActionResult> ChangePassword()
        {
            var hasPassword = await _userControllerService.HassPasswordAsync(User);
            var model = new ChangePasswordViewModel {StatusMessage = StatusMessage};
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await _userControllerService.UpdatePassword(User, model.OldPassword, model.NewPassword);
            if (!result.Succeeded)
            {
                AddErrors(result);
                StatusMessage = "Error occurred while changing your password";
                model.StatusMessage = StatusMessage;
                return PartialView(model);
            }
            _logger.LogInformation("User changed their password successfully.");
            StatusMessage = "Your password has been changed.";
            return RedirectToAction(nameof(ChangePassword));
        }


        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AdminManage(int? page, int? size)
        {
            var allUsers = Mapper.Map<List<UserInfoViewModel>>(await _userControllerService.GetAllUsersAsync());
            var pageNumber = page ?? 1;
            var currentPageSize = size?? pageSize;
            var paginatedList = new PaginatedList<UserInfoViewModel>(allUsers.AsQueryable(), allUsers.Count, pageNumber,
                Math.Abs(currentPageSize));
            return PartialView(paginatedList);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> EditUser(string email)
        {
            var user = await _userControllerService.GetUserByEmailAsync(email);
            var userInfo = Mapper.Map<ApplicationUser, UserInfoViewModel>(user);
            userInfo.Roles = await _userControllerService.GetUserRolesAsync(user);
            ViewBag.IsAdmin = User.IsInRole(Roles.Admin.ToString());
            userInfo.StatusMessage = StatusMessage;
            return PartialView("UserInfo", userInfo);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> EditUser(string userEmail,UserInfoViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(nameof(UserInfo),model);
            }

            var user = Mapper.Map<ApplicationUser>(model);
            await _userControllerService.UpdateUserInfo(userEmail, user);
            StatusMessage = "profile has been updated";
            return await EditUser(model.Email);
        }

        [HttpPost]
        [Authorize(Roles ="Admin")]
        public async Task<IActionResult> AddUserToRole(string userEmail,string roleName)
        {
            await _userControllerService.AddRoleToUserAsync(userEmail, roleName);
            return await EditUser(userEmail);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> RemoveUserFromRole(string userEmail, string roleName)
        {
            var result = await _userControllerService.RemoveUserFromRole(userEmail, roleName);
            if (!result.Succeeded)
            {
                AddErrors(result);
            }
            return await EditUser(userEmail);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> BlockUser(BlockUserViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return await EditUser(model.UserEmail);
            }
            await _userControllerService.BlockUserAsync(model.UserEmail, model.BlockReason);
            return await EditUser(model.UserEmail);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Unlock(string userEmail)
        {
            await _userControllerService.UnlockUserAsync(userEmail);
            return await EditUser(userEmail);
        }

        [HttpGet]
        public async Task<IActionResult> TwoFactorAuthentication()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var model = new TwoFactorAuthenticationViewModel
            {
                HasAuthenticator = await _userManager.GetAuthenticatorKeyAsync(user) != null,
                Is2faEnabled = user.TwoFactorEnabled,
                TwoFactorEmailEnabled = user.TwoFactorEmailEnabled,
                RecoveryCodesLeft = await _userManager.CountRecoveryCodesAsync(user),
            };
            ViewBag.StatusMessage = StatusMessage;
            return PartialView(model);
        }
        [HttpGet]
        public async Task<IActionResult> Disable2faWarning()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!user.TwoFactorEnabled && !user.TwoFactorEmailEnabled)
            {
                throw new ApplicationException($"Unexpected error occured disabling 2FA for user with ID '{user.Id}'.");
            }

            return PartialView(nameof(Disable2fa));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Disable2fa()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var disable2faResult = await _userControllerService.DisableTwoFactor(user);
            if (!disable2faResult.Succeeded)
            {
                throw new ApplicationException($"Unexpected error occured disabling 2FA for user with ID '{user.Id}'.");
            }

            _logger.LogInformation("User with ID {UserId} has disabled 2fa.", user.Id);
            return RedirectToAction(nameof(TwoFactorAuthentication));
        }

    
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> EnableEmailAuthenticator()
        {
            var user = await _userManager.GetUserAsync(User);
            var codeString = new StringBuilder();
            await _userManager.ResetAuthenticatorKeyAsync(user);
            var unformattedKey = await _userManager.GetAuthenticatorKeyAsync(user);
            var code = await _userManager.GenerateTwoFactorTokenAsync(user, "EmailTwoFactor");
            await _emailSender.SendEmail($"your recovery code is {code}", user);
            var model = new EmailAuthenticator();
            return PartialView(model);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> EnableEmailAuthenticator(EmailAuthenticator model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(model);
            }
            var user = await _userManager.GetUserAsync(User);
            var verificationCode = model.Code.Replace(" ", string.Empty).Replace("-", string.Empty);
            if (!await _userManager.VerifyTwoFactorTokenAsync(user,
                "EmailTwoFactor", verificationCode))
            {
                ModelState.AddModelError("Code", "Verification code is invalid.");
                return PartialView(model);
            }
            await _userControllerService.EnableEmailAuthenticator(User);
           
            var codes =  await _userManager.GenerateNewTwoFactorRecoveryCodesAsync(user, 10);
            StatusMessage = "Email Authentication active";
            return RedirectToAction(nameof(TwoFactorAuthentication));
        }

        [HttpGet]
        public async Task<IActionResult> EnableAuthenticator()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var model = new EnableAuthenticatorViewModel();
            await LoadSharedKeyAndQrCodeUriAsync(user, model);

            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EnableAuthenticator(EnableAuthenticatorViewModel model)
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                await LoadSharedKeyAndQrCodeUriAsync(user, model);
                return PartialView(model);
            }

            // Strip spaces and hypens
            var verificationCode = model.Code.Replace(" ", string.Empty).Replace("-", string.Empty);

            var is2faTokenValid = await _userManager.VerifyTwoFactorTokenAsync(
                user, _userManager.Options.Tokens.AuthenticatorTokenProvider, verificationCode);

            if (!is2faTokenValid)
            {
                ModelState.AddModelError("Code", "Verification code is invalid.");
                await LoadSharedKeyAndQrCodeUriAsync(user, model);
                return PartialView(model);
            }

            await _userManager.SetTwoFactorEnabledAsync(user, true);
            _logger.LogInformation("User with ID {UserId} has enabled 2FA with an authenticator app.", user.Id);
            var recoveryCodes = await _userManager.GenerateNewTwoFactorRecoveryCodesAsync(user, 10);
            TempData[RecoveryCodesKey] = recoveryCodes.ToArray();

            return RedirectToAction(nameof(ShowRecoveryCodes));
        }

     

        [HttpGet]
        public IActionResult ShowRecoveryCodes()
        {
            var recoveryCodes = (string[])TempData[RecoveryCodesKey];
            if (recoveryCodes == null)
            {
                return RedirectToAction(nameof(TwoFactorAuthentication));
            }

            var model = new ShowRecoveryCodesViewModel { RecoveryCodes = recoveryCodes };
            return PartialView(model);
        }

        [HttpGet]
        public IActionResult ResetAuthenticatorWarning()
        {
            return PartialView(nameof(ResetAuthenticator));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetAuthenticator()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            await _userManager.SetTwoFactorEnabledAsync(user, false);
            await _userManager.ResetAuthenticatorKeyAsync(user);
            _logger.LogInformation("User with id '{UserId}' has reset their authentication app key.", user.Id);

            return RedirectToAction(nameof(EnableAuthenticator));
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GenerateRecoveryCodesWarning()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!user.TwoFactorEnabled)
            {
                throw new ApplicationException($"Cannot generate recovery codes for user with ID '{user.Id}' because they do not have 2FA enabled.");
            }

            return PartialView(nameof(GenerateRecoveryCodes));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> GenerateRecoveryCodes()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!user.TwoFactorEnabled)
            {
                throw new ApplicationException($"Cannot generate recovery codes for user with ID '{user.Id}' as they do not have 2FA enabled.");
            }

            var recoveryCodes = await _userManager.GenerateNewTwoFactorRecoveryCodesAsync(user, 10);
            _logger.LogInformation("User with ID {UserId} has generated new 2FA recovery codes.", user.Id);

            var model = new ShowRecoveryCodesViewModel { RecoveryCodes = recoveryCodes.ToArray() };

            return PartialView(nameof(ShowRecoveryCodes), model);
        }

        private async Task LoadSharedKeyAndQrCodeUriAsync(ApplicationUser user, EnableAuthenticatorViewModel model)
        {
            var unformattedKey = await _userManager.GetAuthenticatorKeyAsync(user);
            if (string.IsNullOrEmpty(unformattedKey))
            {
                await _userManager.ResetAuthenticatorKeyAsync(user);
                unformattedKey = await _userManager.GetAuthenticatorKeyAsync(user);
            }

            model.SharedKey = FormatKey(unformattedKey);
            model.AuthenticatorUri = GenerateQrCodeUri(user.Email, unformattedKey);
        }

        [HttpPost]
        public async Task<IActionResult> SendVerificationEmail()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            
            var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
            var email = user.Email;
            await _emailSender.SendEmailConfirmationAsync(email, callbackUrl);
            StatusMessage = "Verification email sent. Please check your email.";
            return RedirectToAction(nameof(UserInfo));
        }


        #region AutoGen
        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private string FormatKey(string unformattedKey)
        {
            var result = new StringBuilder();
            int currentPosition = 0;
            while (currentPosition + 4 < unformattedKey.Length)
            {
                result.Append(unformattedKey.Substring(currentPosition, 4)).Append(" ");
                currentPosition += 4;
            }
            if (currentPosition < unformattedKey.Length)
            {
                result.Append(unformattedKey.Substring(currentPosition));
            }

            return result.ToString().ToLowerInvariant();
        }

        private string GenerateQrCodeUri(string email, string unformattedKey)
        {
            return string.Format(
                AuthenticatorUriFormat,
                _urlEncoder.Encode("Lottery.Web"),
                _urlEncoder.Encode(email),
                unformattedKey);
        }
        /*
        private async Task LoadSharedKeyAndQrCodeUriAsync(ApplicationUser user, EnableAuthenticatorViewModel model)
        {
            var unformattedKey = await _userManager.GetAuthenticatorKeyAsync(user);
            if (string.IsNullOrEmpty(unformattedKey))
            {
                await _userManager.ResetAuthenticatorKeyAsync(user);
                unformattedKey = await _userManager.GetAuthenticatorKeyAsync(user);
            }

            model.SharedKey = FormatKey(unformattedKey);
            model.AuthenticatorUri = GenerateQrCodeUri(user.Email, unformattedKey);
        }
        */
        #endregion

        #endregion
    }
}
