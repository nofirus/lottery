﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lottery.Manage.services;
using Lottery.Manage.services.InfoCollector;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.Roles;
using Lottery.Models.Models.Excel;
using Lottery.Models.Models.LotteryViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Lottery.Web.Controllers
{
    public class ExcelDataController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IExcelDataService _excelDataService;
        private readonly ILotteryInfoProviderService _infoProviderService;
        private readonly int pageSize = 10;

        public ExcelDataController(IHostingEnvironment hostingEnvironment, IExcelDataService excelDataService, ILotteryInfoProviderService infoProviderService)
        {
            _hostingEnvironment = hostingEnvironment;
            _excelDataService = excelDataService;
            _infoProviderService = infoProviderService;
        }

        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> Index(int? page, int? size)
        {
            var lotteryList = await _infoProviderService.GetAllLotteries();
            var pageNumber = page ?? 1;
            var currentPageSize = size ?? pageSize;
            var paginatedList = new PaginatedList<LotteryEntity>(lotteryList.AsQueryable(), lotteryList.Count(),
                pageNumber, currentPageSize);
            ViewBag.StatusMessage = StatusMessage;
            return PartialView(paginatedList);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> ExportLotteries(LotteryFilters filter)
        {
            await _excelDataService.ExportLotteries(_hostingEnvironment.WebRootPath, filter);
            StatusMessage = $"Exported:";
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> ImportLottery()
        {
            var file = Request.Form.Files[0];
            var result = await _excelDataService.ImportLottery(_hostingEnvironment.WebRootPath, file);
            StatusMessage = result.Message;
            return RedirectToAction(nameof(Index));
        }
        [TempData]
        public string StatusMessage { get; set; }
    }
}