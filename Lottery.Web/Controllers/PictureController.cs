﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Lottery.Manage.services;
using Lottery.Manage.services.LotteryManage;
using Lottery.Models.Models.DataStorageModels.Picture;
using Lottery.Models.Models.LotteryViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Lottery.Web.Controllers
{
    public class PictureController : Controller
    {
        private readonly ILotteryPictureService _lotteryPictureService;
        private readonly IHostingEnvironment _appEnvironment;
        private readonly int pageSize = 10;

        public PictureController(ILotteryPictureService lotteryPictureService, IHostingEnvironment appEnvironment)
        {
            _lotteryPictureService = lotteryPictureService;
            _appEnvironment = appEnvironment;
        }


        [HttpGet]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> Index(int? page, int? size)
        {
            var pageNumber = page??1;
            var currentPageSize = size ?? pageSize;
            var allPictures = await _lotteryPictureService.GetAllPictures();
            var paginatedList = new PaginatedList<Picture>(allPictures.AsQueryable(), allPictures.Count, pageNumber,
                currentPageSize);
            ViewBag.StatusMessage = StatusMessage;
            return PartialView(paginatedList);
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> AddPicture()
        {
            var pictures = Request.Form.Files;
            foreach (var picture in pictures)
            {
                var path = "/PictureFiles/" + picture.FileName;
                
                    using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                    {
                        await picture.CopyToAsync(fileStream);
                    }
               await _lotteryPictureService.AddPicture(path);
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> DeletePicture(int pictureId)
        {
            var result =  await _lotteryPictureService.DeletePicture(pictureId, _appEnvironment.WebRootPath);
            if (!result.Success)
            {
                StatusMessage = result.Description;
            }
           return RedirectToAction(nameof(Index));
        }

        [TempData]
        public string StatusMessage { get; set; }
    }
}