﻿using Lottery.Models.Models.DataStorageModels.Roles;
using Lottery.Models.Models.DataStorageModels.User;
using Lottery.Web.Services;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace Lottery.Web
{
    public class Program
    {
        public static void  Main(string[] args)
        {
            var host = BuildWebHost(args);
            var services = host.Services.CreateScope().ServiceProvider;
            var userManager = services.GetRequiredService<UserManager<ApplicationUser>>();
            var roleManager = services.GetRequiredService<RoleManager<UserRole>>();
            RoleInitializer.InitializeAsync(userManager,roleManager).GetAwaiter();// i am not sure about this
            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
