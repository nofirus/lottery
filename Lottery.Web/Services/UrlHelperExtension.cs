﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lottery.Web.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Lottery.Web.Services
{
    public static  class UrlHelperExtension
    {
        public static string EmailConfirmationLink(this IUrlHelper urlHelper, string userId, string code, string scheme)
        {
            return urlHelper.Action(
                action: nameof(AccountController.ConfirmEmail),
                controller: "Account",
                values: new { userId, code },
                protocol: scheme);
        }
    }
}
