﻿// Write your JavaScript code.
function refreshIndex() {
    $.get("/Lottery/Index",
        function (result) {
            $("#update").html(result);
        });
}
function refreshTimer() {
    if (document.getElementById("trigger")!==null) {
        refreshIndex();
    }
}
window.RefreshTimerInterval = setInterval(refreshTimer, 240000);

        function timer() {

            if (document.getElementById("hour") !== null) {
                let hour = document.getElementById("hour").innerHTML;
                let minute = document.getElementById("minute").innerHTML;
                let second = document.getElementById("second").innerHTML;
                let end = false;

                if (second > 0) second--;
                else {
                    second = 59;

                    if (minute > 0) minute--;
                    else {
                        second = 59;

                        if (hour > 0) hour--;
                        else end = true;
                    }
                }
                if (end) {
                    clearInterval(intervalID);
                    clearInterval(window.RefreshTimerInterval);
                    showWinners();
                    window.RefreshTimerInterval = setInterval(refreshTimer, 240000);
                } else {
                    document.getElementById("hour").innerHTML = hour;
                    document.getElementById("minute").innerHTML = minute;
                    document.getElementById("second").innerHTML = second;
                }
            }
        }
        window.intervalID = setInterval(timer, 1000);
    
    

function showWinners() {
    $.ajax({
        type: "GET",
        url: "/Lottery/LastLotteryWinners",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var timerId;
            $("#DIV").html("");
            const DIV = "";
            var i = -1;
            clearInterval(RefreshTimerInterval);
            return function() {
                if (++i < data.length) {
                    const rows = `<tr><td id='Place'>${data[i]}</td></tr>`;
                    $("#Table").append(rows);
                    console.log(data);
                    timerId = setTimeout(arguments.callee, 2000);
                }
            }();
        }
    }); 
}

//upload images 

function Upload() {
        var fileUpload = $("#files").get(0);
        var files = fileUpload.files;
        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            data.append(files[i].name, files[i]);
        }
        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = (evt.loaded / evt.total) * 100;
                        $("#progressBar").attr("value", percentComplete);
                       // $("#textIndicator").innerHTML = `uploading in progress${percentComplete}%`;
                        document.getElementById("textIndicator").innerHTML = `uploading in progress: ${Math.round(percentComplete)}%`;
                    }
                }, false);
                return xhr;
            },
            type: "POST",
            url: "/Picture/AddPicture",
            contentType: false,
            processData: false,
            data: data,
            success: function (message) {
                document.getElementById("update").innerHTML = message;
            },
            beforeSend: function () {
                $("#progressBar").show();
                $("#textIndicator").show();
            },
            error: function() {
                $("#progressBar").hide();
                document.getElementById("textIndicator").innerHTML = "There was error uploading files!";
            }
        }); 
}
