﻿using AutoMapper;
using Lottery.Models.Models.AccountViewModels;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryQuiz;
using Lottery.Models.Models.DataStorageModels.Prize;
using Lottery.Models.Models.DataStorageModels.User;
using Lottery.Models.Models.LotteryViewModels;
using Lottery.Models.Models.ManageViewModels;
using Lottery.Models.Models.PrizeViewModels;
using Lottery.Models.Models.QuestionViewModel;
using Lottery.Models.Models.QuizViewModel;

namespace Lottery.Web
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {

                cfg.CreateMap<RegisterViewModel, ApplicationUser>()
                    .ForMember("UserName", opt => opt.MapFrom(src => src.Email.Split( new []{'@'})[0]));
                cfg.CreateMap<ApplicationUser, UserInfoViewModel>()
                    .ForMember(m => m.StatusMessage, i => i.Ignore());
                cfg.CreateMap<UserInfoViewModel, ApplicationUser>();
                cfg.CreateMap<ApplicationUser, UserInfoViewModel>()
                    .ForMember("IsEmailConfirmed",opt=>opt.MapFrom(src=>src.EmailConfirmed));
                cfg.CreateMap<LotteryEntity, LotteryCreatingViewModel>();
                cfg.CreateMap<LotteryCreatingViewModel, LotteryEntity>();
                cfg.CreateMap<CreatePrizeViewModel, LotteryPrize>();
                cfg.CreateMap<Question, QuestionCreateViewModel>();
                cfg.CreateMap<Quiz, CreateQuizViewModel>();
                cfg.CreateMap<CreateQuizViewModel, Quiz>();
                cfg.CreateMap<Quiz,QuizInfoViewModel>();
                cfg.CreateMap<QuizInfoViewModel, Quiz>();
            });
        }
    }
}
