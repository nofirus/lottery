﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Lottery.Web.Services;
using Lottery.Manage.services;
using Lottery.DataStorage.Data;
using Lottery.DataStorage.Repositories.ExcludedFromTheLotteryUsersRepository;
using Lottery.DataStorage.Repositories.LotteryParticipantsRepository;
using Lottery.DataStorage.Repositories.LotteryRepository;
using Lottery.DataStorage.Repositories.LotteryWinerRepository;
using Lottery.DataStorage.Repositories.PictureRepository;
using Lottery.DataStorage.Repositories.PrizeRepository;
using Lottery.DataStorage.Repositories.QuizRepository;
using Lottery.DataStorage.Repositories.UserRepository;
using Lottery.DataStorage.Services;
using Lottery.Manage;
using Lottery.Manage.services.InfoCollector;
using Lottery.Manage.services.LotteryManage;
using Lottery.Manage.services.LotteryQuiz;
using Lottery.Manage.services.Prize;
using Lottery.Manage.services.UserController;
using Lottery.Models.Models.DataStorageModels.Roles;
using Lottery.Models.Models.DataStorageModels.User;

namespace Lottery.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<LotteryDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddTransient<UserControllerService, UserControllerService>();

            AutoMapperConfiguration.Configure();
        
            services.AddIdentity<ApplicationUser, UserRole>(options =>
            {
                options.Lockout.AllowedForNewUsers = true;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.User.RequireUniqueEmail = true;
                options.Tokens.ProviderMap.Add("EmailTwoFactor", new TokenProviderDescriptor(typeof(IUserTwoFactorTokenProvider<ApplicationUser>)));
            }).AddEntityFrameworkStores<LotteryDbContext>()
             .AddDefaultTokenProviders();

            services.AddAuthentication().AddGoogle(googleOptions =>
            {
                googleOptions.ClientId = Configuration["Authentication:Google:ClientId"];
                googleOptions.ClientSecret = Configuration["Authentication:Google:ClientSecret"];
            });

            // Add application services.
            services.AddScoped<SignInManager<ApplicationUser>>();
            services.AddScoped<UserManager<ApplicationUser>>();
            services.AddTransient<RoleManager<UserRole>>();
            services.AddScoped<ILotteryManageService,LotteryManageService>();
            services.AddScoped<ILotteryQuizService, LotteryQuizService>();
            services.AddScoped<ILotteryManageService, LotteryManageService>();
            services.AddScoped<ILotteryQuizService, LotteryQuizService>();
            services.AddScoped<IUserControllerService, UserControllerService>();   
            services.AddScoped<ILotteryInfoProviderService, LotteryInfoProviderService>();
            services.AddScoped<ILotteryPrizeService, LotteryPrizeService>();
            services.AddScoped<ILotteryPictureService, LotteryPictureService>();
            services.AddScoped<ILotteryRepository, LotteryRepository>();
            services.AddScoped<ILotteryWinerRepository, LotterWinnerRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ILotteryParticipantsRepository, LotteryParticipantsRepository>();
            services.AddScoped<IQuizRepository, QuizRepository>();
            services.AddScoped<IExcludedFromTheLotteryUsersRepository, ExcludedFromTheLotteryUsersRepository>();
            services.AddScoped<IPictureRepository, PictureRepository>();
            services.AddScoped<IPrizeRepository, PrizeRepository>();
            services.AddScoped<IUserTwoFactorTokenProvider<ApplicationUser>, DataProtectorTokenProvider<ApplicationUser>>();
            services.AddScoped<IExcelDataService,ExcelDataService>();

            services.AddScoped<EmailSender>();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

           // app.UseSignalR(routes => routes.MapHub<WinnersHub>("winners"));

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Lottery}/{action=MainPage}/{id?}");
            });
        }
    }
}
