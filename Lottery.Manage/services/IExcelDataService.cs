﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.Excel;
using Lottery.Models.Models.LotteryViewModels;
using Microsoft.AspNetCore.Http;

namespace Lottery.Manage.services
{
    public interface IExcelDataService
    {
        Task ExportLottery(string rootPath,LotteryEntity lottery);
        Task ExportLotteries(string rootPath, LotteryFilters filters);

        Task<LotteryCreationResult> ImportLottery(string rootPath, IFormFile file);
    }
}
