﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Lottery.DataStorage.Repositories.ExcludedFromTheLotteryUsersRepository;
using Lottery.DataStorage.Repositories.LotteryParticipantsRepository;
using Lottery.DataStorage.Repositories.LotteryRepository;
using Lottery.DataStorage.Repositories.LotteryWinerRepository;
using Lottery.DataStorage.Repositories.PictureRepository;
using Lottery.DataStorage.Repositories.PrizeRepository;
using Lottery.DataStorage.Repositories.QuizRepository;
using Lottery.Manage.services.InfoCollector;
using Lottery.Manage.services.Validation;
using Lottery.Models.Models;
using Lottery.Models.Models.DataStorageModels;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryParticipants;
using Lottery.Models.Models.DataStorageModels.LotteryQuiz;
using Lottery.Models.Models.DataStorageModels.LotteryWiner;
using Lottery.Models.Models.DataStorageModels.Picture;
using Lottery.Models.Models.DataStorageModels.Prize;
using Lottery.Models.Models.DataStorageModels.Types;
using Lottery.Models.Models.DataStorageModels.User;
using Lottery.Models.Models.LotteryViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Lottery.Manage.services.LotteryManage
{
    public class LotteryManageService : ILotteryManageService
    {
        private readonly ILotteryRepository _lotteryRepository;
        private readonly IPictureRepository _pictureRepository;
        private readonly IPrizeRepository _prizeRepository;
        private readonly ILotteryWinerRepository _lotteryWinerRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILotteryParticipantsRepository _lotteryParticipantsRepository;
        private readonly IExcludedFromTheLotteryUsersRepository _excludedFromTheLotteryUsersRepository;
        private readonly IQuizRepository _quizRepository;
        private readonly ILotteryInfoProviderService _lotteryInfoProviderService;
        private readonly EmailSender _emailSender;

        public LotteryManageService(ILotteryRepository lotteryRepository, IPictureRepository pictureRepository,
            IPrizeRepository prizeRepository, ILotteryWinerRepository lotteryWinerRepository,
            UserManager<ApplicationUser> userManager, ILotteryParticipantsRepository lotteryParticipantsRepository, IExcludedFromTheLotteryUsersRepository excludedFromTheLotteryUsersRepository, IQuizRepository quizRepository, ILotteryInfoProviderService lotteryInfoProviderService, EmailSender emailSender)
        {
            this._lotteryRepository = lotteryRepository;
            this._pictureRepository = pictureRepository;
            _prizeRepository = prizeRepository;
            _lotteryWinerRepository = lotteryWinerRepository;
            _userManager = userManager;
            _lotteryParticipantsRepository = lotteryParticipantsRepository;
            _excludedFromTheLotteryUsersRepository = excludedFromTheLotteryUsersRepository;
            _quizRepository = quizRepository;
            _lotteryInfoProviderService = lotteryInfoProviderService;
            _emailSender = emailSender;
        }

       

        public async Task<ValidationResult> SetLotteryPriziesAsync(SetPrizesViewModel model)
        {
           
            var leftGroupBorder = model.LeftGroupBorder.ToList();
            var rightGroupBorder = model.RightGtoupBorder.ToList();
            for (int i = 0; i < leftGroupBorder.Count()-1; i++)
            {
                if (rightGroupBorder[i] < leftGroupBorder[i])
                {
                    return new ValidationResult(){Description = string.Format("wrong border in line {0}",i+1),Success = false};
                }

                if (leftGroupBorder[i + 1] <= rightGroupBorder[i])
                {
                    return new ValidationResult(){Description = string.Format("wrong border in line {0}",i+2), Success = false};
                }
            }

            if (rightGroupBorder[rightGroupBorder.Count() - 1] != model.AmountOfWinners)
            {
                return new ValidationResult() { Description = string.Format("wrong border in line {0}", rightGroupBorder.Count), Success = false };
            }

            var lottery = await _lotteryRepository.GetLotteryByIdAsync(model.LotteryId);
            lottery.AmountOfPrizeGroups = model.AmountOfPrizeGroups;
            var prizies = new List<LotteryPrizeList>();
            for (var i = 0; i < leftGroupBorder.Count(); i++)
            {
                for (var j = leftGroupBorder[i]-1; j < rightGroupBorder[i]; j++)
                {
                    prizies.Add(new LotteryPrizeList()
                    {
                        LotteryId = model.LotteryId,
                        PrizeId = model.GroupPrizies.ElementAt(i),
                        Place = j + 1
                    });
                }
            }
            await _prizeRepository.AddPriziesForLotteryAsync(lottery, prizies);
            await _lotteryRepository.UpdateLotteryAsync(lottery);
            return new ValidationResult() { Description = "OK", Success = true};
        }

        public async Task SetLotteryImageAsync(int lotteryId, int pictureId)
        {
            var lottery = await _lotteryRepository.GetLotteryByIdAsync(lotteryId);
            var picture = await _pictureRepository.GetPictureById(pictureId);
            await _pictureRepository.SetImageToTheLotteryAsync(lottery, picture);
        }

        public async Task UpdateLotteryAsync(LotteryCreatingViewModel model)
        {
            var lottery = Mapper.Map<LotteryEntity>(model);
            await _lotteryRepository.UpdateLotteryAsync(lottery);
        }

        public async Task<LotteryCreationResult> AddLotteryAsync(LotteryEntity lottery)
        {
            try
            {
                var closestLottery = await _lotteryInfoProviderService.GetClosestLottery();
                if (Math.Abs(lottery.LotteryDate.Subtract(closestLottery.LotteryDate).TotalMinutes) < 5)
                    return new LotteryCreationResult()
                    {
                        Succsess = false,
                        Message = string.Format(
                            "minumum time between lotteries must be greater then 5 minutes, closest lottery is {0}",
                            closestLottery.Description + ":" + closestLottery.LotteryDate)
                    };
                if (Math.Abs(lottery.LotteryDate.Subtract(DateTime.Now).TotalMinutes) < 5)
                    return new LotteryCreationResult()
                    {
                        Succsess = false,
                        Message = string.Format(
                            "The lottery start time must not be earlier than 5 minutes")
                    };
                await _lotteryRepository.AddLotteryAsync(lottery);
            }
            catch (Exception e)
            {
                return new LotteryCreationResult() { Succsess = false, Message = e.Message };
            }
           
            return new LotteryCreationResult(){Succsess = true,Message = "success"};
        }

        public async Task<LotteryEntity> LotteryRandomazer()
        {
            var random = new Random();
            var lottery = await _lotteryInfoProviderService.GetClosestLottery();
            if (!(lottery.LotteryDate.Subtract(DateTime.Now).TotalSeconds < 40)) return new LotteryEntity();
            var participantsList = await _lotteryParticipantsRepository.GetAllParticipantsForLottery(lottery);
                  
            var winners = new List<LotteryWinners>();
            if (participantsList.Any())
            {
                for (var i = 0; i < lottery.AmountOfWinners; i++)
                {
                    var randValue = random.Next(1, participantsList.Count());
                    var userId = participantsList.ElementAt(randValue-1).Participants.Id;
                    participantsList.RemoveAt(randValue - 1);
                    winners.Add(new LotteryWinners()
                    {
                        LotteryId = lottery.Id,
                        UserId = userId,
                        Place = i + 1
                    });
                    if (!participantsList.Any())
                    {
                        break;
                    }
                }
            }
            await _lotteryWinerRepository.AddLotteryWinners(winners);
            return lottery;
        }

        public async Task<bool> AddParticipants(ClaimsPrincipal userClaims, int lotteryId)
        {
            var user = await _userManager.GetUserAsync(userClaims);
            var lottery = await _lotteryRepository.GetLotteryByIdAsync(lotteryId);
            var partisipants = _lotteryParticipantsRepository.GetAllParticipantsForLottery(lottery).Result
                .Select(x => x.Participants);
            if (partisipants.Contains(user)) return false;
            await _lotteryParticipantsRepository.AddLotteryParticipantsAsync(
                new LotteryParticipants() {Lottery = lottery, Participants = user});
            return true;

        }
      
        public async Task AddParticipantsByEmail(int lotteryId, string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            var lottery = await _lotteryRepository.GetLotteryByIdAsync(lotteryId);
            await _lotteryParticipantsRepository.AddLotteryParticipantsAsync( new LotteryParticipants(){Lottery = lottery,Participants = user});
        }

        public async Task RemoveLotteryParticipantsByEmail(int lotteryId, string email, string removeReason)
        {
            var lottery = await _lotteryRepository.GetLotteryByIdAsync(lotteryId);
            var user = await _userManager.FindByEmailAsync(email);
            await _lotteryParticipantsRepository.RemoveParticipant(new LotteryParticipants()
            {
                Lottery = lottery,
                Participants = user
            });
            await _excludedFromTheLotteryUsersRepository.Add(
                new ExcludedFromTheLotteryUsers() {Lottery = lottery, User = user,RemoveReason = removeReason});
        }

        public async Task RemoveUserFromExcluded(int lotteryId, string userEmail)
        {
            var user = await _userManager.FindByEmailAsync(userEmail);
            var lottery = await _lotteryRepository.GetLotteryByIdAsync(lotteryId);
            await _excludedFromTheLotteryUsersRepository.RemoveUserFromExcluded(
                new ExcludedFromTheLotteryUsers() {Lottery = lottery, User = user});
            await AddParticipantsByEmail(lotteryId, userEmail);
        }

        public async Task<ValidationResult> AddNewQuestion(string questionText, QuestionType type, List<string> answers)
        {
            if (type.Equals(QuestionType.Close))
            {
                var q = new Question
                {
                    Text = questionText,
                    Type = type
                };
                var allQuestion = await _quizRepository.GetAllQuestions();
                var addedAnswers = await AddAsnwers(answers);
                var questionAsnwers = new List<QuestionAnswers>();

                if (allQuestion.Select(x => x.Text).Contains(q.Text))
                {
                    return new ValidationResult(){Success = false,Description = "This question already exist"};
                }
                var targetAnswer = new  List<Answer>();
                foreach (var text in answers)
                {
                   targetAnswer.Add(await _quizRepository.GetAnswerByText(text));
                }
                var addedQuestion = await _quizRepository.AddQuestion(q);
               
                foreach (var ans in targetAnswer)
                {
                    questionAsnwers.Add(new QuestionAnswers() {QuestionId = addedQuestion.Id, AnswerId = ans.Id});
                }
                await _quizRepository.AddAnswersOnQuestion(questionAsnwers);
            }
            else
            {
                var allQuestion = await _quizRepository.GetAllQuestions();
                    if (allQuestion.Select(x=>x.Text).Contains(questionText))
                    {
                        return new ValidationResult(){Success = false,Description = "Ok"};
                    }
               await _quizRepository.AddQuestion(new Question() {Text = questionText, Type = type});
            }
            return new ValidationResult() { Success = true, Description = "Ok" };
        }

        public async Task<List<Answer>> AddAsnwers(List<string> answers)
        {
            var allAnswers = await _quizRepository.GetAllAnswers();
            var uniqueAnswers = answers.Except(allAnswers.Select(x => x.Text));
            return await _quizRepository.AddAnswers(uniqueAnswers.Select(x => new Answer() { Text = x }).ToList());
        }

        public  Task<ValidationResult> CheckUserForLottery(ApplicationUser user, LotteryEntity lottery)
        {
            var validator = UserLotteryValidator.GetValidatorForLottery(lottery);
            var tcs = new TaskCompletionSource<ValidationResult>();
            if (validator.Validate(user, lottery))
            {
                tcs.SetResult( new ValidationResult(){Description = "OK", Success = true});
                return tcs.Task;
            }
            tcs.SetResult(new ValidationResult() { Description = "user does not match the conditions", Success = true });
            return tcs.Task;
        }

        public async Task<ValidationResult> CheckUserForLottery(ClaimsPrincipal userClaims, int lotteryId)
        {
            var user = await _userManager.GetUserAsync(userClaims);
            var lottery = await _lotteryInfoProviderService.GetLotteryByIdAsync(lotteryId);
            return await CheckUserForLottery(user, lottery);
        }

        public async Task AddPrize(string description)
        {
            var prize = new LotteryPrize(){Description = description};
            await _prizeRepository.AddPrizeAsync(prize);
        }

        public async Task DeleteLottery(int lotteryId)
        {
            
            var lottery = await _lotteryRepository.GetLotteryByIdAsync(lotteryId);
            if (_lotteryInfoProviderService.GetAllParticipantsForLotteryAsync(lotteryId).Result.Any())
            {
                var lotteryParticipants = await _lotteryParticipantsRepository.GetAllParticipantsForLottery(lottery);
                if (lottery.LotteryDate > DateTime.Now)
                {
                   
                    foreach (var participant in lotteryParticipants)
                    {
                        await _emailSender.SendEmail(string.Format("lottery {0} was canceled",lottery.Description),
                            participant.Participants);
                    }
                }

                if (await _lotteryInfoProviderService.IsLotteryHasQuiz(lotteryId))
                {
                   var quiz = await _quizRepository.GetQuizForLottery(lottery);
                   await _quizRepository.DeleteQuiz(quiz);
                }

               await _lotteryParticipantsRepository.DeleteAllParticipantsForLottery(lottery);
            }
            await _prizeRepository.DeleteAllPriziesForLottery(lottery);
            await  _lotteryRepository.DeleteLotteryAsync(lottery);
        }

        public async Task<ValidationResult> UpdateLotteryAsync(LotteryEntity lottery)
        {
           
             

                var oldLottery = await _lotteryRepository.GetLotteryByIdAsync(lottery.Id);
                var lotteryParticipants = await _lotteryInfoProviderService.GetAllParticipantsForLotteryAsync(lottery.Id);
                if (lottery.LotteryType == LotteryType.Close && lottery.LotteryKind == LotteryKind.Quiz)
                {
                    return new ValidationResult(){Success = false,Description = "a closed lottery can not have a quiz"};
                }
                if (oldLottery.LotteryKind != LotteryKind.Quiz && lottery.LotteryKind == LotteryKind.Quiz)
                {
                    if (lotteryParticipants.Any())
                    {

                        foreach (var participants in lotteryParticipants)
                        {
                          await _emailSender.SendEmail(
                                string.Format(
                                    "you was removed from lottery {0} because now you should to pass a quiz before taking part in the lottery",
                                    oldLottery.Description), participants);
                        }

                        await _lotteryParticipantsRepository.DeleteAllParticipantsForLottery(oldLottery);
                    }

                    var quiz = await _quizRepository.GetQuizForLottery(oldLottery);
                    if (quiz != null)
                    {
                        await _quizRepository.DeleteQuiz(quiz);
                    }
                }

                if (oldLottery.LotteryType == LotteryType.Open && lottery.LotteryType != LotteryType.Open)
                {

                    if (lotteryParticipants.Any())
                    {
                        foreach (var participants in lotteryParticipants)
                        {
                            await _emailSender.SendEmail(
                                string.Format("you was removed from lottery {0} because now this lottery is close type",
                                    oldLottery.Description), participants);
                        }

                        await _lotteryParticipantsRepository.DeleteAllParticipantsForLottery(oldLottery);

                    }
                }

                if (oldLottery.AmountOfWinners != lottery.AmountOfWinners)
                {
                    foreach (var participants in lotteryParticipants)
                    {
                        await _emailSender.SendEmail(
                            string.Format("amount of prize places changed from {0} to {1}", oldLottery.AmountOfWinners,
                                lottery.AmountOfWinners), participants);
                    }
                }

                oldLottery.LotteryDate = lottery.LotteryDate;
                oldLottery.RegistrationEndDate = lottery.RegistrationEndDate;
                oldLottery.AmountOfWinners = lottery.AmountOfWinners;
                oldLottery.Delay = lottery.Delay;
                oldLottery.LotteryKind = lottery.LotteryKind;
                oldLottery.LotteryType = lottery.LotteryType;
                oldLottery.OutputType = lottery.OutputType;
                oldLottery.Description = lottery.Description;
                await _lotteryRepository.UpdateLotteryAsync(oldLottery);
            return new ValidationResult(){Success = true, Description = "OK"};
        }

        public async Task<ValidationResult> DeletePrize(int prizeId)
        {
           var lotteries = await _prizeRepository.GetLotteriesForPrize(prizeId);
           if (lotteries.Any())
            {
                var message = new StringBuilder();
                foreach (var lottery in lotteries)
                {
                    message.Append(lottery.Description + ": " + lottery.LotteryDate).Append(Environment.NewLine);
                }
                return new ValidationResult(){Success = false,Description = "prize is used in lotteries: " + message};
            }
           await _prizeRepository.DeletePrizeAsync(prizeId);
           return new ValidationResult() { Success = true, Description = "OK" };
        }

        public async Task<ValidationResult> UpdatePrize(LotteryPrize prize)
        {
            var lotteries = await _prizeRepository.GetLotteriesForPrize(prize.Id);
            if (lotteries.Any())
            {
                var message = new StringBuilder();
                foreach (var lottery in lotteries)
                {
                    message.Append(lottery.Description + ": " + lottery.LotteryDate).Append(Environment.NewLine);
                }
                return new ValidationResult() { Success = false, Description = "prize is used in lotteries: " + message };
            }
             await _prizeRepository.UpdatePrizeAsync(prize);
            return new ValidationResult() { Success = true, Description = "OK" };
        }

        public async Task<ValidationResult> DeleteQuestion(int questionId)
        {
            var lotteries = await _quizRepository.GetAllLotteriesForQuestion(questionId);
            if (lotteries.Any())
            {
                var message = new StringBuilder();
                foreach (var lottery in lotteries)
                {
                    message.Append(lottery.Description + ": " + lottery.LotteryDate).Append(Environment.NewLine);
                }
                return new ValidationResult() { Success = false, Description = "question is used in lotteries: " + message };
            }

            await _quizRepository.DeleteQuestion(questionId);
            return new ValidationResult() { Success = true, Description = "OK" };
        }

        public async Task<ValidationResult> UpdateQuestion(Question question, List<string> answers)
        {
            var lotteries = await _quizRepository.GetAllLotteriesForQuestion(question.Id);
            if (lotteries.Any())
            {
                var message = new StringBuilder();
                foreach (var lottery in lotteries)
                {
                    message.Append(lottery.Description + ": " + lottery.LotteryDate).Append(Environment.NewLine);
                }
                return new ValidationResult() { Success = false, Description = "question is used in lotteries: " + message };
            }

            //var allAsnwers = await  _quizRepository.GetAllAnswersOnTheQuestion(question.Id);
            var questionAsnwers = new List<QuestionAnswers>();
            await _quizRepository.DeleteQuestionAnswers(question.Id);
            var addedAnswers = await AddAsnwers(answers);
            var targetAnswer = new List<Answer>();
            foreach (var text in answers)
            {
                targetAnswer.Add(await _quizRepository.GetAnswerByText(text));
            }
            foreach (var ans in targetAnswer)
            {
                questionAsnwers.Add(new QuestionAnswers() { QuestionId = question.Id, AnswerId = ans.Id });
            }
            await _quizRepository.AddAnswersOnQuestion(questionAsnwers);
            await _quizRepository.UpdateQuestion(question);
            return new ValidationResult() { Success = true, Description = "OK" };
        }
    }
}

