﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Lottery.DataStorage.Services;
using Lottery.Models.Models;
using Lottery.Models.Models.DataStorageModels;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryParticipants;
using Lottery.Models.Models.DataStorageModels.LotteryQuiz;
using Lottery.Models.Models.DataStorageModels.LotteryWiner;
using Lottery.Models.Models.DataStorageModels.Picture;
using Lottery.Models.Models.DataStorageModels.Prize;
using Lottery.Models.Models.DataStorageModels.User;
using Lottery.Models.Models.LotteryViewModels;

namespace Lottery.Manage.services.LotteryManage
{
    public interface ILotteryManageService
    {
        /// <summary>
        /// update lottery with id = model.LotteryId
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ValidationResult> SetLotteryPriziesAsync(SetPrizesViewModel model);

        /// <summary>
        /// set target image to the target lottery
        /// </summary>
        /// <param name="lotteryId">target lottery id</param>
        /// <param name="pictureId">target picture id</param>
        /// <returns></returns>
        Task SetLotteryImageAsync(int lotteryId, int pictureId);
         
        /// <summary>
        /// update lottery 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task UpdateLotteryAsync(LotteryCreatingViewModel model);

        Task<LotteryCreationResult> AddLotteryAsync(LotteryEntity lottery);

        /// <summary>
        /// Generate random winner for lottery with id = lotteryId;
        /// </summary>
        /// <param name="lotteryId"></param>
        /// <returns></returns>
        Task<LotteryEntity> LotteryRandomazer();

        /// <summary>
        /// Add currents user to list of participants to lottery with id = lottery id 
        /// </summary>
        /// <param name="userClaims">current user</param>
        /// <param name="lotteryId">target lottery id </param>
        /// <returns></returns>
        Task<bool> AddParticipants(ClaimsPrincipal userClaims, int lotteryId);

        /// <summary>
        /// Add target user to the target lottery participants
        /// </summary>
        /// <param name="lotteryId">target lottery id</param>
        /// <param name="email">target user email</param>
        /// <returns></returns>
        Task AddParticipantsByEmail(int lotteryId, string email);

        /// <summary>
        /// remove target user from the participants list of the target lottery
        /// </summary>
        /// <param name="lotteryId">target lottery id</param>
        /// <param name="email">target user email</param>
        /// <param name="removeReason"></param>
        /// <returns></returns>
        Task RemoveLotteryParticipantsByEmail(int lotteryId, string email, string removeReason);

        /// <summary>
        /// removes target user from from excluded users for target lottery
        /// </summary>
        /// <param name="lotteryId">target lottery id</param>
        /// <param name="userEmail">target user email</param>
        /// <returns></returns>
        Task RemoveUserFromExcluded(int lotteryId, string userEmail);

        /// <summary>
        /// add new question with text = questionText, question type = type, and answers = answers
        /// </summary>
        /// <param name="questionText"></param>
        /// <param name="type"></param>
        /// <param name="answers"></param>
        /// <returns></returns>
        Task<ValidationResult> AddNewQuestion(string questionText, QuestionType type, List<string> answers);

        /// <summary>
        /// add answers to the database
        /// </summary>
        /// <param name="answers"></param>
        /// <returns></returns>
        Task<List<Answer>> AddAsnwers(List<string> answers);

        /// <summary>
        /// returns user validation result which represent whether the user can participate in the target lottery lottery
        /// </summary>
        /// <param name="user">target user</param>
        /// <param name="lottery">target lottery</param>
        /// <returns></returns>
        Task<ValidationResult> CheckUserForLottery(ApplicationUser user,LotteryEntity lottery);

        /// <summary>
        /// returns user validation result which represent whether the user can participate in the target lottery lottery
        /// </summary>
        /// <param name="userClaims">target user</param>
        /// <param name="lotteryId"></param>
        /// <returns></returns>
        Task<ValidationResult> CheckUserForLottery(ClaimsPrincipal userClaims, int lotteryId);

        /// <summary>
        /// add new prize to the databse with target description
        /// </summary>
        /// <param name="description">target description</param>
        /// <returns></returns>
        Task AddPrize(string description);

        /// <summary>
        /// delete terget lottery
        /// </summary>
        /// <param name="lotteryId">target lottery id</param>
        /// <returns></returns>
        Task DeleteLottery(int lotteryId);

        Task<ValidationResult> UpdateLotteryAsync(LotteryEntity lottery);

        Task<ValidationResult> DeletePrize(int prizeId);

        Task<ValidationResult> UpdatePrize(LotteryPrize prize);

        Task<ValidationResult> DeleteQuestion(int questionId);

        Task<ValidationResult> UpdateQuestion(Question question, List<string> answers);
    }
}
