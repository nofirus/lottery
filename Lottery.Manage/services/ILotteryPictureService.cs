﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Lottery.Models.Models;
using Lottery.Models.Models.DataStorageModels.Picture;

namespace Lottery.Manage.services
{
    public interface ILotteryPictureService
    {
        Task<List<Picture>> GetAllPictures();
        Task<Picture> AddPicture(string path);
        Task<ValidationResult> DeletePicture(int pictureId, string rootPath);
    }
}
