﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lottery.DataStorage.Repositories.LotteryParticipantsRepository;
using Lottery.DataStorage.Repositories.LotteryRepository;
using Lottery.DataStorage.Repositories.LotteryWinerRepository;
using Lottery.DataStorage.Repositories.PictureRepository;
using Lottery.DataStorage.Repositories.PrizeRepository;
using Lottery.Manage.services.InfoCollector;
using Lottery.Manage.services.LotteryManage;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryParticipants;
using Lottery.Models.Models.DataStorageModels.Picture;
using Lottery.Models.Models.DataStorageModels.Prize;
using Lottery.Models.Models.DataStorageModels.Types;
using Lottery.Models.Models.DataStorageModels.User;
using Lottery.Models.Models.Excel;
using Lottery.Models.Models.LotteryViewModels;
using Microsoft.AspNetCore.Http;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Lottery.Manage.services
{
    public class ExcelDataService:IExcelDataService
    {
        private readonly ILotteryRepository _lotteryRepository;
        private readonly ILotteryInfoProviderService _infoProviderService;
        private readonly IPrizeRepository _prizeRepository;
        private readonly ILotteryParticipantsRepository _lotteryParticipantsRepository;
        private readonly ILotteryWinerRepository _lotteryWinerRepository;
        private readonly ILotteryManageService _lotteryManageService;
        private readonly IPictureRepository _pictureRepository;

        public ExcelDataService(ILotteryRepository lotteryRepository, ILotteryInfoProviderService infoProviderService, IPrizeRepository prizeRepository, ILotteryParticipantsRepository lotteryParticipantsRepository, ILotteryWinerRepository lotteryWinerRepository, ILotteryManageService lotteryManageService, IPictureRepository pictureRepository)
        {
            _lotteryRepository = lotteryRepository;
            _infoProviderService = infoProviderService;
            _prizeRepository = prizeRepository;
            _lotteryParticipantsRepository = lotteryParticipantsRepository;
            _lotteryWinerRepository = lotteryWinerRepository;
            _lotteryManageService = lotteryManageService;
            _pictureRepository = pictureRepository;
        }

        public async Task ExportLottery(string rootPath,LotteryEntity lottery)
        {
            var sWebRootFolder = rootPath;
            var fileName = DateTime.Now.ToString(CultureInfo.InvariantCulture).Replace('/','_').Replace(" ","_").Replace(":","_")+ "Lotteries.xlsx";
            var file = new FileInfo(Path.Combine(sWebRootFolder, fileName));
            var memory = new MemoryStream();
            using (var fs = new FileStream(Path.Combine(sWebRootFolder, fileName), FileMode.Create, FileAccess.Write))
            {
                var workbook = new XSSFWorkbook();
                var excelSheet = workbook.CreateSheet("Demo");
                var row = excelSheet.CreateRow(0);

                var type = typeof(LotteryEntity);
                var propeties = type.GetProperties();
                for (int i = 0; i < propeties.Length; i++)
                {
                    if (!propeties[i].PropertyType.IsGenericType)
                    {
                        row.CreateCell(i).SetCellValue(propeties[i].Name);
                    }
                }
                row = excelSheet.CreateRow(1);
                for (int i = 0; i < propeties.Length; i++)
                {
                    if (!propeties[i].PropertyType.IsGenericType)
                    {
                        if (propeties[i].PropertyType == typeof(Picture))
                        {
                            row.CreateCell(i).SetCellValue(lottery.Picture.Id);
                        }
                        else
                        {
                            row.CreateCell(i).SetCellValue(propeties[i].GetValue(lottery).ToString());
                        }
                    }
                }

                workbook.Write(fs);
            }
        }

        public async Task ExportLotteries(string rootPath, LotteryFilters filters)
        {
            var lotteries = await _infoProviderService.GetAllLotteries(filters);
            var sWebRootFolder = rootPath;
            var fileName = DateTime.Now.ToString(CultureInfo.InvariantCulture).Replace('/', '_').Replace(" ", "_").Replace(":", "_") + "Lotteries.xlsx";
            using (var fs = new FileStream(Path.Combine(sWebRootFolder, fileName), FileMode.Create,
                    FileAccess.Write))
            {
                var workbook = new XSSFWorkbook();
                var excelSheet = workbook.CreateSheet("Lotteries");
                var row = excelSheet.CreateRow(0);

                var type = typeof(LotteryEntity);
                var propeties = type.GetProperties();
                for (int i = 0; i < propeties.Length; i++)
                {
                    if (!propeties[i].PropertyType.IsGenericType)
                    {
                        row.CreateCell(i).SetCellValue(propeties[i].Name);
                    }
                }

                try
                {
                    dynamic value = 0;
                    var dataFormat = workbook.CreateDataFormat();
                    for (int k = 0; k < lotteries.Count; k++)
                    {
                        row = excelSheet.CreateRow(k + 1);
                        for (int i = 0; i < propeties.Length; i++)
                        {
                            if (!propeties[i].PropertyType.IsGenericType)
                            {
                                var cell = row.CreateCell(i);
                                if (propeties[i].PropertyType == typeof(Picture))
                                {
                                    cell.SetCellValue(lotteries[k].Picture.Id);
                                }
                                else if (propeties[i].PropertyType == typeof(LotteryEntity))
                                {
                                    cell.SetCellValue(lotteries[k].Description);
                                }
                                else if (propeties[i].PropertyType == typeof(int))
                                {
                                    cell.SetCellValue(Double.Parse(propeties[i].GetValue(lotteries[k]).ToString()));
                                }
                                else if (propeties[i].PropertyType== typeof(DateTime))
                                {
                                    cell.SetCellValue(propeties[i].GetValue(lotteries[k]).ToString());
                                }
                                else if(propeties[i].PropertyType == typeof(Boolean))
                                {
                                    cell.SetCellValue((bool)propeties[i].GetValue(lotteries[k]));
                                }
                                else
                                {
                                    cell.SetCellValue(propeties[i].GetValue(lotteries[k]).ToString());
                                }
                            }
                        }

                        await ExportAdditionalLotteryInfo(rootPath, lotteries[k]);
                    }
                }
                catch (Exception e)
                {

                }
               
                workbook.Write(fs);
            }
           
        }

        private async Task ExportAdditionalLotteryInfo(string rootPath, LotteryEntity lottery)
        {

            await ExportLotteryPrizies(rootPath, lottery);
            await ExportLotteryPartisipants(rootPath, lottery);
            await ExportLotteryWinners(rootPath, lottery);
        }

        private async Task ExportLotteryPrizies(string rootPath, LotteryEntity lottery)
        {
            var lotteryPrizies = await _prizeRepository.GetAllPriziesForLotteryAsync(lottery);
            var sWebRootFolder = rootPath;
            var fileName =
                DateTime.Now.ToString(CultureInfo.InvariantCulture).Replace('/', '_').Replace(" ", "_")
                    .Replace(":", "_") + $"Lottery_{lottery.Id}_Prizies.xlsx";
          
                using (var fs = new FileStream(Path.Combine(sWebRootFolder, fileName), FileMode.Create,
                    FileAccess.Write))
                {
                    var workbook = new XSSFWorkbook();
                    var excelSheet = workbook.CreateSheet("Lotteries");
                    var row = excelSheet.CreateRow(0);

                    var type = typeof(LotteryPrizeList);
                    var propeties = type.GetProperties();
                    for (int i = 0; i < propeties.Length; i++)
                    {
                            row.CreateCell(i).SetCellValue(propeties[i].Name);
                    }

                    for (int k = 0; k < lotteryPrizies.Count; k++)
                    {
                        row = excelSheet.CreateRow(k + 1);
                        for (int i = 0; i < propeties.Length; i++)
                        {
                            if (propeties[i].PropertyType == typeof(LotteryPrize))
                            {
                                var prize = typeof(LotteryPrize);
                                row.CreateCell(i).SetCellValue(prize.GetProperty("Description").GetValue(lotteryPrizies[k].Prize).ToString());
                            }
                            else if (propeties[i].PropertyType != typeof(LotteryEntity))
                            {
                                row.CreateCell(i).SetCellValue(propeties[i].GetValue(lotteryPrizies[k]).ToString());
                            }
                            else if (propeties[i].PropertyType == typeof(LotteryEntity))
                            {  
                                row.CreateCell(i).SetCellValue(lottery.Id);
                            }

                    }
                    }
                    workbook.Write(fs);
                }
        }
        private async Task ExportLotteryPartisipants(string rootPath, LotteryEntity lottery)
        {
            var lotteryParticipantses = await _lotteryParticipantsRepository.GetAllParticipantsForLottery(lottery);
            var sWebRootFolder = rootPath;
            var fileName =
                DateTime.Now.ToString(CultureInfo.InvariantCulture).Replace('/', '_').Replace(" ", "_")
                    .Replace(":", "_") + $"Lottery_{lottery.Id}_Participants.xlsx";

            using (var fs = new FileStream(Path.Combine(sWebRootFolder, fileName), FileMode.Create,
                FileAccess.Write))
            {
                var workbook = new XSSFWorkbook();
                var excelSheet = workbook.CreateSheet("Lotteries");
                var row = excelSheet.CreateRow(0);

                var type = typeof(LotteryParticipants);
                var propeties = type.GetProperties();
                for (int i = 0; i < propeties.Length; i++)
                {
                    if (!propeties[i].PropertyType.IsGenericType)
                    {
                        row.CreateCell(i).SetCellValue(propeties[i].Name);
                    }
                }

                for (int k = 0; k < lotteryParticipantses.Count; k++)
                {
                    row = excelSheet.CreateRow(k + 1);
                    for (int i = 0; i < propeties.Length; i++)
                    {
                        if (propeties[i].PropertyType == typeof(LotteryEntity))
                        {
                            row.CreateCell(i).SetCellValue(lottery.Id);
                        }
                        else
                        {
                            row.CreateCell(i).SetCellValue(propeties[i].GetValue(lotteryParticipantses[k]).ToString());
                        }
                    }
                }
                workbook.Write(fs);
            }
        }
        private async Task ExportLotteryWinners(string rootPath, LotteryEntity lottery)
        {
            var lotteryWinners = await _lotteryWinerRepository.GetLotteryWinners(lottery);
            var sWebRootFolder = rootPath;
            var fileName =
                DateTime.Now.ToString(CultureInfo.InvariantCulture).Replace('/', '_').Replace(" ", "_")
                    .Replace(":", "_") + $"Lottery_{lottery.Id}_Winners.xlsx";

            using (var fs = new FileStream(Path.Combine(sWebRootFolder, fileName), FileMode.Create,
                FileAccess.Write))
            {
                var workbook = new XSSFWorkbook();
                var excelSheet = workbook.CreateSheet("Lotteries");
                var row = excelSheet.CreateRow(0);

                var type = typeof(LotteryParticipants);
                var propeties = type.GetProperties();
                for (int i = 0; i < propeties.Length; i++)
                {
                    if (!propeties[i].PropertyType.IsGenericType)
                    {
                        row.CreateCell(i).SetCellValue(propeties[i].Name);
                    }
                }

                for (int k = 0; k < lotteryWinners.Count; k++)
                {
                    row = excelSheet.CreateRow(k + 1);
                    for (int i = 0; i < propeties.Length; i++)
                    {
                        if (propeties[i].PropertyType == typeof(LotteryEntity))
                        {
                            row.CreateCell(i).SetCellValue(lottery.Id);
                        }
                        else
                        {
                            row.CreateCell(i).SetCellValue(propeties[i].GetValue(lotteryWinners[k]).ToString());
                        }
                    }
                }
                workbook.Write(fs);
            }
        }

        public Task<LotteryCreationResult> ImportLottery(string rootPath, IFormFile file)
        {
            var folderName = "Upload";
            var webRootPath = rootPath;
            var newPath = Path.Combine(webRootPath, folderName);
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }
            if (file.Length > 0)
            {
                var lottery = new LotteryEntity();
                string sFileExtension = Path.GetExtension(file.FileName).ToLower();
                ISheet sheet;
                string fullPath = Path.Combine(newPath, file.FileName);
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                    stream.Position = 0;
                    if (sFileExtension == ".xls")
                    {
                        HSSFWorkbook hssfwb = new HSSFWorkbook(stream); //This will read the Excel 97-2000 formats  
                        sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook  
                    }
                    else
                    {
                        XSSFWorkbook hssfwb = new XSSFWorkbook(stream); //This will read 2007 Excel format  
                        sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook   
                    }

                  
                    int cellCount = sheet.GetRow(0).LastCellNum;
                  
                    var lotteryType = typeof(LotteryEntity).GetProperties();
                    try
                    {
                        for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
                        {
                            IRow row = sheet.GetRow(i);
                            if (row == null) continue;
                            if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;
                            for (int j = row.FirstCellNum; j < cellCount; j++)
                            {
                                if (row.GetCell(j) != null)
                                {
                                    dynamic value = 0;
                                    var cellType = row.GetCell(j).CellType;
                                    LotteryType lt;
                                    LotteryKind lk;
                                    OutputType ot;
                                    DateTime time;
                                  
                                    switch (cellType)
                                    {
                                        case CellType.String:
                                            var str = row.GetCell(j).StringCellValue;
                                            if (LotteryType.TryParse(str, out lt))
                                            {
                                                value = lt;
                                            }

                                            else if (LotteryKind.TryParse(str, out lk))
                                            {
                                                value = lk;
                                            }

                                            else if (OutputType.TryParse(str, out ot))
                                            {
                                                value = ot;
                                            }
                                            else if(DateTime.TryParse(str,out time))
                                            {
                                                value = time;
                                            }
                                            else
                                            {
                                                value = str;
                                            }

                                            break;
                                        case CellType.Numeric:
                                        {
                                            if (DateUtil.IsCellDateFormatted(row.GetCell(j)))
                                            {
                                                value = row.GetCell(j).DateCellValue;
                                            }
                                            else
                                            {
                                                value = (int) row.GetCell(j).NumericCellValue;
                                            }
                                            break;
                                        }
                                        case CellType.Boolean:
                                            value = row.GetCell(j).BooleanCellValue;
                                            break;
                                        case CellType.Unknown:
                                            break;
                                        case CellType.Formula:
                                            break;
                                        case CellType.Blank:
                                            break;
                                        case CellType.Error:
                                            break;
                                        default:
                                            value = row.GetCell(j).DateCellValue;
                                            break;
                                    }

                                    if (lotteryType[j].PropertyType == typeof(Picture))
                                    {
                                        var picture = _pictureRepository.GetPictureById((int)value).Result;
                                        lotteryType[j].SetValue(lottery, picture);
                                    }
                                    else
                                    {
                                        lotteryType[j].SetValue(lottery, value);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        var tcs = new TaskCompletionSource<LotteryCreationResult>();
                        tcs.SetResult(new LotteryCreationResult() { Succsess = false, Message = e.Message });
                        return tcs.Task;
                    }
                    
                }

                lottery.Id = 0;
                var result = _lotteryManageService.AddLotteryAsync(lottery);
                return result;
            }
            var defaultTcs = new TaskCompletionSource<LotteryCreationResult>();
            defaultTcs.SetResult(new LotteryCreationResult(){Succsess = false,Message = "no file"});
            return defaultTcs.Task;
        }
    }
}
