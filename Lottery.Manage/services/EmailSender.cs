﻿using System;
using System.IO;
using System.Net.Mail;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Lottery.Models.Models.DataStorageModels.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;

namespace Lottery.Manage.services
{
    public class EmailSender
    {
        private string UserName { get; set; }
        private string Password { get; set; }
        private string MailTo { get; set; }
        private string TestUserEmail { get; set; }
        private UserManager<ApplicationUser> _userManager;

        public EmailSender(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();
            UserName = configuration["UserName"];
            Password = configuration["Password"];
            MailTo = configuration["MailTo"];
            TestUserEmail = configuration["TestUserEmail"];

        }
        public Task SendEmail(string text, ApplicationUser targetUser)
        {
            MailMessage message = new MailMessage();
            message.To.Add(new MailAddress(MailTo, targetUser.Name));// in real project replace on this-> message.To.Add(new MailAddress(targetUser.Email, targetUser.Name)); 
            message.From = new MailAddress(UserName, "ssonic1");
            message.Subject = "Azure Web App Email";
            message.Body = text;
            message.IsBodyHtml = true;
            SmtpClient client = new SmtpClient
            {
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential(UserName, Password),
                Port = 587,
                Host = "smtp.gmail.com",
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = true
            };
            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }

            return Task.CompletedTask;
        }
        public async Task SendEmailConfirmationAsync( string email, string link)
        {
            var user = await _userManager.FindByEmailAsync(email);
            await SendEmail($"Confirm your email Please confirm your account by clicking this link: <a href='{HtmlEncoder.Default.Encode(link)}'>link</a>",user);
        }

        public async Task SendTwoFactorCode(ApplicationUser user, string code)
        {
            await SendEmail(string.Format("<h3>Code {0}</h3>",code), user);
        }
    }
}
