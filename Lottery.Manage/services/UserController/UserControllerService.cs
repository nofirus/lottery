﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Lottery.DataStorage.Repositories.UserRepository;
using Lottery.Models.Models.AccountViewModels;
using Lottery.Models.Models.DataStorageModels.Roles;
using Lottery.Models.Models.DataStorageModels.User;
using Microsoft.AspNetCore.Identity;

namespace Lottery.Manage.services.UserController
{
    public class UserControllerService:IUserControllerService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IUserRepository _userRepository;
        private readonly EmailSender _emailSender;

        public UserControllerService(IUserRepository userRepository, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, EmailSender emailSender)
        {
            _userRepository = userRepository;
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
        }


        /// <summary>
        /// update personal info of current user according on application user param;
        /// 
        /// </summary>
        /// <param name="userClaims"></param>
        /// <param name="applicationUser"></param>
        /// <returns></returns>
        public async Task UpdateUserInfo(ClaimsPrincipal userClaims,ApplicationUser applicationUser)
        {
            
            var user = await GetUserAsync(userClaims);
            user.Name = applicationUser.Name;
            user.Surname = applicationUser.Surname;
            user.Address = applicationUser.Address;
            user.PhoneNumber = applicationUser.PhoneNumber;
            user.DateOfBirth = applicationUser.DateOfBirth;
            await _userManager.UpdateAsync(user);
        }
        /// <summary>
        /// update user with email =  emailOfTargetUser;
        /// </summary>
        /// <param name="emailOfTargetUser"></param>
        /// <param name="applicationUser">new info of user</param>
        /// <returns></returns>
        public async Task UpdateUserInfo(string emailOfTargetUser, ApplicationUser applicationUser)
        {
            var user = await GetUserByEmailAsync(emailOfTargetUser);
            user.Name = applicationUser.Name;
            user.Surname = applicationUser.Surname;
            user.Address = applicationUser.Address;
            user.PhoneNumber = applicationUser.PhoneNumber;
            user.DateOfBirth = applicationUser.DateOfBirth;
            await _userManager.UpdateAsync(user);
        }

        /// <summary>
        /// try to create new user; on succeed return true, else return false
        /// </summary>
        /// <param name="user">user to create</param>
        /// <param name="password">user password</param>
        /// <param name="scheme"></param>
        /// <returns>bool</returns>
        public async Task<IdentityResult> RegisterNewUser(ApplicationUser user, string password)
        {
            var result = await _userManager.CreateAsync(user, password);
            if (!result.Succeeded) return result;
            var passwordHasher = _userManager.PasswordHasher;
            var currentPasswordHash = passwordHasher.HashPassword(user, password);
            await _userRepository.AddOldPasswordHash(currentPasswordHash, user.Id);
            await _userManager.AddToRoleAsync(user, "User");
            await _signInManager.SignInAsync(user, isPersistent: false);
            return result;
        }
        /// <summary>
        /// return currtent user presented as ApplicationUser
        /// </summary>
        /// <param name="userClaims"></param>
        /// <returns></returns>
        public async Task<ApplicationUser> GetUserAsync(ClaimsPrincipal userClaims)
        {
            var user = await _userManager.GetUserAsync(userClaims);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(userClaims)}'.");
            }

            return user;
        }

        public async Task BlockUserAsync(string userEmail, string reason)
        {
            var user = await _userRepository.GetUserByEmailAsync(userEmail);
            user.BlockReason = reason;
            await _userManager.SetLockoutEndDateAsync(user, new DateTimeOffset(DateTime.Now.Add(TimeSpan.FromMinutes(5))));
            await _userManager.UpdateAsync(user);

        }

        public async Task<SignInResult> PasswordSignInAsync(LoginViewModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            if(user!=null)
            {
                if ( await _userManager.IsInRoleAsync(user,Roles.Admin.ToString()))
                {
                   return await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, true);
                }
            }
            var result = await _signInManager.PasswordSignInAsync(model.Email.Split('@')[0], model.Password, model.RememberMe, true);
            return result;
        }

        public async Task<IdentityResult> ResetPasswordAsync(ResetPasswordViewModel model)
        {
            var user =await GetUserByEmail(model.Email);
            return  await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
        }

        public async Task SignOutAsync()
        {
           await _signInManager.SignOutAsync();
        }

        public Task<ApplicationUser> GetUserByEmail(string email)
        {
            return _userManager.FindByEmailAsync(email);
        }

        public Task<string> GeneratePasswordResetTokenAsync(ApplicationUser user)
        {
            return _userManager.GeneratePasswordResetTokenAsync(user);
        }

        public async Task UnlockUserAsync(string userEmail)
        {
            var user = await _userManager.FindByEmailAsync(userEmail);
            await _userManager.SetLockoutEndDateAsync(user,DateTimeOffset.Now);
            await _userManager.UpdateAsync(user);
        }

        public async Task<IdentityResult> UpdatePassword(ClaimsPrincipal userClaims, string oldPassword, string newPassword)
        {
            var user = await  GetUserAsync(userClaims);
            var passwordHasher = _userManager.PasswordHasher;
            var oldPasswrodHash = passwordHasher.HashPassword(user, oldPassword);
            if (passwordHasher.VerifyHashedPassword(user, oldPasswrodHash, newPassword) ==
                PasswordVerificationResult.Success)
            {
                return IdentityResult.Failed(new IdentityError() { Code = "WrongPassword", Description = "new password can't be the same as previous" });
            }

            if (user.OldPassword != null)
            {
                if (passwordHasher.VerifyHashedPassword(user, user.OldPassword, newPassword) ==
                    PasswordVerificationResult.Success)
                {
                    return IdentityResult.Failed(new IdentityError() { Code = "DuplicatePasswords", Description = "you already used this password earlier" });
                }
            }

            if (user.OldPassword2 != null)
            {
                if (passwordHasher.VerifyHashedPassword(user, user.OldPassword2, newPassword) ==
                    PasswordVerificationResult.Success)
                {
                    return IdentityResult.Failed(new IdentityError() { Code = "DuplicatePasswords", Description = "you already used this password earlier" });
                }
            }

            var changePasswordResult = await _userManager.ChangePasswordAsync(user, oldPassword, newPassword);
            if (!changePasswordResult.Succeeded)
            {
                return IdentityResult.Failed(new IdentityError(){Code = "WrongPassword",Description = "Wrong curretn password"});
            }
            await _signInManager.SignInAsync(user, false);

            await _userRepository.AddOldPassword2Hash(user.OldPassword, user.Id);
            await _userRepository.AddOldPasswordHash(oldPasswrodHash, user.Id);

            return changePasswordResult;
        }

        public async Task<bool> HassPasswordAsync(ClaimsPrincipal userClaims)
        {
            var user = await GetUserAsync(userClaims);
            return await _userManager.HasPasswordAsync(user);
        }

        public async Task<List<ApplicationUser>> GetAllUsersAsync()
        {
            return await _userRepository.GetAllUsersAsync();
        }

        public async Task<ApplicationUser> GetUserByEmailAsync(string email)
        {
            return await _userManager.FindByEmailAsync(email);
        }

        public async Task<List<UserRole>> GetUserRolesAsync(ApplicationUser applicationUser)
        {
            var roleStringList = await _userManager.GetRolesAsync(applicationUser);
            return  roleStringList.Select(role => new UserRole(role)).ToList();
        }

        public async Task AddRoleToUserAsync(string userEmail, string roleName)
        {
            var user = await GetUserByEmailAsync(userEmail);
            await _userManager.AddToRoleAsync(user, roleName);
        }

        public async Task<IdentityResult> RemoveUserFromRole(string userEmail, string roleName)
        {
            var user = await _userManager.FindByEmailAsync(userEmail);
            return await _userManager.RemoveFromRoleAsync(user, roleName);
        }

        public Task SendPasswordResetTokenAsync(ApplicationUser user, string token)
        {
           return _emailSender.SendEmail(token, user);
        }

        public Task<ApplicationUser> GetTwoFactorAuthenticationUserAsync()
        {
            return _signInManager.GetTwoFactorAuthenticationUserAsync();
        }

        public async Task EnableEmailAuthenticator(ClaimsPrincipal userClaims)
        {
            var user = await _userManager.GetUserAsync(userClaims);
            await _userRepository.SetEnableEmailAuthenticator(user, true);
            await _userManager.SetTwoFactorEnabledAsync(user, true);
        }

        public Task<IdentityResult> DisableTwoFactor(ApplicationUser user)
        {
            var tcs = new TaskCompletionSource<IdentityResult>();
            if (user.TwoFactorEnabled)
            {
                _userRepository.SetEnableEmailAuthenticator(user, false);
                return _userManager.SetTwoFactorEnabledAsync(user, false);
            }

            if (user.TwoFactorEmailEnabled)
            {
                try
                {
                    _userRepository.SetEnableEmailAuthenticator(user, false);
                    return _userManager.SetTwoFactorEnabledAsync(user, false);
                }
                catch (Exception e)
                {
                    tcs.SetResult(IdentityResult.Failed(new IdentityError { Code = "TwoFactorEmailFailed", Description = "TwoFactorEmailFailed " }));
                    return tcs.Task;
                }
              
            }
            tcs.SetResult(IdentityResult.Failed(new IdentityError{Code = "TwoFactorDisableFailed", Description = "TwoFactorEnabled = false, EnableEmailAuthenticator = false " }));
            return tcs.Task;
        }

       
        public Task<SignInResult> TwoFactorSignInAsync(string code, bool rememberMe, bool rememberMachine,
            ApplicationUser user)
        {
            return user.TwoFactorEmailEnabled ? _signInManager.TwoFactorSignInAsync("EmailTwoFactor", code, rememberMe, rememberMachine) : _signInManager.TwoFactorAuthenticatorSignInAsync(code, rememberMe, rememberMachine);
        }

        public async Task AddManagerRights(string userEmail)
        {
            var user = await GetUserByEmailAsync(userEmail);
            await _userManager.AddToRoleAsync(user, "Manager");
        }
    }
}
