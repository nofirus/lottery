﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Lottery.Models.Models.AccountViewModels;
using Lottery.Models.Models.DataStorageModels.Roles;
using Lottery.Models.Models.DataStorageModels.User;
using Microsoft.AspNetCore.Identity;

namespace Lottery.Manage.services.UserController
{
    public interface IUserControllerService
    {
        Task UpdateUserInfo(ClaimsPrincipal userClaims, ApplicationUser applicationUser);
        Task UpdateUserInfo(string emailOfTargetUser, ApplicationUser applicationUser);
        Task<IdentityResult> RegisterNewUser(ApplicationUser user, string password);
        Task<ApplicationUser> GetUserAsync(ClaimsPrincipal userClaims);
        Task BlockUserAsync(string userEmail, string reason);

        /// <summary>
        /// return true if user loggined false if not
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<SignInResult> PasswordSignInAsync(LoginViewModel model);
        
        Task<IdentityResult> ResetPasswordAsync(ResetPasswordViewModel model);
        
        /// <summary>
        /// signi out current user
        /// </summary>
        /// <returns></returns>
        Task SignOutAsync();

        /// <summary>
        /// retunt user by email;
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        Task<ApplicationUser> GetUserByEmail(string email);

        Task<string> GeneratePasswordResetTokenAsync(ApplicationUser user);

        Task UnlockUserAsync(string userEmail);

        Task<List<UserRole>> GetUserRolesAsync(ApplicationUser applicationUser);
        /// <summary>
        /// add a manager role to the target user
        /// </summary>
        /// <param name="userEmail"></param>
        /// <returns></returns>
        Task AddManagerRights(string userEmail);
        /// <summary>
        /// returns true if user has a password, otherwise return false
        /// </summary>
        /// <param name="userClaims"></param>
        /// <returns></returns>
        Task<bool> HassPasswordAsync(ClaimsPrincipal userClaims);
        Task<List<ApplicationUser>> GetAllUsersAsync();
        Task<ApplicationUser> GetUserByEmailAsync(string email);
        Task<IdentityResult> UpdatePassword(ClaimsPrincipal userClaims, string oldPassword, string newPassword);
        /// <summary>
        /// add a target role to the target user
        /// </summary>
        /// <param name="userEmail">target user</param>
        /// <param name="roleName">target role</param>
        /// <returns></returns>
        Task AddRoleToUserAsync(string userEmail, string roleName);

        /// <summary>
        /// remove target user from target role
        /// </summary>
        /// <param name="userEmail">target user email</param>
        /// <param name="roleName">target role name</param>
        /// <returns></returns>
        Task<IdentityResult> RemoveUserFromRole(string userEmail, string roleName);

        Task SendPasswordResetTokenAsync(ApplicationUser user, string token);

        Task<ApplicationUser> GetTwoFactorAuthenticationUserAsync();

        Task EnableEmailAuthenticator(ClaimsPrincipal userClaims);

        Task<IdentityResult> DisableTwoFactor(ApplicationUser user);

        Task<SignInResult> TwoFactorSignInAsync(string code, bool rememberMe, bool rememberMachine,
            ApplicationUser user);
    }
}
