﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lottery.DataStorage.Repositories.ExcludedFromTheLotteryUsersRepository;
using Lottery.DataStorage.Repositories.LotteryParticipantsRepository;
using Lottery.DataStorage.Repositories.LotteryRepository;
using Lottery.DataStorage.Repositories.LotteryWinerRepository;
using Lottery.DataStorage.Repositories.PictureRepository;
using Lottery.DataStorage.Repositories.PrizeRepository;
using Lottery.DataStorage.Repositories.QuizRepository;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.Prize;
using Lottery.Models.Models.DataStorageModels.User;
using Microsoft.AspNetCore.Identity;

namespace Lottery.Manage.services.Prize
{
    public class LotteryPrizeService:ILotteryPrizeService
    {
        private readonly ILotteryRepository _lotteryRepository;
        private readonly ILotteryParticipantsRepository _lotteryParticipantsRepository;
        private readonly ILotteryWinerRepository _lotteryWinerRepository;
        private readonly IQuizRepository _quizRepository;
        private readonly IPrizeRepository _prizeRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IExcludedFromTheLotteryUsersRepository _excludedFromTheLotteryUsersRepository;
        private readonly IPictureRepository _pictureRepository;

        public LotteryPrizeService(ILotteryRepository lotteryRepository, ILotteryParticipantsRepository lotteryParticipantsRepository, 
            ILotteryWinerRepository lotteryWinerRepository, IQuizRepository quizRepository, 
            IPrizeRepository prizeRepository, UserManager<ApplicationUser> userManager,
            IExcludedFromTheLotteryUsersRepository excludedFromTheLotteryUsersRepository, IPictureRepository pictureRepository)
        {
            _lotteryRepository = lotteryRepository;
            _lotteryParticipantsRepository = lotteryParticipantsRepository;
            _lotteryWinerRepository = lotteryWinerRepository;
            _quizRepository = quizRepository;
            _prizeRepository = prizeRepository;
            _userManager = userManager;
            _excludedFromTheLotteryUsersRepository = excludedFromTheLotteryUsersRepository;
            _pictureRepository = pictureRepository;
        }

        public async Task<List<LotteryPrize>> GetLotteryPrizies(int lotteryId)
        {
            var lottery = await _lotteryRepository.GetLotteryByIdAsync(lotteryId);
            var prizeis = await _prizeRepository.GetAllPriziesForLotteryAsync(lottery);
            return prizeis.OrderBy(x => x.Place).Select(x => x.Prize).ToList();
        }

        public Task<List<LotteryPrize>> GetGroupLotteryPrizies(int lotteryId, out List<int> leftBorderGroup, out List<int> rightBorderGroup)
        {
            var lottery = _lotteryRepository.GetLotteryByIdAsync(lotteryId).Result;
            var prizeis = _prizeRepository.GetAllPriziesForLotteryAsync(lottery).Result.OrderBy(x => x.Place).ToList();
            var groupedPrizies = prizeis.OrderBy(x => x.Place).GroupBy(x => x.PrizeId).ToList();
            var currentGroupedPrizies = new List<LotteryPrize>();
            foreach (var elem in groupedPrizies)
            {
                if (groupedPrizies.Count == 1)
                {
                    foreach (var subElem in elem)
                    {
                        currentGroupedPrizies.Add(subElem.Prize);
                    }
                }
                else
                {
                    currentGroupedPrizies.Add(elem.FirstOrDefault()?.Prize);
                }
            }
            var tcs = new TaskCompletionSource<List<LotteryPrize>>();
            tcs.SetResult(currentGroupedPrizies);
            var leftBorder = new List<int>();

            var rightBorder = new List<int>();
            if (prizeis.Any())
            {


                leftBorder.Add(1);
                for (var i = 0; i < prizeis.Count - 1; i++)
                {
                    if (prizeis[i].PrizeId == prizeis[i + 1].PrizeId) continue;
                    rightBorder.Add(prizeis[i].Place);
                    leftBorder.Add(prizeis[i].Place + 1);
                }
                rightBorder.Add(prizeis.ElementAt(prizeis.Count - 1).Place);
                if (rightBorder.Count == 1 && leftBorder.Count == 1)
                {
                    rightBorder.Clear();
                    leftBorder.Clear();
                    for (int i = 0; i < prizeis.Count; i++)
                    {
                        leftBorder.Add(i + 1);
                        rightBorder.Add(i + 1);
                    }
                }
            }

            leftBorderGroup = leftBorder;
            rightBorderGroup = rightBorder;
            return tcs.Task;
        }

        public Task<LotteryPrize> GetPrizeById(int prizeId)
        {
            return _prizeRepository.GetPrizeByIdAsync(prizeId);
        }

        public async Task<LotteryPrize> GetPrizeForPlace(LotteryEntity lottery, int place)
        {
            var allLotteryPrizies = await _prizeRepository.GetAllPriziesForLotteryAsync(lottery);
            var prizeId = allLotteryPrizies.Where(x => x.Place.Equals(place)).Select(x => x.Place).FirstOrDefault();
            return await _prizeRepository.GetPrizeByIdAsync(prizeId);
        }

        public Task<List<LotteryPrize>> GetAllPrizeis()
        {
            return _prizeRepository.GetAllPriziesAsync();
        }
    }
}
