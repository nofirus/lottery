﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.Prize;

namespace Lottery.Manage.services.Prize
{
    public interface ILotteryPrizeService
    {

        /// <summary>
        /// return target prize by id;
        /// </summary>
        /// <param name="prizeId">target prize id</param>
        /// <returns></returns>
        Task<LotteryPrize> GetPrizeById(int prizeId);

        /// <summary>
        /// if target place  exist in target lottery return prize for target palce and target lottery, otherwise return null;
        /// </summary>
        /// <param name="lottery">target lottery</param>
        /// <param name="place">target place</param>
        /// <returns></returns>
        Task<LotteryPrize> GetPrizeForPlace(LotteryEntity lottery, int place);

        /// <summary>
        /// returns list of all prizies;
        /// </summary>
        /// <returns></returns>
        Task<List<LotteryPrize>> GetAllPrizeis();

        /// <summary>
        /// return prizies for target lottery if there is;
        /// </summary>
        /// <param name="lotteryId">target lottery id</param>
        /// <returns></returns>
        Task<List<LotteryPrize>> GetLotteryPrizies(int lotteryId);

        Task<List<LotteryPrize>> GetGroupLotteryPrizies(int lotteryId, out List<int> leftBorderGroup,
            out List<int> rightBorderGroup);
    }
}
