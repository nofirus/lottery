﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Lottery.Models.Models;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryQuiz;
using Lottery.Models.Models.LotteryViewModels;

namespace Lottery.Manage.services.LotteryQuiz
{
    public interface ILotteryQuizService
    {
        
        /// <summary>
        /// retuns all Questions
        /// </summary>
        /// <returns></returns>
        Task<List<Question>> GetAllQuestions();

        Task<Question> GetQuestionById(int questionId);

        Task<List<Answer>> GetAllAnwserOnTheQuestion(Question question);

        /// <summary>
        /// return ture is lottey type = quiz, otherwise return false
        /// </summary>
        /// <param name="lotteryId"></param>
        /// <returns></returns>
        Task<bool> IsLotteryHasQuiz(int lotteryId);

       
        Task AddNewQuiz(string description, IEnumerable<int> selectedQuestions);

        /// <summary>
        /// returns List of answersId on each question 
        /// </summary>
        /// <param name="questions"></param>
        /// <returns></returns>
        Task<List<List<Answer>>> GetAnswersOnTheQuestion(List<Question> questions);

        /// <summary>
        /// add to database user anwers on the quiz
        /// </summary>
        /// <param name="model"></param>
        ///   /// <param name="userClaims"></param>
        /// <returns></returns>
        Task SaveQuizAnswers(QuizViewModel model, ClaimsPrincipal userClaims);

        Task AddAnswersOnTheQuiz(int quizId,ClaimsPrincipal userclaims,IEnumerable<int> answersId, IEnumerable<int> quizQuestionsesId, IEnumerable<string> customAnswers);

        Task<Quiz> GetQuizForLottery(LotteryEntity lottery);

        Task<List<Question>> GetAllQuestionForQuiz(Quiz quiz);

        Task<List<LotteryQuizzes>> GetAllLotteryQuizzes();

        Task<List<Quiz>> GetAllQuizzes();

        Task<ValidationResult> DeleteQuiz(int quizId);

        Task<ValidationResult> UpdateQuiz(Quiz quiz, IEnumerable<int> selectedQuestions);

        Task<Quiz> GetQuizById(int quizId);

        Task AddQuizToLottery(int lotteryId, int quizId);

        Task<List<LotteryEntity>> GetAllLotteriesForQuiz(Quiz quiz);
    }
}
