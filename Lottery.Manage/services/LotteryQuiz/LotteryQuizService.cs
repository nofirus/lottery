﻿using System;
using Lottery.DataStorage.Repositories.LotteryRepository;
using Lottery.DataStorage.Repositories.QuizRepository;
using Lottery.Models.Models.DataStorageModels.Types;
using Lottery.Models.Models.DataStorageModels.User;
using Lottery.Models.Models.LotteryViewModels;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Lottery.Models.Models;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryQuiz;

namespace Lottery.Manage.services.LotteryQuiz
{
    public class LotteryQuizService:ILotteryQuizService
    {
        private readonly ILotteryRepository _lotteryRepository;
     
        private readonly UserManager<ApplicationUser> _userManager;
      
        private readonly IQuizRepository _quizRepository;

        public LotteryQuizService(ILotteryRepository lotteryRepository, UserManager<ApplicationUser> userManager, IQuizRepository quizRepository)
        {
            _lotteryRepository = lotteryRepository;
            _userManager = userManager;
            _quizRepository = quizRepository;
        }
       
        public Task<bool> IsLotteryHasQuiz(int lotteryId)
        {
            var lottery = _lotteryRepository.GetLotteryByIdAsync(lotteryId).Result;
            var tsc = new TaskCompletionSource<bool>();
            tsc.SetResult(lottery.LotteryKind.Equals(LotteryKind.Quiz));
            return tsc.Task;
        }

        public async Task AddNewQuiz(string description,IEnumerable<int> selectedQuestions)
        {
            var quiz = await _quizRepository.AddQuiz(new Quiz(){Description = description});
            var quizQuestion = selectedQuestions.Select(questionId => new QuizQuestions() { QuestionId = questionId, QuizId = quiz.Id }).ToList();
            await _quizRepository.SetQuizQuestion(quizQuestion);
        }

        public async Task SaveQuizAnswers(QuizViewModel model, ClaimsPrincipal userClaims)
        {
            var user = await _userManager.GetUserAsync(userClaims);
            var customAnswers = model.CustomAnswers;
            var answers = model.Answers.Select(x => _quizRepository.GetAnswerById(x).Result.Text).ToList();
            answers.AddRange(customAnswers);
            var completedquizes = new List<CompletedQuizzes>();
            for (var i = 0; i < model.QuizQuestionsesId.Count(); i++)
            {
                completedquizes.Add(new CompletedQuizzes()
                {
                    QuizId = model.QuizId
                    ,
                    QuestionId = model.QuizQuestionsesId.ElementAt(i)
                    ,
                    AnswerText = answers.ElementAt(i),
                    ApplicationUser = user
                });
            }
            await _quizRepository.AddQuizAnswers(completedquizes);
        }

        public async Task AddAnswersOnTheQuiz(int quizId, ClaimsPrincipal userclaims, IEnumerable<int> answersId, IEnumerable<int> quizQuestionsesId, IEnumerable<string> customAnswers)
        {
            var complitedQuiz = new List<CompletedQuizzes>();
            var user = await _userManager.GetUserAsync(userclaims);
            var quiz = await _quizRepository.GetQuizById(quizId);
            var quizQuestion = await _quizRepository.GetAllQuestionForQuiz(quiz);
            var answers = new List< Task<Answer>>();
            if (answersId!=null)
            {
                answers = answersId.Select(async x => await _quizRepository.GetAnswerById(x)).ToList();
            }
            var cAnswers = new List<string>();
            if (customAnswers!=null)
            {
                cAnswers = customAnswers.ToList().Select(x => x).ToList();
            }
            string textAnswer = "";
            for (int i = 0; i < quizQuestion.Count(); i++)
            {
                if (quizQuestion.ElementAt(i).Type.Equals(QuestionType.Close))
                {
                    textAnswer = answers.ElementAt(0).Result.Text;
                    answers.RemoveAt(0);
                }

                if (quizQuestion.ElementAt(i).Type.Equals(QuestionType.Open))
                {
                    textAnswer = cAnswers.ElementAt(0);
                    cAnswers.RemoveAt(0);
                }
                var complQuiz = new CompletedQuizzes()
                {
                    ApplicationUser = user,
                    Quiz = quiz,
                    Question = quizQuestion.ElementAt(i),
                    AnswerText = textAnswer
                };
                complitedQuiz.Add(complQuiz);
            }
           await _quizRepository.AddQuizAnswers(complitedQuiz);
        }

        public Task<Quiz> GetQuizForLottery(LotteryEntity lottery)
        {
            return _quizRepository.GetQuizForLottery(lottery);
        }

        public Task<List<Question>> GetAllQuestionForQuiz(Quiz quiz)
        {
            return _quizRepository.GetAllQuestionForQuiz(quiz);
        }

        public Task<List<LotteryQuizzes>> GetAllLotteryQuizzes()
        {
            return _quizRepository.GetAllLotteryQuizzes();
        }

        public Task<List<Quiz>> GetAllQuizzes()
        {
            return _quizRepository.GetAllQuizzes();
        }

        public Task AddQuiz(Quiz quiz)
        {
           return _quizRepository.AddQuiz(quiz);
        }

        public async Task<ValidationResult> DeleteQuiz(int quizId)
        {
            var quiz = await _quizRepository.GetQuizById(quizId);
            var lotteryForQuiz = await _quizRepository.GetAllLotteryForQuiz(quiz);
            var message = new StringBuilder();
            lotteryForQuiz.ForEach(x => message.Append(x.Description).Append(":").Append(x.LotteryDate).Append(";"));
            if (lotteryForQuiz.Any())
            {
                return new ValidationResult(){Success = false,Description = string.Format("this quiz is used in: {0}",message+" ") };
            }

           await _quizRepository.DeleteQuizQuestions(quizId);
           await _quizRepository.DeleteQuiz(quiz);
           return new ValidationResult(){Success = true,Description = "Quiz deleted"};
        }

        public async Task<ValidationResult> UpdateQuiz(Quiz quiz, IEnumerable<int> selectedQuestions)
        {
            var lotteryForQuiz = await _quizRepository.GetAllLotteryForQuiz(quiz);
            var message = new StringBuilder();
            lotteryForQuiz.ForEach(x => message.Append(x.Description).Append(":").Append(x.LotteryDate).Append(";"));
            if (lotteryForQuiz.Any())
            {
                return new ValidationResult() { Success = false, Description = string.Format("this quiz is used in: {0}", message + " ") };
            }
            await _quizRepository.DeleteQuizQuestions(quiz.Id);
            var quizQuestion = selectedQuestions.Select(questionId => new QuizQuestions() { QuestionId = questionId, QuizId = quiz.Id }).ToList();
            await _quizRepository.SetQuizQuestion(quizQuestion);
            await _quizRepository.UpdateQuiz(quiz);
            return new ValidationResult(){Success = true, Description = "quiz updated"};
        }

        public Task<Quiz> GetQuizById(int quizId)
        {
            return _quizRepository.GetQuizById(quizId);
        }

        public async Task AddQuizToLottery(int lotteryId, int quizId)
        {
            var lottery = await _lotteryRepository.GetLotteryByIdAsync(lotteryId);
            var quiz = await _quizRepository.GetQuizById(quizId);
            var oldQuiz = await _quizRepository.GetQuizForLottery(lottery);
            if (oldQuiz != null)
            {
               await _quizRepository.RemoveLotteryQuiz(lotteryId,oldQuiz.Id);
            }
            var lotteryQuiz = new LotteryQuizzes() { Quiz = quiz,Lottery = lottery};
            await _quizRepository.AddLotteryQuizzes(lotteryQuiz);
        }

        public Task<List<LotteryEntity>> GetAllLotteriesForQuiz(Quiz quiz)
        {
            return _quizRepository.GetAllLotteryForQuiz(quiz);
        }

        public async Task<List<List<Answer>>> GetAnswersOnTheQuestion(List<Question> questions)
        {
            var answers = new List<List<Answer>>();
            for (var i = 0; i < questions.Count(); i++)
            {
                var answerquestion = await _quizRepository.GetAllAnswersOnTheQuestion(questions.ElementAt(i));
                answers.Add(new List<Answer>());
                foreach (var ans in answerquestion)
                {
                    answers.ElementAt(i).Add(await _quizRepository.GetAnswerById(ans.AnswerId));
                }
            }
            return answers;
        }

        public Task<List<Question>> GetAllQuestions()
        {
            return _quizRepository.GetAllQuestions();
        }

        public Task<Question> GetQuestionById(int questionId)
        {
            return _quizRepository.GetQuestionById(questionId);
        }

        public Task<List<Answer>> GetAllAnwserOnTheQuestion(Question question)
        {
            return _quizRepository.GetAllAnswerOnTheQuestion(question);
        }
    }
}
