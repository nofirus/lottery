﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lottery.DataStorage.Repositories.PictureRepository;
using Lottery.Models.Models;
using Lottery.Models.Models.DataStorageModels.Picture;
using Lottery.Models.Models.LotteryViewModels;

namespace Lottery.Manage.services
{
    public class LotteryPictureService:ILotteryPictureService
    {
        private readonly IPictureRepository _pictureRepository;

        public LotteryPictureService(IPictureRepository pictureRepository)
        {
            _pictureRepository = pictureRepository;
        }

        public Task<List<Picture>> GetAllPictures()
        {
           return _pictureRepository.GetAllPictures();
        }

        public Task<Picture> AddPicture(string path)
        {
           var picture = new Picture(){Pass = path};
           return _pictureRepository.AddPicture(picture);
        }

        public async Task<ValidationResult> DeletePicture(int pictureId, string rootPath)
        {
            var picture = await _pictureRepository.GetPictureById(pictureId);
            var lotteriesWithPicture = await _pictureRepository.GetAllLotteriesForPicture(picture);
            var message = new StringBuilder();
            foreach (var lottery in lotteriesWithPicture)
            {
                message.Append(lottery.Description + ": " + lottery.LotteryDate).Append(Environment.NewLine);
            }
            if (lotteriesWithPicture.Any())
            {
                return new ValidationResult(){Description = "can't delete picture because it assugned to the lotteries: " + message, Success = false};
            }
            await _pictureRepository.DeletePicture(picture);
            File.Delete(rootPath+picture.Pass);
            return new ValidationResult(){Success = true,Description = "OK"};
        }
    }
}
