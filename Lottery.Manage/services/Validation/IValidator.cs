﻿using System;
using System.Collections.Generic;
using System.Text;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.User;

namespace Lottery.Manage.services.Validation
{
    public interface IValidator
    {
        bool Validate(ApplicationUser user, LotteryEntity lottery);
    }
}
