﻿using System;
using System.Collections.Generic;
using System.Text;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.Types;

namespace Lottery.Manage.services.Validation
{
    public class UserLotteryValidator
    {
        public static IValidator GetValidatorForLottery(LotteryEntity lottery)
        {
            switch (lottery.LotteryType)
            {
                case LotteryType.Close:
                    return new CloseLotteryUserValidataor();
                case LotteryType.Open:
                    return new OpenLotteryUserValidator();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
