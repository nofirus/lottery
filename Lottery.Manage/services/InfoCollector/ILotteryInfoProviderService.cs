﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Lottery.Models.Models.DataStorageModels;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryParticipants;
using Lottery.Models.Models.DataStorageModels.LotteryWiner;
using Lottery.Models.Models.DataStorageModels.Picture;
using Lottery.Models.Models.DataStorageModels.Prize;
using Lottery.Models.Models.DataStorageModels.User;
using Lottery.Models.Models.LotteryViewModels;

namespace Lottery.Manage.services.InfoCollector
{
    public interface ILotteryInfoProviderService
    {
        #region lottery
        /// <summary>
        /// retuns winners of target lottery
        /// </summary>
        /// <param name="lottery">target lottery</param>
        /// <returns></returns>
        Task<List<LotteryWinners>> GetLotteryWinners(LotteryEntity lottery);

        /// <summary>
        /// retuns participants of target lottery
        /// </summary>
        /// <param name="lottery">target lottery</param>
        /// <returns></returns>
        Task<List<LotteryParticipants>> GetLotteryParticipants(LotteryEntity lottery);

        /// <summary>
        /// returs true if lottery type is equl quiz,otherwise return false
        /// </summary>
        /// <param name="lotteryId">target lottery id</param>
        /// <returns></returns>
        Task<bool> IsLotteryHasQuiz(int lotteryId);

        /// <summary>
        /// return true if user is winner if target lottery
        /// </summary>
        /// <param name="userClaims">target user</param>
        /// <param name="lottery">target lottery</param>
        /// <returns></returns>
        Task<bool> IsUserWinner(ClaimsPrincipal userClaims, LotteryEntity lottery);

        /// <summary>
        /// return place if tarhet user is winner,otherwise return 0
        /// </summary>
        /// <param name="userClaims"></param>
        /// <param name="lottery"></param>
        /// <returns></returns>
        Task<int> GetWinnerPlace(ClaimsPrincipal userClaims, LotteryEntity lottery);

        /// <summary>
        /// returns list of lottery whose LotteryDate  earlier than DateTime.Now
        /// </summary>
        /// <param name="filter">lottery filter</param>
        /// <returns></returns>
        Task<List<LotteryEntity>> GetAllHeldLotteries(LotteryFilters filter);

        /// <summary>
        /// return list of lottery participants for target lottery
        /// </summary>
        /// <param name="lotteryId">target lottery id</param>
        /// <returns></returns>
        Task<List<LotteryParticipants>> GetLotteryParticipants(int lotteryId);

        /// <summary>
        /// return list of lottery winners for target lottery
        /// </summary>
        /// <param name="lotteryId">target lottery id</param>
        /// <returns></returns>
        Task<List<LotteryWinners>> GetAllLotteryWinners(int lotteryId);

        /// <summary>
        /// return last lottery held 
        /// </summary>
        /// <returns></returns>
        Task<LotteryEntity> GetLastHeldLottery();

        /// <summary>
        /// return nearest future lottery for user
        /// </summary>
        /// <param name="userClaims">current user</param>
        /// <returns></returns>
        Task<LotteryEntity> GetClosestLotteryForUser(ClaimsPrincipal userClaims);

        /// <summary>
        /// return nearest future lottery
        /// </summary>
        /// <returns></returns>
        Task<LotteryEntity> GetClosestLottery();

        /// <summary>
        /// Check is user already registerd on lottery;
        /// if registred return true, otherwise return false
        /// </summary>
        /// <param name="lotteryId"></param>
        /// <param name="userClaims"></param>
        /// <returns></returns>
        Task<bool> IsAlreadyRegisteredOnTheLottery(int lotteryId, ClaimsPrincipal userClaims);

        /// <summary>
        /// return list of users which are not registred on the target lottery
        /// </summary>
        /// <param name="lotteryId">target lottery id</param>
        /// <returns></returns>
        Task<List<ApplicationUser>> GetAllUserUnregistredOnLottery(int lotteryId);

        /// <summary>
        /// returns excluded users from target lottery
        /// </summary>
        /// <param name="lotteryId">target lottery id</param>
        /// <returns></returns>
        Task<List<ExcludedFromTheLotteryUsers>> GetExcludedUsersForLottery(int lotteryId);
        /// <summary>
        /// return all lotteries in the database
        /// </summary>
        /// <returns></returns>
        Task<List<LotteryEntity>> GetAllLotteries();

        /// <summary>
        /// return all lotteries which correspond to the filter parameters
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<List<LotteryEntity>> GetAllLotteries(LotteryFilters filter);

        /// <summary>
        /// return all Participants for lottery with id = lotteryId
        /// </summary>
        /// <param name="lotteryId">target lottery id</param>
        /// <returns></returns>
        Task<List<ApplicationUser>> GetAllParticipantsForLotteryAsync(int lotteryId);

        Task<bool> IsUserExcludeFromLottery(ClaimsPrincipal userClaims, int lotteryId);

        /// <summary>
        /// return target lottery picture
        /// </summary>
        /// <param name="lotteryId">target lottery id</param>
        /// <returns></returns>
        Task<Picture> GetLotteryPicture(int lotteryId);

        /// <summary>
        /// return last  last held lottery
        /// </summary>
        /// <returns></returns>
        Task<LotteryEntity> GetLastLottery();

        /// <summary>
        ///  return all lotteries which correspond to the filter parameters and  whose LotteryDate > DateTime.Now
        /// </summary>
        /// <returns></returns>
        /// 
        Task<List<LotteryEntity>> GetFutureLotteries();

        /// <summary>
        /// returns lottery with id = lotteryIds
        /// </summary>
        /// <param name="lotteryId"></param>
        /// <returns></returns>
        Task<LotteryEntity> GetLotteryByIdAsync(int lotteryId);

        Task<LotteryEntity> GetLotteryByIdNoTracking(int lotteryId);

        #endregion
        /// <summary>
        /// returns all info about lottery
        /// </summary>
        /// <param name="lottery"></param>
        /// <returns></returns>
        /// 
        Task<string> CollectInfoAboutLottery(LotteryEntity lottery);

        /// <summary>
        /// collect info about helded lottery in the last month
        /// </summary>
        /// <returns></returns>
        Task<string> CollescInfoAboutLotteryForLastMonth();

        
        /// <summary>
        /// returns all pictures
        /// </summary>
        /// <returns></returns>
        Task<List<Picture>> GetAllPicturesAsync();

        /// <summary>
        /// returns pass of terget picture
        /// </summary>
        /// <param name="pictureId">target picture id</param>
        /// <returns></returns>
        Task<string> GetPicturePass(int pictureId);

      
    }
}
