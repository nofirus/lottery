﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Lottery.DataStorage.Repositories.ExcludedFromTheLotteryUsersRepository;
using Lottery.DataStorage.Repositories.LotteryParticipantsRepository;
using Lottery.DataStorage.Repositories.LotteryRepository;
using Lottery.DataStorage.Repositories.LotteryWinerRepository;
using Lottery.DataStorage.Repositories.PictureRepository;
using Lottery.DataStorage.Repositories.PrizeRepository;
using Lottery.DataStorage.Repositories.QuizRepository;
using Lottery.Models.Models.DataStorageModels;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryParticipants;
using Lottery.Models.Models.DataStorageModels.LotteryQuiz;
using Lottery.Models.Models.DataStorageModels.LotteryWiner;
using Lottery.Models.Models.DataStorageModels.Picture;
using Lottery.Models.Models.DataStorageModels.Prize;
using Lottery.Models.Models.DataStorageModels.Types;
using Lottery.Models.Models.DataStorageModels.User;
using Lottery.Models.Models.LotteryViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.EntityFrameworkCore;

namespace Lottery.Manage.services.InfoCollector
{
    public class LotteryInfoProviderService:ILotteryInfoProviderService
    {
        private readonly ILotteryRepository _lotteryRepository;
        private readonly ILotteryParticipantsRepository _lotteryParticipantsRepository;
        private readonly ILotteryWinerRepository _lotteryWinerRepository;
        private readonly IQuizRepository _quizRepository;
        private readonly IPrizeRepository _prizeRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IExcludedFromTheLotteryUsersRepository _excludedFromTheLotteryUsersRepository;
        private readonly IPictureRepository _pictureRepository;

        public LotteryInfoProviderService(IPrizeRepository prizeRepository, IQuizRepository quizRepository, ILotteryWinerRepository lotteryWinerRepository, ILotteryParticipantsRepository lotteryParticipantsRepository, ILotteryRepository lotteryRepository, UserManager<ApplicationUser> userManager, IExcludedFromTheLotteryUsersRepository excludedFromTheLotteryUsersRepository, IPictureRepository pictureRepository)
        {
            _prizeRepository = prizeRepository;
            _quizRepository = quizRepository;
            _lotteryWinerRepository = lotteryWinerRepository;
            _lotteryParticipantsRepository = lotteryParticipantsRepository;
            _lotteryRepository = lotteryRepository;
            _userManager = userManager;
            _excludedFromTheLotteryUsersRepository = excludedFromTheLotteryUsersRepository;
            _pictureRepository = pictureRepository;
        }

        public async Task<string> CollectInfoAboutLottery(LotteryEntity lottery)
        {
            var participants = await _lotteryParticipantsRepository.GetAllParticipantsForLottery(lottery);
            var winners = await _lotteryWinerRepository.GetLotteryWinners(lottery);
            var lotteryPrizies = await _prizeRepository.GetAllPriziesForLotteryAsync(lottery);
            var completedQuizAnswers = new List<IGrouping<ApplicationUser,CompletedQuizzes>>();
            if (lottery.LotteryKind.Equals(LotteryKind.Quiz))
            {
                var quiz = await _quizRepository.GetQuizForLottery(lottery);
                var answerdQuiz = await _quizRepository.GetComplitedQuizAnswer(quiz);
                completedQuizAnswers = answerdQuiz.GroupBy(x => x.ApplicationUser).ToList();
            }
            var resultString = new StringBuilder();
            resultString.Append("<h1> Lottery: ").Append(lottery.Description).Append("</h1>");
            resultString.Append("<h1>Participants</h1>").Append(Environment.NewLine);
            resultString.Append("<ul>");
            foreach (var p in participants)
            {
                resultString.Append("<li>").Append(p.Participants.UserName.ToString()).Append("</li>");
            }

            resultString.Append("</ul>");
            resultString.Append("<h1>Winners</h1>").Append(Environment.NewLine);
            resultString.Append("<ul>");
            foreach (var w in winners)
            {
                resultString.Append("<li>").Append(w.ApplicationUser.UserName.ToString()).Append("</li>");
            }
            resultString.Append("</ul>");
            resultString.Append("<h1>QuizAnswers</h1>").Append(Environment.NewLine);
            resultString.Append("<ul>");
            if (!completedQuizAnswers.Any()) return resultString.Append("</ul>").ToString();
            foreach (var cqa in completedQuizAnswers)
            {

                resultString.Append("<li>").Append(cqa.Key.UserName).Append("<ul>");
                foreach (var q in cqa.Select(x=>x))
                {
                    resultString.Append("<li>").Append(q.Question.Text+":"+q.AnswerText).Append("</li>");
                }

                resultString.Append("</ul>").Append("</li>");
            }
            resultString.Append("</ul>");
            return resultString.ToString();
        }

        public async Task<string> CollescInfoAboutLotteryForLastMonth()
        {
            var lotterries = await _lotteryRepository.GetAllLotteriesAsync();
            var lastMonth =
                DateTime.Now.Subtract(TimeSpan.FromDays(DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) + 1));
            var lastMonthLottery = lotterries.Where(x => x.LotteryDate < DateTime.Now && x.LotteryDate > lastMonth);
            var resultString = new StringBuilder();
            foreach (var lottery in lastMonthLottery)
            {
                resultString.Append( await CollectInfoAboutLottery(lottery));
            }

            return resultString.ToString();
        }

        public Task<List<LotteryWinners>> GetLotteryWinners(LotteryEntity lottery)
        {
           return _lotteryWinerRepository.GetLotteryWinners(lottery);
        }

        public Task<List<LotteryParticipants>> GetLotteryParticipants(LotteryEntity lottery)
        {
            return _lotteryParticipantsRepository.GetAllParticipantsForLottery(lottery);
        }

        public Task<bool> IsLotteryHasQuiz(int lotteryId)
        {
            var lottery = _lotteryRepository.GetLotteryByIdAsync(lotteryId).Result;
            var tsc = new TaskCompletionSource<bool>();
            tsc.SetResult(lottery.LotteryKind.Equals(LotteryKind.Quiz));
            return tsc.Task;
        }

        public async Task<bool> IsUserWinner(ClaimsPrincipal userClaims, LotteryEntity lottery)
        {
            var user = await _userManager.GetUserAsync(userClaims);
            var possibleWinner = new LotteryWinners() { LotteryId = lottery.Id, UserId = user.Id };
            return _lotteryWinerRepository.GetLotteryWinners(lottery).Result.Any(x => x.LotteryId.Equals(lottery.Id) && x.ApplicationUser.Equals(user));
        }

        public async Task<int> GetWinnerPlace(ClaimsPrincipal userClaims, LotteryEntity lottery)
        {
            var user = await _userManager.GetUserAsync(userClaims);
            var possibleWinner = new LotteryWinners() { LotteryId = lottery.Id, UserId = user.Id };
            return _lotteryWinerRepository.GetLotteryWinners(lottery).Result.Where(x => x.Equals(possibleWinner))
                .Select(x => x.Place).SingleOrDefault();

        }

      
        public async Task<List<LotteryEntity>> GetAllHeldLotteries(LotteryFilters filter)
        {
            var allLotteres = await GetAllLotteries(filter);
            return allLotteres.Where(x => x.LotteryDate < (DateTime.Now).Add(TimeSpan.FromMinutes(1))).ToList();
        }

        public async Task<List<LotteryParticipants>> GetLotteryParticipants(int lotteryId)
        {
            var lottery = await _lotteryRepository.GetLotteryByIdAsync(lotteryId);
            return await _lotteryParticipantsRepository.GetAllParticipantsForLottery(lottery);
        }

        public async Task<List<LotteryWinners>> GetAllLotteryWinners(int lotteryId)
        {
            var lottery = await _lotteryRepository.GetLotteryByIdAsync(lotteryId);
            return await _lotteryWinerRepository.GetLotteryWinners(lottery);
        }

        public async Task<LotteryEntity> GetLastHeldLottery()
        {
            var allLotteries = await _lotteryRepository.GetAllLotteriesAsync();
            allLotteries = allLotteries.Where(x => x.LotteryDate < DateTime.Now).ToList();
            return allLotteries.OrderByDescending(x => x.LotteryDate).FirstOrDefault();
        }

        public async Task<LotteryEntity> GetClosestLotteryForUser(ClaimsPrincipal userClaims)
        {
            var user = _userManager.GetUserAsync(userClaims).Result;
            if (user == null) return await GetClosestLottery();
            var userLotteries = await _lotteryParticipantsRepository.GetAllLotteriesForUser(user);
            if (userLotteries.Any())
            {
                var futureUserLottery = userLotteries.Where(x => x.RegistrationEndDate > DateTime.Now).OrderBy(x => x.LotteryDate).ToList();
                if (futureUserLottery.Any())
                {
                    return futureUserLottery.ElementAt(0);
                }
            }
            return await GetClosestLottery();
        }

        public async Task<LotteryEntity> GetClosestLottery()
        {
            var lotteryEntity = await _lotteryRepository.GetAllLotteriesAsync();
            if (lotteryEntity.Any())
            {
                var lotteryList = from lottery in lotteryEntity
                    where lottery.LotteryDate > DateTime.Now
                    orderby lottery.LotteryDate
                    select lottery;
                if (lotteryList.Any())
                {
                    return lotteryList.First();
                }
            }
            return new LotteryEntity();
        }

        public async Task<bool> IsAlreadyRegisteredOnTheLottery(int lotteryId, ClaimsPrincipal userClaims)
        {
            var user = await _userManager.GetUserAsync(userClaims);
            var lotterys = await _lotteryParticipantsRepository.GetAllLotteriesForUser(user);
            var lottery = await _lotteryRepository.GetLotteryByIdAsync(lotteryId);
            return lotterys.Contains(lottery);
        }

        public async Task<List<ApplicationUser>> GetAllUserUnregistredOnLottery(int lotteryId)
        {
            var lottery = await _lotteryRepository.GetLotteryByIdAsync(lotteryId);
            var partisipantsList = await _lotteryParticipantsRepository.GetAllParticipantsForLottery(lottery);
            var allUser = _userManager.Users;
            return await allUser.Except(partisipantsList.Select(x => x.Participants).ToList()).ToListAsync();
        }

        public async Task<List<ExcludedFromTheLotteryUsers>> GetExcludedUsersForLottery(int lotteryId)
        {
            var lottery = await _lotteryRepository.GetLotteryByIdAsync(lotteryId);
            return await _excludedFromTheLotteryUsersRepository.GetExcludedUserForLottery(lottery);
        }

        public Task<List<LotteryEntity>> GetAllLotteries()
        {
            return _lotteryRepository.GetAllLotteriesAsync();
        }

        public async Task<List<LotteryEntity>> GetAllLotteries(LotteryFilters filter)
        {
            var allLotteres = await _lotteryRepository.GetAllLotteriesAsync();
            if (filter.Page != 0)
            {
                allLotteres = allLotteres
                    .Where(x => x.LotteryDate >= filter.LeftDate && x.LotteryDate <= filter.RightDate).ToList();
                if (filter.AmountOfWinnersMax != 0 && filter.AmountOfWinnersMin != 0)
                {
                    allLotteres = allLotteres.Where(x =>
                            x.AmountOfWinners >= filter.AmountOfWinnersMin &&
                            x.AmountOfWinners <= filter.AmountOfWinnersMax)
                        .ToList();
                }
            }
            return allLotteres.ToList();
        }

        public async Task<List<ApplicationUser>> GetAllParticipantsForLotteryAsync(int lotteryId)
        {
            var lottery = await _lotteryRepository.GetLotteryByIdAsync(lotteryId);
            var participants = await _lotteryParticipantsRepository.GetAllParticipantsForLottery(lottery);
            return participants.Select(x => x.Participants).ToList();
        }

        public async Task<bool> IsUserExcludeFromLottery(ClaimsPrincipal userClaims, int lotteryId)
        {
            var lottery = await _lotteryRepository.GetLotteryByIdAsync(lotteryId);
            var user = await _userManager.GetUserAsync(userClaims);
            var excludedUsers = await _excludedFromTheLotteryUsersRepository.GetExcludedUserForLottery(lottery);
            return excludedUsers.Select(x => x.User).Contains(user);
        }

        public async Task<Picture> GetLotteryPicture(int lotteryId)
        {
            return await _pictureRepository.GetLotteryPicture(lotteryId);
        }

       
        public async Task<List<LotteryEntity>> GetFutureLotteries()
        {
            var allLotteries = await GetAllLotteries();
            allLotteries = allLotteries.Where(l => l.RegistrationEndDate > DateTime.Now).ToList();
            var futureLotteries = new List<LotteryEntity>();
            foreach (var lottery in allLotteries)
            {
                if (await IsLotteryHasQuiz(lottery.Id))
                {
                    if ((await _quizRepository.GetQuizForLottery(lottery)) == null)
                    {
                        continue;
                    }
                }
                if ((await _prizeRepository.GetAllPriziesForLotteryAsync(lottery)).Any())
                {
                    futureLotteries.Add(lottery);
                }
            }
            return futureLotteries.Where(l => l.RegistrationEndDate > DateTime.Now).OrderBy(x=>x.LotteryDate).ToList();
        }

        public async Task<LotteryEntity> GetLotteryByIdAsync(int lotteryId)
        {
            return await _lotteryRepository.GetLotteryByIdAsync(lotteryId);
        }

        public Task<LotteryEntity> GetLotteryByIdNoTracking(int lotteryId)
        {
            return  _lotteryRepository.GetLotteryByIdAsyncNoTracking(lotteryId);
        }

        public Task<List<Picture>> GetAllPicturesAsync()
        {
            return _pictureRepository.GetAllPictures();
        }

        public Task<string> GetPicturePass(int pictureId)
        {
            return _pictureRepository.GetPicturePass(pictureId);
        }

        public async Task<LotteryEntity> GetLastLottery()
        {
            return await _lotteryRepository.GetLastHeldLottery();
        }

    }
}
