﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Lottery.Models.Models.DataStorageModels.Roles;
using Lottery.Models.Models.DataStorageModels.User;

namespace Lottery.DataStorage.Services
{
    public interface IUserStorageService
    {
         Task<List<ApplicationUser>> GetAllUsers();
         Task<List<UserRole>> GetUserRolesAsync(ApplicationUser applicationUser);
         Task<ApplicationUser> GetAllUserPaginated(int page, int pageSize);
         Task AddRoleToUserAsync(ApplicationUser user, string roleName);
         Task<ApplicationUser> GetUserByEmailAsync(string email);
         Task BlockUserAsync(string email, string reason);
    }
}
