﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Lottery.DataStorage.Data;
using Lottery.DataStorage.Repositories.LotteryRepository;
using Lottery.DataStorage.Repositories.PictureRepository;
using Lottery.DataStorage.Repositories.UserRepository;
using Lottery.Models.Models.DataStorageModels.LotteryWiner;
using Lottery.Models.Models.DataStorageModels.Prize;

namespace Lottery.DataStorage.Services
{
    public class DataStorageService:IDataStorageService
    {
        public ILotteryRepository LotteryRepository { get;}
        public IUserRepository UserRepository { get; }
        public IPictureRepository PictureRepository { get; }

        public DataStorageService(ILotteryRepository lotteryRepository, IUserRepository userRepository, IPictureRepository pictureRepository)
        {
            LotteryRepository = lotteryRepository;
            UserRepository = userRepository;
            PictureRepository = pictureRepository;
        }

    }
}
