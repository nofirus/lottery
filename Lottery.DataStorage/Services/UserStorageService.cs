﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lottery.DataStorage.Repositories.UserRepository;
using Lottery.Models.Models.DataStorageModels.Roles;
using Lottery.Models.Models.DataStorageModels.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Lottery.DataStorage.Services
{
    public class UserStorageService:IUserStorageService
    {
        public IUserRepository UserRepository { get; }
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<UserRole> _roleManager;

        public UserStorageService(IUserRepository userRepository, RoleManager<UserRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            UserRepository = userRepository;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        public void UpdateUserInfo(ApplicationUser user)
        {
            
        }

        public async Task<List<ApplicationUser>> GetAllUsers()
        {
            return await UserRepository.GetAllUsersAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationUser"></param>
        /// <returns></returns>
        public async Task<List<UserRole>> GetUserRolesAsync(ApplicationUser applicationUser)
        {
            var roleStringList = await _userManager.GetRolesAsync(applicationUser);
            return roleStringList.Select(role => new UserRole(role)).ToList();
        }

        public async Task<ApplicationUser> GetAllUserPaginated(int page, int pageSize)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public async Task AddRoleToUserAsync(ApplicationUser user,string roleName)
        {
           await _userManager.AddToRoleAsync(user, roleName);
        }

        public async Task<ApplicationUser> GetUserByEmailAsync(string email)
        {
            return await UserRepository.GetUserByEmailAsync(email);
        }

        /// <summary>
        /// block user on 5 minutes
        /// </summary>
        /// <param name="email"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        public async Task BlockUserAsync(string email, string reason)
        {
            var user = await UserRepository.GetUserByEmailAsync(email);
            user.BlockReason = reason;
            await _userManager.SetLockoutEndDateAsync(user, new DateTimeOffset(DateTime.Now.Add(TimeSpan.FromMinutes(5))));
            UpdateUserInfo(user);

            //await UserRepository.BlockUserAsync(email, reason);
        }
    }
}
