﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Lottery.DataStorage.Repositories.LotteryRepository;
using Lottery.DataStorage.Repositories.PictureRepository;
using Lottery.DataStorage.Repositories.UserRepository;
using Lottery.Models.Models.DataStorageModels.LotteryWiner;

namespace Lottery.DataStorage.Services
{
    public interface IDataStorageService
    {
        ILotteryRepository LotteryRepository { get; }
        IUserRepository UserRepository { get; }
        IPictureRepository PictureRepository { get; }

      

       
    }
}
