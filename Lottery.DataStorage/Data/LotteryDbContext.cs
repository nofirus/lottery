﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Lottery.Models.Models.DataStorageModels;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryParticipants;
using Lottery.Models.Models.DataStorageModels.LotteryQuiz;
using Lottery.Models.Models.DataStorageModels.LotteryWiner;
using Lottery.Models.Models.DataStorageModels.Picture;
using Lottery.Models.Models.DataStorageModels.Prize;
using Lottery.Models.Models.DataStorageModels.Roles;
using Lottery.Models.Models.DataStorageModels.User;

namespace Lottery.DataStorage.Data
{
    public class LotteryDbContext : IdentityDbContext<ApplicationUser>
    {
        private readonly DbContextOptions<LotteryDbContext> _options;
        

        public LotteryDbContext(DbContextOptions<LotteryDbContext> options)
            : base(options)
        {
            _options = options;
        }

        public DbSet<LotteryEntity> Lotteries { get; set; }
        public DbSet<LotteryWinners> LotteryWinners { get; set; }
        public DbSet<LotteryPrizeList> LotteryPrizeList { get; set; }
        public DbSet<LotteryPrize> LotteryPrize { get; set; }
        public DbSet<Picture> Picture { get; set; }
        public DbSet<LotteryParticipants> LotteryParticipantses { get; set; }
        public DbSet<ExcludedFromTheLotteryUsers> ExcludedFromTheLotteryUserses { get; set; }
        public DbSet<LotteryQuizzes> LotteryQuizzes { get; set; }

        public DbSet<Answer> Answers { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Quiz> Quizzes { get; set; }
        public DbSet<CompletedQuizzes> CompletedQuizzeses { get; set; }
        public DbSet<QuestionAnswers> QuestionAnswers { get; set; }
        public DbSet<QuizQuestions> QuizQuestions { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<LotteryWinners>().HasKey(l => l.Id);
            builder.Entity<LotteryWinners>()
                .HasOne(lpl => lpl.Lottery)
                .WithMany(l => l.LotteryWiners)
                .HasForeignKey(lpl => lpl.LotteryId);

            builder.Entity<LotteryWinners>()
                .HasOne(lpl => lpl.ApplicationUser)
                .WithMany(l => l.LotteryWiners)
                .HasForeignKey(lpl => lpl.UserId);

            builder.Entity<LotteryUsers>()
              .HasOne(lu => lu.Lottery)
              .WithMany(l => l.LotteryUsers)
              .HasForeignKey(lu => lu.LotteryId);

            builder.Entity<LotteryUsers>()
                .HasOne(lu => lu.ApplicationUser)
                .WithMany(l => l.LotteryUsers)
                .HasForeignKey(lu => lu.UserId);

            builder.Entity<LotteryPrizeList>().HasKey(t => t.Id);
            builder.Entity<LotteryPrizeList>()
                .HasOne(lpl => lpl.Lottery)
                .WithMany(l => l.LotteryPrizies)
                .HasForeignKey(lpl => lpl.LotteryId);

            builder.Entity<LotteryPrizeList>()
                .HasOne(lpl => lpl.Prize)
                .WithMany(l => l.LotteryPrizies)
                .HasForeignKey(lpl => lpl.PrizeId);

           

            builder.Entity<QuizQuestions>().HasKey(t => t.Id);
            builder.Entity<QuizQuestions>()
                .HasOne(q => q.Question)
                .WithMany(x => x.QuizQuestions)
                .HasForeignKey(q => q.QuestionId);

            builder.Entity<QuizQuestions>()
                .HasOne(q => q.Quiz)
                .WithMany(x => x.QuizQuestions)
                .HasForeignKey(q => q.QuizId);

            builder.Entity<QuestionAnswers>().HasKey(t => t.Id);
            builder.Entity<QuestionAnswers>()
                .HasOne(q => q.Answer)
                .WithMany(x => x.QuestionAnswers)
                .HasForeignKey(q => q.AnswerId);

            builder.Entity<QuestionAnswers>()
                .HasOne(q => q.Question)
                .WithMany(x => x.QuestionAnswers)
                .HasForeignKey(q => q.QuestionId);


            builder.Entity<LotteryQuizzes>().HasKey(lq => lq.Id);
            builder.Entity<LotteryQuizzes>()
                .HasOne(lq => lq.Lottery)
                .WithMany(x => x.LotteryQuizzes)
                .HasForeignKey(lq => lq.LotteryId);

            builder.Entity<LotteryQuizzes>()
                .HasOne(lq => lq.Quiz)
                .WithMany(x => x.LotteryQuizzes)
                .HasForeignKey(lq => lq.QuizId);
        }
    }
}
