﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryWiner;

namespace Lottery.DataStorage.Repositories.LotteryWinerRepository
{
    public interface ILotteryWinerRepository
    {
        #region LotteryWinners

        Task<List<LotteryWinners>> GetLotteryWinners(LotteryEntity lottery);

        Task AddLotteryWinners(List<LotteryWinners> winnersList);

        #endregion
    }
}
