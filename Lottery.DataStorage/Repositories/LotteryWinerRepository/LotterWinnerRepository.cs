﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lottery.DataStorage.Data;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryWiner;
using Microsoft.EntityFrameworkCore;

namespace Lottery.DataStorage.Repositories.LotteryWinerRepository
{
    public class LotterWinnerRepository:ILotteryWinerRepository
    {

        private readonly LotteryDbContext _lotteryDbContext;

        public LotterWinnerRepository(LotteryDbContext lotteryDbContext)
        {
            _lotteryDbContext = lotteryDbContext;
        }

        public  Task<List<LotteryWinners>> GetLotteryWinners(LotteryEntity lottery)
        {
            return   _lotteryDbContext.LotteryWinners.Include(x=>x.ApplicationUser).Include(x=>x.Lottery).Where(w => w.Lottery.Equals(lottery)).ToListAsync();
        }

        public async Task AddLotteryWinners( List<LotteryWinners> winnersList)
        {
            await _lotteryDbContext.LotteryWinners.AddRangeAsync(winnersList);
            await _lotteryDbContext.SaveChangesAsync();
        }
    }
}
