﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Lottery.Models.Models.DataStorageModels.Roles;
using Lottery.Models.Models.DataStorageModels.User;

namespace Lottery.DataStorage.Repositories.UserRepository
{
    public interface IUserRepository
    {
        /// <summary>
        /// return user with id = user id
        /// </summary>
        /// <param name="userId">id of target user</param>
        /// <returns></returns>
        Task<ApplicationUser> GetUser(string userId);

        Task<ApplicationUser> GetUserByEmailAsync(string email);

        Task UpdateUserAsync(ApplicationUser user);

        Task<List< ApplicationUser>> GetAllUsersAsync();

        Task AddOldPasswordHash(string oldPasswordHash, string userId);

        Task AddOldPassword2Hash(string oldPasswordHash, string userId);

        Task SetEnableEmailAuthenticator(ApplicationUser user, bool value);
    }
}
