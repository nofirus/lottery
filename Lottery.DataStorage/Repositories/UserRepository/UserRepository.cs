﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lottery.DataStorage.Data;
using Lottery.Models.Models.DataStorageModels.User;
using Microsoft.EntityFrameworkCore;

namespace Lottery.DataStorage.Repositories.UserRepository
{
    ///<inheritdoc/>
    public class UserRepository:IUserRepository
    {
        private readonly LotteryDbContext _userDbContext;

        public UserRepository(LotteryDbContext userDbContext)
        {
            this._userDbContext = userDbContext;
        }
        /// <inheritdoc />
        public async Task<ApplicationUser> GetUser(string userId)
        {
            return await _userDbContext.Users.SingleAsync(u => u.Id.Equals(userId));
        }

        public async Task<ApplicationUser> GetUserByEmailAsync(string email)
        {
            return await _userDbContext.Users.SingleAsync(u => u.Email.Equals(email));
        }

        public async Task UpdateUserAsync(ApplicationUser user)
        {
            _userDbContext.Users.Update(user);
           await _userDbContext.SaveChangesAsync();
        }

        public async Task<List<ApplicationUser>> GetAllUsersAsync()
        {
            return await _userDbContext.Users.ToListAsync();
        }

        public async Task AddOldPasswordHash(string oldPasswordHash, string userId)
        {
            var user = _userDbContext.Users.Find(userId);
            user.OldPassword = oldPasswordHash;
            _userDbContext.Users.Update(user);
           await _userDbContext.SaveChangesAsync();
        }

        public async Task AddOldPassword2Hash(string oldPasswordHash, string userId)
        {
            var user = _userDbContext.Users.Find(userId);
            user.OldPassword2 = oldPasswordHash;
            _userDbContext.Users.Update(user);
            await _userDbContext.SaveChangesAsync();
        }

        public Task SetEnableEmailAuthenticator(ApplicationUser user, bool value)
        {
            user.TwoFactorEmailEnabled = value;
            _userDbContext.Users.Update(user);
            return _userDbContext.SaveChangesAsync();
        }
    }
}
