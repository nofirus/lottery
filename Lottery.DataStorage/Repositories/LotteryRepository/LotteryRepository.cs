﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lottery.DataStorage.Data;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryWiner;
using Lottery.Models.Models.DataStorageModels.Picture;
using Lottery.Models.Models.DataStorageModels.Prize;
using Microsoft.EntityFrameworkCore;


namespace Lottery.DataStorage.Repositories.LotteryRepository
{
    public class LotteryRepository:ILotteryRepository
    {
        private readonly LotteryDbContext _lotteryDbContext;
       

        public LotteryRepository(LotteryDbContext lotteryDbContext)
        {
            _lotteryDbContext = lotteryDbContext;
        }

        #region Lottery

        public Task<LotteryEntity> GetLotteryByIdAsync(int lotteryId)
        {
            return _lotteryDbContext.Lotteries.SingleOrDefaultAsync(l => l.Id.Equals(lotteryId));
        }

        public async Task<LotteryEntity> GetLotteryByIdAsyncNoTracking(int lotteryId)
        {
            var lottery = await _lotteryDbContext.Lotteries.AsNoTracking().SingleAsync(l => l.Id.Equals(lotteryId));
            return lottery;
        }

        public async Task<LotteryEntity> GetLastHeldLottery()
        {
            var heldedLotteries =  await _lotteryDbContext.Lotteries.Where(x => x.LotteryDate < DateTime.Now).ToListAsync();
            return heldedLotteries.OrderByDescending(x => x.LotteryDate).FirstOrDefault();
        }

        public Task SaveChanges()
        {
           return _lotteryDbContext.SaveChangesAsync();
        }

        public async Task<object> GetLotteryInfo(int lotteryId)
        {
            var lotter = await GetLotteryByIdAsyncNoTracking(lotteryId);
            return new {AmountOfWinners = lotter.AmountOfWinners,LotteryKind = lotter.LotteryKind,LotteryType = lotter.LotteryType};
        }

        public async Task AddLotteryAsync(LotteryEntity lottery)
        {
            await _lotteryDbContext.Lotteries.AddAsync(lottery);
            _lotteryDbContext.SaveChanges();
        }

        public async Task DeleteLotteryAsync(LotteryEntity lottery )
        {
            _lotteryDbContext.Lotteries.Remove(lottery);
           await _lotteryDbContext.SaveChangesAsync();
        }

        public Task<List<LotteryEntity>> GetAllLotteriesAsync()
        {
            return _lotteryDbContext.Lotteries.AsNoTracking().Include(x=>x.Picture).ToListAsync();
        }

        public async Task UpdateLotteryAsync(LotteryEntity lottery)
        {
            _lotteryDbContext.Lotteries.Update(lottery);
            await _lotteryDbContext.SaveChangesAsync();
        }
        #endregion
    }
}
