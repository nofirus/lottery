﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryWiner;
using Lottery.Models.Models.DataStorageModels.Picture;
using Lottery.Models.Models.DataStorageModels.Prize;

namespace Lottery.DataStorage.Repositories.LotteryRepository
{
    public interface ILotteryRepository
    {
        /// <summary>
        /// Add to database new lottery;
        /// </summary>
        /// <param name="lottery"></param>
        Task AddLotteryAsync(LotteryEntity lottery);

        /// <summary>
        /// Deleete from database lottery;
        /// </summary>
        /// <param name="lottery"></param>
        Task DeleteLotteryAsync(LotteryEntity lottery);

        /// <summary>
        /// update lottery 
        /// </summary>
        /// <param name="lottery">new lottery data</param>
        /// <remarks>required old lottery id</remarks>
        Task UpdateLotteryAsync(LotteryEntity lottery);
         
        /// <summary>
        /// return the list of all loteries
        /// </summary>
        /// <returns></returns>
        Task<List<LotteryEntity>> GetAllLotteriesAsync();

        /// <summary>
        /// return LotteryEntity with id = lotteryId
        /// </summary>
        /// <param name="lotteryId"></param>
        /// <returns></returns>
        Task<LotteryEntity> GetLotteryByIdAsync(int lotteryId);

        Task<LotteryEntity> GetLotteryByIdAsyncNoTracking(int lotteryId);

        /// <summary>
        /// return last held lottery
        /// </summary>
        /// <returns></returns>
        Task<LotteryEntity> GetLastHeldLottery();

        /// <summary>
        /// it's a test method
        /// </summary>
        /// <returns></returns>
        Task SaveChanges();

        Task<object> GetLotteryInfo(int lotteryId);
    }
}
