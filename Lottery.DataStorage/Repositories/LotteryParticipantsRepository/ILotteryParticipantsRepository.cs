﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryParticipants;
using Lottery.Models.Models.DataStorageModels.User;

namespace Lottery.DataStorage.Repositories.LotteryParticipantsRepository
{
    public interface ILotteryParticipantsRepository
    {
        

        /// <summary>
        /// add new lottery LotteryParticipants
        /// </summary>
        /// <param name="participant"></param>
        /// <returns></returns>
        Task AddLotteryParticipantsAsync(LotteryParticipants participant);

        /// <summary>
        /// return LotteryParticipants object from database if exist otherwise return null
        /// </summary>
        /// <param name="lotteryId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<LotteryParticipants> GetLotteryParticipant(int lotteryId, int userId);

        /// <summary>
        /// return all lotteries for user
        /// </summary>
        /// <param name="user">target user</param>
        /// <returns></returns>
        Task<List<LotteryEntity>> GetAllLotteriesForUser(ApplicationUser user);

        /// <summary>
        /// return all partisipants of the target lottery
        /// </summary>
        /// <param name="lottery"></param>
        /// <returns></returns>
        Task<List<LotteryParticipants>> GetAllParticipantsForLottery(LotteryEntity lottery);

        /// <summary>
        /// remove LotteryParticipants object from the database
        /// </summary>
        /// <param name="lotteryParticipants"></param>
        /// <returns></returns>
        Task RemoveParticipant(LotteryParticipants lotteryParticipants);

        /// <summary>
        /// deletes all participanys of target lottery
        /// </summary>
        /// <param name="lottery">target lottery</param>
        /// <returns></returns>
        Task DeleteAllParticipantsForLottery(LotteryEntity lottery);
    }
}
