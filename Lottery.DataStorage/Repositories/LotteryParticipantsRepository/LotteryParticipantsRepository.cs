﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lottery.DataStorage.Data;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryParticipants;
using Lottery.Models.Models.DataStorageModels.User;
using Microsoft.EntityFrameworkCore;

namespace Lottery.DataStorage.Repositories.LotteryParticipantsRepository
{
    public class LotteryParticipantsRepository:ILotteryParticipantsRepository
    {
        private readonly LotteryDbContext _lotteryDbContext;
        

        public LotteryParticipantsRepository(LotteryDbContext lotteryDbContext)
        {
            _lotteryDbContext = lotteryDbContext;
        }

        public  Task<List<LotteryParticipants>> GetLotteryParticipantsAsync(LotteryEntity lottery)
        {
           return _lotteryDbContext.LotteryParticipantses.Include(x=>x.Participants).Where(x => x.Lottery.Equals(lottery)).ToListAsync();
        }

        public async Task AddLotteryParticipantsAsync(LotteryParticipants participant)
        {
            await _lotteryDbContext.LotteryParticipantses.AddAsync(participant);
            await _lotteryDbContext.SaveChangesAsync();
        }

        public Task<LotteryParticipants> GetLotteryParticipant(int lotteryId, int userId)
        {
            return _lotteryDbContext.LotteryParticipantses.SingleAsync();
        }

        public Task<List<LotteryEntity>> GetAllLotteriesForUser(ApplicationUser user)
        {
            return _lotteryDbContext.LotteryParticipantses.Where(p => p.Participants.Equals(user))
                .Select(p => p.Lottery).Include(x=>x.Picture).ToListAsync();
        }

        public async Task<List<LotteryParticipants>> GetAllParticipantsForLottery(LotteryEntity lottery)
        {
            if (lottery != null)
            {
                return await _lotteryDbContext.LotteryParticipantses.Where(x => x.Lottery.Equals(lottery))
                    .Include(b => b.Participants).ToListAsync();
            }
            return new List<LotteryParticipants>();
        }

        public async Task RemoveParticipant(LotteryParticipants lotteryParticipants)
        {
            var participant = _lotteryDbContext.LotteryParticipantses.SingleOrDefault(x =>
                x.Lottery.Equals(lotteryParticipants.Lottery) &&
                x.Participants.Equals(lotteryParticipants.Participants));
            if (participant != null) _lotteryDbContext.LotteryParticipantses.Remove(participant);
            await _lotteryDbContext.SaveChangesAsync();
        }

        public async Task DeleteAllParticipantsForLottery(LotteryEntity lottery)
        {
            var allParticipants = await GetAllParticipantsForLottery(lottery);
            _lotteryDbContext.LotteryParticipantses.RemoveRange(allParticipants);
            _lotteryDbContext.SaveChanges();
        }
    }
}
