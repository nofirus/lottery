﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Lottery.Models.Models.DataStorageModels;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.User;

namespace Lottery.DataStorage.Repositories.ExcludedFromTheLotteryUsersRepository
{
    public interface IExcludedFromTheLotteryUsersRepository
    {
        Task Add(ExcludedFromTheLotteryUsers user);

        Task RemoveUserFromExcluded(ExcludedFromTheLotteryUsers excludedFromTheLotteryUsers);

        Task<List<ExcludedFromTheLotteryUsers>> GetExcludedUserForLottery(LotteryEntity lottery);
    }
}
