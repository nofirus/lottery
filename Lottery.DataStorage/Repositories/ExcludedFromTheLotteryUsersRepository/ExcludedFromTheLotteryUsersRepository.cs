﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lottery.DataStorage.Data;
using Lottery.Models.Models.DataStorageModels;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.User;
using Microsoft.EntityFrameworkCore;

namespace Lottery.DataStorage.Repositories.ExcludedFromTheLotteryUsersRepository
{
    public class ExcludedFromTheLotteryUsersRepository:IExcludedFromTheLotteryUsersRepository
    {
        private readonly LotteryDbContext _lotteryDbContext;

        public ExcludedFromTheLotteryUsersRepository(LotteryDbContext lotteryDbContext)
        {
            _lotteryDbContext = lotteryDbContext;
        }

        public async Task Add(ExcludedFromTheLotteryUsers exeExcludedFromTheLotteryUsers)
        {
            await _lotteryDbContext.ExcludedFromTheLotteryUserses.AddAsync(exeExcludedFromTheLotteryUsers);
            await _lotteryDbContext.SaveChangesAsync();
        }

        public async Task RemoveUserFromExcluded(ExcludedFromTheLotteryUsers exeExcludedFromTheLotteryUsers)
        {
            var model = _lotteryDbContext.ExcludedFromTheLotteryUserses.FirstOrDefault(x =>
                x.Lottery.Equals(exeExcludedFromTheLotteryUsers.Lottery) &&
                x.User.Equals(exeExcludedFromTheLotteryUsers.User));
            if (model != null) _lotteryDbContext.ExcludedFromTheLotteryUserses.Remove(model);//TODO add logger if null
            await _lotteryDbContext.SaveChangesAsync();
        }

        public async Task<List<ExcludedFromTheLotteryUsers>> GetExcludedUserForLottery(LotteryEntity lottery)
        {
            return await _lotteryDbContext.ExcludedFromTheLotteryUserses.Where(x => x.Lottery.Equals(lottery)).Include(x=>x.User).ToListAsync();
        }
    }
}
