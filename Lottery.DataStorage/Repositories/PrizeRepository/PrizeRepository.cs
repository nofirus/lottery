﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lottery.DataStorage.Data;
using Lottery.DataStorage.Repositories.LotteryRepository;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.Picture;
using Lottery.Models.Models.DataStorageModels.Prize;
using Microsoft.EntityFrameworkCore;

namespace Lottery.DataStorage.Repositories.PrizeRepository
{
    public class PrizeRepository : IPrizeRepository
    {
        private readonly LotteryDbContext _lotteryDbContext;
        private readonly ILotteryRepository _lotteryRepository;

        public PrizeRepository(LotteryDbContext lotteryDbContext, ILotteryRepository lotteryRepository)
        {
            _lotteryDbContext = lotteryDbContext;
            _lotteryRepository = lotteryRepository;
        }

        public async Task<LotteryPrize> AddPrizeAsync(LotteryPrize prize)
        {
            _lotteryDbContext.LotteryPrize.Add(prize);
            await _lotteryDbContext.SaveChangesAsync();
            return prize;
        }

        public async Task DeletePrizeAsync(int prizeId)
        {
            var prize = _lotteryDbContext.LotteryPrize.Find(prizeId);
            _lotteryDbContext.LotteryPrize.Remove(prize);
            await _lotteryDbContext.SaveChangesAsync();

        }

        public async Task<List<LotteryPrizeList>> GetAllPriziesForLotteryAsync(LotteryEntity lottery)
        {
            return await _lotteryDbContext.LotteryPrizeList.Include(x=>x.Prize).Where(x => x.LotteryId.Equals(lottery.Id)).ToListAsync();
        }

        public async Task<Picture> GetLotteryPicture(int lotteryId)
        {
            var lottery = await _lotteryDbContext.Lotteries.FindAsync(lotteryId);
            return lottery.Picture;
        }


        public Task<List<LotteryPrize>> GetAllPriziesAsync()
        {
            return _lotteryDbContext.LotteryPrize.ToListAsync();
        }

        public Task<LotteryPrize> GetPrizeByIdAsync(int prizeId)
        {
            return _lotteryDbContext.LotteryPrize.SingleOrDefaultAsync(p => p.Id.Equals(prizeId));
        }

        public async Task UpdatePrizeAsync(LotteryPrize prize)
        {
            _lotteryDbContext.LotteryPrize.Update(prize);
            await _lotteryDbContext.SaveChangesAsync();
        }


        public async Task AddPriziesForLotteryAsync(LotteryEntity lottery, List<LotteryPrizeList> lotteryPrize)
        {
            await DeleteAllPriziesForLottery(lottery);
            await _lotteryDbContext.LotteryPrizeList.AddRangeAsync(lotteryPrize);
            //lottery.LotteryPrizies = lotteryPrize;
            //await _lotteryRepository.UpdateLotteryAsync(lottery);
            _lotteryDbContext.SaveChanges();
            //_userDbContext.SaveChanges();

        }
        public async Task AddPrizeForLotteryAsync(LotteryPrizeList lotteryPrize)
        {
            _lotteryDbContext.LotteryPrizeList.Add(lotteryPrize);
            await _lotteryDbContext.SaveChangesAsync();
        }

        public Task<List<int>> GetIdOfAllPrizeForLotteryAsync(int lotteryId)
        {
            return _lotteryDbContext.LotteryPrizeList.Where(l => l.LotteryId.Equals(lotteryId)).Select(l => l.PrizeId).ToListAsync();
        }

        public async Task DeleteAllPriziesForLottery(LotteryEntity lottery)
        {
            var prizeList = await GetAllPriziesForLotteryAsync(lottery);
            _lotteryDbContext.LotteryPrizeList.RemoveRange(prizeList);
            await _lotteryDbContext.SaveChangesAsync();
        }

        /// <summary>
        /// do not use this for now
        /// </summary>
        /// <param name="lott"></param>
        public void UpdatePrizeForLottery(LotteryPrizeList lott)
        {
            throw new NotImplementedException();
        }

        public LotteryPrizeList GetPrizeForLottery(int lotteryId, int prizeId)
        {
            return _lotteryDbContext.LotteryPrizeList.Find(new object[] { lotteryId, prizeId });
        }

        public void DeletePrizeForLottery(int lotteryId, int prizeId)
        {
            var prizeForLottery = GetPrizeForLottery(lotteryId, prizeId);
            _lotteryDbContext.LotteryPrizeList.Remove(prizeForLottery);
        }

        public async Task<List<LotteryEntity>> GetLotteriesForPrize(int prizeId)
        {
          return  await _lotteryDbContext.LotteryPrizeList.Where(x => x.PrizeId.Equals(prizeId)).Select(x => x.Lottery)
                .ToListAsync();
        }
    }
}
