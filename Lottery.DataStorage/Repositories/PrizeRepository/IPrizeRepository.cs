﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.Prize;

namespace Lottery.DataStorage.Repositories.PrizeRepository
{
    public interface IPrizeRepository
    {

        #region LotteryPrize

        /// <summary>
        /// return the list of all prizies
        /// </summary>
        /// <returns></returns>
        Task<List<LotteryPrize>> GetAllPriziesAsync();

        /// <summary>
        /// return LotteryPrize with id=prizeId
        /// </summary>
        /// <param name="prizeId"></param>
        /// <returns></returns>
        Task<LotteryPrize> GetPrizeByIdAsync(int prizeId);

        /// <summary>
        /// add new lottery prize to database
        /// </summary>
        /// <param name="prize"></param>
        /// <returns></returns>
        Task<LotteryPrize> AddPrizeAsync(LotteryPrize prize);

        /// <summary>
        /// delite LotteryPrize with id = prizeId;
        /// </summary>
        /// <param name="prizeId"></param>
        Task DeletePrizeAsync(int prizeId);

        /// <summary>
        /// update lottery prize
        /// </summary>
        /// <param name="prize"></param>
        Task UpdatePrizeAsync(LotteryPrize prize);

        /// <summary>
        /// retuns list of all  prizies for current lottery
        /// </summary>
        /// <param name="lotte"></param>
        /// <returns></returns>
        Task<List<LotteryPrizeList>> GetAllPriziesForLotteryAsync(LotteryEntity lotte);

        #endregion

        #region PrizesForeLotery

        /// <summary>
        /// add new prizies to lorttery with id = lotteryId;
        /// </summary>
        /// <param name="lotteryId"></param>
        /// <param name="lotteryPrize"></param>
        /// <returns></returns>
        Task AddPriziesForLotteryAsync(LotteryEntity lottery, List<LotteryPrizeList> lotteryPrize);

        /// <summary>
        /// return all prizies for lottery with id = lotteryId
        /// </summary>
        /// <param name="lotteryId">id of lottery from wich get prizies</param>
        /// <returns>List<LotteryPrize/></returns>
        Task<List<int>> GetIdOfAllPrizeForLotteryAsync(int lotteryId);

        /// <summary>
        /// delite all prizies for lottery with id = lottery id;
        /// </summary>
        /// <param name="lotteryId"></param>
        /// <returns></returns>
        Task DeleteAllPriziesForLottery(LotteryEntity lottery);

        /// <summary>
        /// update  prize with id = prizeId in lottery with id = lotteryId;
        /// </summary>
        /// <param name="lott">lottery id to update</param>
        void UpdatePrizeForLottery(LotteryPrizeList lott);

        /// <summary>
        /// delites prize from LotteryPrizeList
        /// </summary>
        /// <param name="lotteryId">id of target lottery</param>
        /// <param name="prizeId">id of target prize in LotteryPrizeList table</param>
        void DeletePrizeForLottery(int lotteryId, int prizeId);

        Task<List<LotteryEntity>> GetLotteriesForPrize(int prizeId);

        #endregion

    }
}
