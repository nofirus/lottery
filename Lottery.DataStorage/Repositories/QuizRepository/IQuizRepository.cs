﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Lottery.Models.Models;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryQuiz;

namespace Lottery.DataStorage.Repositories.QuizRepository
{
    public interface IQuizRepository
    {
        /// <summary>
        /// return Quiz objec for lottery
        /// </summary>
        /// <param name="lotteryId">target lottery id</param>
        /// <returns></returns>
        Task<Quiz> GetQuizForLottery(LotteryEntity lottery);

        /// <summary>
        /// return all Question
        /// </summary>
        /// <returns></returns>
        Task<List<Question>> GetAllQuestions();

        /// <summary>
        /// return all posible answer to the question;
        /// </summary>
        /// <param name="question">target question</param>
        /// <returns></returns>
        Task<List<QuestionAnswers>> GetAllAnswersOnTheQuestion(Question question);

        /// <summary>
        /// return Answer with id = id
        /// </summary>
        /// <param name="id">target answer id</param>
        /// <returns></returns>
        Task<Answer> GetAnswerById(int id);

        /// <summary>
        /// set to the lottery with id = quiz.LotteryId quiz with id = quiz.id
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns></returns>
        Task<Quiz> AddQuiz(Quiz quiz);

        /// <summary>
        /// add questions to quiz
        /// </summary>
        /// <param name="quizQuestionses"></param>
        /// <returns></returns>
        Task SetQuizQuestion(List<QuizQuestions> quizQuestionses);

        /// <summary>
        /// delete quiz
        /// </summary>
        /// <param name="quiz">target quiz</param>
        /// <returns></returns>
        Task DeleteQuiz(Quiz quiz);

        /// <summary>
        /// returns all question for quiz
        /// </summary>
        /// <param name="quiz">target quiz</param>
        /// <returns></returns>
        Task<List<Question>> GetAllQuestionForQuiz(Quiz quiz);

        /// <summary>
        /// return all answers on the question
        /// </summary>
        /// <param name="question">taget question</param>
        /// <returns></returns>
        Task<List<Answer>> GetAllAnswerOnTheQuestion(Question question);

        /// <summary>
        /// add 
        /// </summary>
        /// <param name="completedQuizzeses"></param>
        /// <returns></returns>
        Task AddQuizAnswers(List<CompletedQuizzes> completedQuizzeses);

        /// <summary>
        /// Add list of Answer object to database
        /// </summary>
        /// <param name="answers"></param>
        /// <returns></returns>
        Task<List<Answer>> AddAnswers(List<Answer> answers);

        Task<List<CompletedQuizzes>> GetComplitedQuizAnswer(Quiz quiz);

        Task<Quiz> GetQuizById(int quizId);

        Task<List<Answer>> GetAllAnswers();

        /// <summary>
        /// returns Answer object which corresponds to the target text;
        /// </summary>
        /// <param name="text">target text</param>
        /// <returns></returns>
        Task<Answer> GetAnswerByText(string text);

        Task AddAnswersOnQuestion(List<QuestionAnswers> questionAnswerses);

        Task<Question> AddQuestion(Question q);

        Task DeleteQuestion(int questionId);

        Task DeleteQuestionAnswers(int questionId);

        Task UpdateQuestion(Question question);

        Task<List<LotteryEntity>> GetAllLotteriesForQuestion(int questionId);

        Task<Question> GetQuestionById(int questionId);

        Task<List<QuestionAnswers>> GetAllAnswersOnTheQuestion(int questionId);

        Task<List<LotteryQuizzes>> GetAllLotteryQuizzes();

        Task<LotteryQuizzes> AddLotteryQuizzes(LotteryQuizzes lotteryQuizzes);

        Task UpdateQuiz(Quiz quiz);

        Task<List<LotteryEntity>> GetAllLotteryForQuiz(Quiz quiz);

        Task DeleteQuizQuestions(int quizId);

        Task<List<Quiz>> GetAllQuizzes();

        Task RemoveLotteryQuiz(int lotteryId, int quizId);
    }
}
