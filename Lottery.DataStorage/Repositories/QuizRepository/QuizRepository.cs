﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Lottery.DataStorage.Data;
using Lottery.Models.Models;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryQuiz;
using Microsoft.EntityFrameworkCore;

namespace Lottery.DataStorage.Repositories.QuizRepository
{
    public class QuizRepository:IQuizRepository
    {
        private readonly LotteryDbContext _lotteryDbContext;

        public QuizRepository(LotteryDbContext lotteryDbContext)
        {
            _lotteryDbContext = lotteryDbContext;
        }

        public Task<Quiz> GetQuizForLottery(LotteryEntity lottery)
        {
            var tcs = new TaskCompletionSource<Quiz>();
            var test = _lotteryDbContext.LotteryQuizzes.ToList();
            var qq = _lotteryDbContext.LotteryQuizzes.Include(x=>x.Quiz).FirstOrDefault(x => x.LotteryId.Equals(lottery.Id))?.Quiz;
            tcs.SetResult(qq);
            return tcs.Task;
        }

        public Task<List<Question>> GetAllQuestions()
        {
            return _lotteryDbContext.Questions.ToListAsync();
        }

        public Task<List<QuestionAnswers>> GetAllAnswersOnTheQuestion(Question question)
        {
            return _lotteryDbContext.QuestionAnswers.Where(a => a.Question.Equals(question)).ToListAsync();
        }

        public Task<Answer> GetAnswerById(int id)
        {
            return _lotteryDbContext.Answers.FindAsync(id);
        }

        public async Task<Quiz> AddQuiz(Quiz quiz)
        {
            var q = await _lotteryDbContext.Quizzes.AddAsync(quiz);
            await _lotteryDbContext.SaveChangesAsync();
            return q.Entity;
        }

        public async Task SetQuizQuestion(List<QuizQuestions> quizQuestionses)
        {
             _lotteryDbContext.AddRange(quizQuestionses);
             await _lotteryDbContext.SaveChangesAsync();
        }

        public async Task DeleteQuiz(Quiz quiz)
        {
            _lotteryDbContext.Remove(quiz);
            await _lotteryDbContext.SaveChangesAsync();
        }

        public async Task<List<Question>> GetAllQuestionForQuiz(Quiz quiz)
        {
            return await _lotteryDbContext.QuizQuestions.Where(q => q.Quiz.Equals(quiz)).Select(q=>q.Question).ToListAsync();
        }

        public async Task<List<Answer>> GetAllAnswerOnTheQuestion(Question question)
        {
            return await _lotteryDbContext.QuestionAnswers.Where(a => a.Question.Equals(question)).Select(a => a.Answer).ToListAsync();
        }

        public async Task AddQuizAnswers(List<CompletedQuizzes> completedQuizzeses)
        {
            await _lotteryDbContext.CompletedQuizzeses.AddRangeAsync(completedQuizzeses);
            await _lotteryDbContext.SaveChangesAsync();
        }

        public async Task<List<Answer>> AddAnswers(List<Answer> answer)
        {
            var addedAnswers = new List<Answer>();
            foreach (var ans in answer)
            {
                var addedAnswer = await _lotteryDbContext.Answers.AddAsync(ans);
                addedAnswers.Add(addedAnswer.Entity);
                await _lotteryDbContext.SaveChangesAsync();
            }

            return addedAnswers;
        }

        public Task<List<CompletedQuizzes>> GetComplitedQuizAnswer(Quiz quiz)
        {
            return _lotteryDbContext.CompletedQuizzeses.Include(x=>x.Quiz).Include(x=>x.ApplicationUser).Include(x=>x.Question).Where(x => x.Quiz.Equals(quiz)).ToListAsync();
        }

        public Task<Quiz> GetQuizById(int quizId)
        {
            return _lotteryDbContext.Quizzes.Include(x => x.LotteryQuizzes).Include(x => x.QuizQuestions).FirstOrDefaultAsync(x=>x.Id.Equals(quizId));
        }

        public async Task<List<Answer>> GetAllAnswers()
        {
            return await _lotteryDbContext.Answers.ToListAsync();
        }

        public Task<Answer> GetAnswerByText(string text)
        {
           return _lotteryDbContext.Answers.SingleOrDefaultAsync(x => x.Text.Equals(text));
        }

        public async Task AddAnswersOnQuestion(List<QuestionAnswers> questionAnswerses)
        {
            await _lotteryDbContext.QuestionAnswers.AddRangeAsync(questionAnswerses);
            await _lotteryDbContext.SaveChangesAsync();
        }

        public async Task<Question> AddQuestion(Question q)
        {
            var addQuestion = await _lotteryDbContext.Questions.AddAsync(q);
            await _lotteryDbContext.SaveChangesAsync();
            return addQuestion.Entity;
        }

        public Task DeleteQuestion(int questionId)
        {
            DeleteQuestionAnswers(questionId);
            var question = _lotteryDbContext.Questions.Find(questionId);
            _lotteryDbContext.Remove(question);
            _lotteryDbContext.SaveChanges();
            return Task.CompletedTask;
        }

        public Task DeleteQuestionAnswers(int questionId)
        {
           var answers = _lotteryDbContext.QuestionAnswers.Where(x => x.QuestionId.Equals(questionId));
            _lotteryDbContext.QuestionAnswers.RemoveRange(answers);
            _lotteryDbContext.SaveChanges();
            return Task.CompletedTask;
        }

        public Task UpdateQuestion(Question question)
        {
            _lotteryDbContext.Questions.Update(question);
            _lotteryDbContext.SaveChanges();
            return Task.CompletedTask;
        }

        public Task<List<LotteryEntity>> GetAllLotteriesForQuestion(int questionId)
        {
            var quizzes = _lotteryDbContext.QuizQuestions.Where(x => x.QuestionId.Equals(questionId)).Select(x=>x.Quiz).ToList();
            var allQuizzes = _lotteryDbContext.LotteryQuizzes.Include(x=>x.Lottery);
            var lotteries = new List<LotteryEntity>();

            foreach (var quiz in allQuizzes)
            {
                if (quizzes.Contains(quiz.Quiz))
                {
                    lotteries.Add(quiz.Lottery);
                }
            }
            var tcs = new TaskCompletionSource<List<LotteryEntity>>();
            tcs.SetResult(lotteries);
            return tcs.Task;
        }

        public Task<Question> GetQuestionById(int questionId)
        {
           return _lotteryDbContext.Questions.FindAsync(questionId);
        }

        public Task<List<QuestionAnswers>> GetAllAnswersOnTheQuestion(int questionId)
        {
            return _lotteryDbContext.QuestionAnswers.Include(x=>x.Question).Include(x=>x.Answer).Where(x => x.QuestionId.Equals(questionId)).ToListAsync();
        }

        public Task<List<LotteryQuizzes>> GetAllLotteryQuizzes()
        {
            return _lotteryDbContext.LotteryQuizzes.ToListAsync();
        }

        public async Task<LotteryQuizzes> AddLotteryQuizzes(LotteryQuizzes lotteryQuizzes)
        {
            var quiz = await _lotteryDbContext.LotteryQuizzes.AddAsync(lotteryQuizzes);
            await _lotteryDbContext.SaveChangesAsync();
            return quiz.Entity;
        }

        public Task UpdateQuiz(Quiz quiz)
        {
            _lotteryDbContext.Quizzes.Update(quiz);
            return _lotteryDbContext.SaveChangesAsync();
        }

        public Task<List<LotteryEntity>> GetAllLotteryForQuiz(Quiz quiz)
        {
           return _lotteryDbContext.LotteryQuizzes.Include(x => x.Lottery).Where(x => x.Quiz.Equals(quiz)).Select(x=>x.Lottery).ToListAsync();
        }

        public Task DeleteQuizQuestions(int quizId)
        {
            var allQuestions = _lotteryDbContext.QuizQuestions.Where(x => x.QuizId.Equals(quizId));
            _lotteryDbContext.QuizQuestions.RemoveRange(allQuestions);
            return _lotteryDbContext.SaveChangesAsync();
        }

        public Task<List<Quiz>> GetAllQuizzes()
        {
            return _lotteryDbContext.Quizzes.Include(x=>x.QuizQuestions).Include(x=>x.LotteryQuizzes).ToListAsync();
        }

        public async Task RemoveLotteryQuiz(int lotteryId, int quizId)
        {
            var lotteryQuiz =
               await _lotteryDbContext.LotteryQuizzes.FirstOrDefaultAsync(x =>
                    x.QuizId.Equals(quizId) && x.LotteryId.Equals(lotteryId));
            _lotteryDbContext.LotteryQuizzes.Remove(lotteryQuiz);
            await _lotteryDbContext.SaveChangesAsync();
        }
    }
}
