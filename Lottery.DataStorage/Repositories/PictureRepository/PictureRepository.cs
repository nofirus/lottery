﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lottery.DataStorage.Data;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.Picture;
using Microsoft.EntityFrameworkCore;

namespace Lottery.DataStorage.Repositories.PictureRepository
{
    public class PictureRepository:IPictureRepository
    {
        private readonly LotteryDbContext _lotteryDbContext;

        public PictureRepository(LotteryDbContext lotteryDbContext)
        {
            _lotteryDbContext = lotteryDbContext;
        }

        public async Task<List<Picture>> GetAllPictures()
        {
            return await _lotteryDbContext.Picture.ToListAsync();
        }

        public async Task<string> GetPicturePass(int pictureId)
        {
            var picture = await _lotteryDbContext.Picture.FindAsync(pictureId);
            return picture.Pass;
        }

        public async Task<Picture> GetPictureById(int pictureId)
        {
            return await _lotteryDbContext.Picture.FindAsync(pictureId);
        }
        public async Task SetImageToTheLotteryAsync(LotteryEntity lottery, Picture pic)
        {
            lottery.Picture = pic;
            _lotteryDbContext.Lotteries.Update(lottery);
            await _lotteryDbContext.SaveChangesAsync();
        }

        public async Task<Picture> GetLotteryPicture(int lotteryId)
        {
            var lottery = _lotteryDbContext.Lotteries.Include(x=>x.Picture).FirstOrDefault(x=>x.Id.Equals(lotteryId));
            if (lottery != null) return lottery.Picture;
            return new Picture();
        }

        public Task<Picture> AddPicture(Picture picture)
        {
            var pic = _lotteryDbContext.Picture.Add(picture);
            _lotteryDbContext.SaveChanges();
            var tcs = new TaskCompletionSource<Picture>();
            tcs.SetResult(pic.Entity);
            return tcs.Task;
        }

        public Task DeletePicture(Picture picture)
        {
            _lotteryDbContext.Picture.Remove(picture);
            return _lotteryDbContext.SaveChangesAsync();
        }

        public Task<List<LotteryEntity>> GetAllLotteriesForPicture(Picture picture)
        {
            return _lotteryDbContext.Lotteries.Where(x => x.Picture.Equals(picture)).ToListAsync();
        }
    }
}
