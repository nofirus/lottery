﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.Picture;

namespace Lottery.DataStorage.Repositories.PictureRepository
{
    public interface IPictureRepository
    {
        Task<List<Picture>> GetAllPictures();
        Task<string> GetPicturePass(int pictureId);
        Task<Picture> GetPictureById(int pictureId);
        /// <summary>
        /// set image with id = imageId to the lottery with id = lotteryId 
        /// </summary>
        /// <param name="lotteryId"></param>
        /// <param name="imageId"></param>
        /// <returns></returns>
        Task SetImageToTheLotteryAsync(LotteryEntity lottery, Picture pic);

        Task<Picture> GetLotteryPicture(int lotteryId);

        Task<Picture> AddPicture(Picture picture);

        Task DeletePicture(Picture picture);

        Task<List<LotteryEntity>> GetAllLotteriesForPicture(Picture picture);
    }
}
