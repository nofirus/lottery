﻿using System;
using System.Collections.Generic;
using System.Text;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryQuiz;

namespace Lottery.Models.Models.QuizViewModel
{
    public class QuizInfoViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public List<Question> Questions { get; set; }
        public List<LotteryEntity> Lotteries { get; set; }
    }
}
