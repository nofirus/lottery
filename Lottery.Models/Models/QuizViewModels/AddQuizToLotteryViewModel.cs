﻿using System;
using System.Collections.Generic;
using System.Text;
using Lottery.Models.Models.DataStorageModels.LotteryQuiz;
using Lottery.Models.Models.LotteryViewModels;

namespace Lottery.Models.Models.QuizViewModel
{
    public class AddQuizToLotteryViewModel
    {
        public int LotteryId { get; set; }
        public PaginatedList<Quiz> PaginatedQuizList { get; set; }
    }
}
