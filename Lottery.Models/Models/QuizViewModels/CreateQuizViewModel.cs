﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryQuiz;

namespace Lottery.Models.Models.QuizViewModel
{
    public class CreateQuizViewModel
    {
        public int Id { get; set; }
        public List<Question> AllQuestions { get; set; }
        [Required]
        public IEnumerable<int> SelectedQuestion { get; set; }
        public string Description { get; set; }
        public List<List<Answer>> AllAsnwers { get; set; }
        public int AmountOfQuestions { get; set; }
        public Quiz CurrentQuiz { get; set; }
        public List<LotteryQuizzes> AllQuizes { get; set; }
        public int SelectedQuizId { get; set; }
    }
}
