﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lottery.Models.Models.Excel
{
    public class ExcelDataSelectViewModel
    {
        public bool Selected { get; set; }
        public int LotteryId { get; set; }
    }
}
