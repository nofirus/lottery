﻿using System;
using System.Collections.Generic;
using System.Text;
using Lottery.Models.Models.DataStorageModels.Lottery;

namespace Lottery.Models.Models.LotteryViewModels
{
    public class LotteryViewModel
    {
        public List<LotteryEntity> FutureLottery { get; set; }
        public string StatusMessage { get; set; }}
}
