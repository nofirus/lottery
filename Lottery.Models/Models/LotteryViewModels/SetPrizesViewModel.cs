﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Lottery.Models.Models.DataStorageModels.Picture;
using Lottery.Models.Models.DataStorageModels.Prize;

namespace Lottery.Models.Models.LotteryViewModels
{
    public class SetPrizesViewModel
    {
        [Required]
        public int LotteryId { get; set; }

        public int AmountOfWinners { get; set; }
        
        public IEnumerable<LotteryPrize> CurrentGroupPrizeList { get; set; }
        public List<LotteryPrize> AllPrizies { get; set; }

        public int AmountOfPrizeGroups { get; set; }
        [Required]
        public IEnumerable<int> LeftGroupBorder { get; set; }
        [Required]
        public IEnumerable<int> RightGtoupBorder { get; set; }
        [Required]
        public IEnumerable<int> GroupPrizies { get; set; }

        public string StatusMessage { get; set; }
    }
}
