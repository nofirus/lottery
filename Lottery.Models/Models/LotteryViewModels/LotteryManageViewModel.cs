﻿using System;
using System.Collections.Generic;
using System.Text;
using Lottery.Models.Models.DataStorageModels.Lottery;

namespace Lottery.Models.Models.LotteryViewModels
{
    public class LotteryManageViewModel
    {
        public List<LotteryEntity> AllLotteries { get; set; }
        public PaginatedList<LotteryEntity> PaginatedList { get; set; }

        public LotteryManageViewModel(List<LotteryEntity> allLotteries)
        {
            AllLotteries = allLotteries;
        }
    }
}
