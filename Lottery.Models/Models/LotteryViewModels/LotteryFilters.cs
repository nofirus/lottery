﻿using System;
using System.Collections.Generic;
using System.Text;
using Lottery.Models.Models.DataStorageModels.Lottery;

namespace Lottery.Models.Models.LotteryViewModels
{
    public class LotteryFilters
    {
        public int Page { get; set; }

        public DateTime LeftDate { get; set; }
        public DateTime RightDate { get; set; }
        public int AmountOfWinnersMin { get; set; }
        public int AmountOfWinnersMax { get; set; }
        public int PageSize { get; set; }

        public PaginatedList<LotteryEntity> PaginatedList { get; set; }
    }
}
