﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lottery.Models.Models.LotteryViewModels
{
    public class CustomAnswers
    {
        public int QuestionId;
        public string AnswerText;
    }
}
