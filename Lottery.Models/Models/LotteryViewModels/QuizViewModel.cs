﻿using System;
using System.Collections.Generic;
using System.Text;
using Lottery.Models.Models.DataStorageModels.LotteryQuiz;

namespace Lottery.Models.Models.LotteryViewModels
{
    public class QuizViewModel
    {
        public Quiz Quiz { get; set; }
        public int QuizId { get; set; }
        public int LotteryId { get; set; }
        public IEnumerable<Question> QuizQuestionses { get; set; }
        public IEnumerable<int> QuizQuestionsesId { get; set; }
        public List<List<Answer>> AnswersToQuestion { get; set; }
        public IEnumerable<int> Answers { get; set; }
        public IEnumerable<string> CustomAnswers { get; set; }
        public IEnumerable<CustomAnswers> CAnswers { get; set; }

    }
}
