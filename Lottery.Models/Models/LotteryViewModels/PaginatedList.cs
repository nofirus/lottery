﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PagedList.Core;

namespace Lottery.Models.Models.LotteryViewModels
{
    public class PaginatedList<T>
    {
        public int PageNumber { get; private set; }
        public int PageSize { get; set; }
        public int TotalPages { get; private set; }
        public IQueryable<T> ItemList { get; set; }

        public PaginatedList(IQueryable<T> items, int count, int pageNumber, int pageSize)
        {
            PageNumber = pageNumber;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
            ItemList = items;
            PageSize = pageSize;
        }

        public bool HasPreviousPage => (PageNumber > 1);

        public bool HasNextPage => (PageNumber < TotalPages);

        public IPagedList<T> GetPaginatedList()
        {
            return ItemList.AsQueryable().ToPagedList(PageNumber, PageSize);
        }
    }
}
