﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Lottery.Models.Models.DataStorageModels.Picture;
using Lottery.Models.Models.DataStorageModels.Prize;
using Lottery.Models.Models.DataStorageModels.Types;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;


namespace Lottery.Models.Models.LotteryViewModels
{
    public class LotteryCreatingViewModel
    {

        public int Id { get; set; }
        [Required]
        public LotteryType LotteryType { get; set; }
        [Required]
        [Range(0,int.MaxValue)]
        public int AmountOfWinners { get; set; }
        public int TestId { get; set; }
        [Required]
        public int PictureId { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime RegistrationEndDate { get; set; }
        [Required]
        public DateTime LotteryDate { get; set; }
        [Required]
        public OutputType OutputType { get; set; }
        [Required]
        public LotteryKind LotteryKind { get; set; }
        public int Delay { get; set; }

        public string StatusMessage { get; set; }
        
    }
}
