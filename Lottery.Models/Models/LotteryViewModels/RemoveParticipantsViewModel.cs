﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Lottery.Models.Models.DataStorageModels;
using Lottery.Models.Models.DataStorageModels.LotteryParticipants;
using Lottery.Models.Models.DataStorageModels.User;

namespace Lottery.Models.Models.LotteryViewModels
{
    public class RemoveParticipantsViewModel
    {
        public PaginatedList<ApplicationUser> Participants { get; set; }
        public List<ExcludedFromTheLotteryUsers> ExcludedParticapnts { get; set; }
        [Required]
        public int LotteryId { get; set; }
        [Required]
        public string RemoveReason { get; set; }
        [Required]
        public string Email { get; set; }
        public int PageNumer { get; set; }
        public int PageSize { get; set; }
        
    }
}
