﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryParticipants;

namespace Lottery.Models.Models.LotteryViewModels
{
    public class LotteryListViewModel
    {
        public LotteryEntity ClosestLottery { get; set; }
        public PaginatedList<LotteryEntity> PaginadeFutureLotteryList { get; set; }
        public LotteryEntity LastHeldLottery { get; set; }
        public PaginatedList<LotteryParticipants> ClosestLotteryParticipants { get; set; }
        public string StatusMessage { get; set; }
        public LotteryFilters Filter { get; set; }


        public LotteryListViewModel(LotteryEntity closestLottery, PaginatedList<LotteryEntity> paginadeFutureLotteryList, LotteryEntity lastHeldLottery, 
            PaginatedList<LotteryParticipants> closestLotteryParticipants, string statusMessage)
        {
            ClosestLottery = closestLottery;
            PaginadeFutureLotteryList = paginadeFutureLotteryList;
            LastHeldLottery = lastHeldLottery;
            ClosestLotteryParticipants = closestLotteryParticipants;
            StatusMessage = statusMessage;
        }
    }
}
