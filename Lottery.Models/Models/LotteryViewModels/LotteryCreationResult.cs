﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lottery.Models.Models.LotteryViewModels
{
    public class LotteryCreationResult
    {
        public bool Succsess { get; set; }
        public string Message { get; set; }
    }
}
