﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Lottery.Models.Models.DataStorageModels.Picture;

namespace Lottery.Models.Models.LotteryViewModels
{
    public class SetLotteryImageViewModel
    {
        [Required]
        public int LotteryId { get; set; }

        public Picture Picture { get; set; }
        public int PictureId { get; set; }

        public List<Picture> AllPictures { get; set; }
    }
}
