﻿using System;
using System.Collections.Generic;
using System.Text;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryParticipants;
using Lottery.Models.Models.DataStorageModels.LotteryWiner;
using Lottery.Models.Models.DataStorageModels.Prize;
using Lottery.Models.Models.DataStorageModels.User;

namespace Lottery.Models.Models.LotteryViewModels
{
    public class LotteryResultViewModel
    {
        public LotteryEntity Lottery { get; set; }
        public LotteryPrize Prize { get; set; }
        public bool IsUserWinner { get; set; }
        public int Place { get; set; }
        public List<LotteryParticipants> LotteryParticipants { get; set; }
        public List<LotteryWinners> LotteryWinners { get; set; }
    }
}
