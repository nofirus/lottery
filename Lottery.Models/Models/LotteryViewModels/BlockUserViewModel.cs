﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Lottery.Models.Models.LotteryViewModels
{
    public class BlockUserViewModel
    {
        [Required]
        public string UserEmail { get; set; }
        [Required]
        public string BlockReason { get; set; }

        public DateTimeOffset LockoutEnd { get; set; }
    }
}
