﻿using System;
using System.Collections.Generic;
using System.Text;
using Lottery.Models.Models.DataStorageModels.Lottery;

namespace Lottery.Models.Models.LotteryViewModels
{
    public class ClosestLotteryViewModel
    {
        public LotteryEntity Lottery { get; set; }
    }
}
