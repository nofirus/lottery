﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Lottery.Models.Models.PrizeViewModels
{
    public class CreatePrizeViewModel
    {
        [Required]
        public string Description { get; set; }
        public int Id { get; set; }
    }
}
