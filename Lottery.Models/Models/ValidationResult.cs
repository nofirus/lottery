﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lottery.Models.Models
{
    public class ValidationResult
    {
        public string Description { get; set; }
        public bool Success { get; set; }
    }
}
