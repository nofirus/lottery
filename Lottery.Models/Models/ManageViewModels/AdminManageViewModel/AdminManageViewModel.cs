﻿using System.Collections.Generic;

namespace Lottery.Models.Models.ManageViewModels.AdminManageViewModel
{
    public class AdminManageViewModel
    {
        public AdminManageViewModel(List<UserInfoViewModel> userList)
        {
            UserList = userList;
        }

        public  List<UserInfoViewModel> UserList { get; set; }

    }
}
