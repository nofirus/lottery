﻿using System;
using Lottery.Models.Models.DataStorageModels.Roles;

namespace Lottery.Models.Models.ManageViewModels.AdminManageViewModel
{
    public class ApplicationUserViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Address { get; set; }
        public UserRole Role { get; set; }
    }
}
