﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Lottery.Models.Models.DataStorageModels.Roles;

namespace Lottery.Models.Models.ManageViewModels
{
    public class UserInfoViewModel
    {

        public string Id;

        public string UserName { get; set; }

        public  List<UserRole> Roles { get; set; }

        public DateTimeOffset LockoutEnd { get; set; }

        public string BlockReason { get; set; }

        public bool IsEmailConfirmed { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [RegularExpression("[a-zA-Z]{1,}")]
        [StringLength(maximumLength: 50)]
        public string Name { get; set; }

        [Required]
        [RegularExpression("[a-zA-Z]{1,}")]
        [StringLength(maximumLength: 50)]
        public string Surname { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        [Required]
        public string Address { get; set; }

        [Phone]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }

        public string StatusMessage { get; set; }

    }
}
