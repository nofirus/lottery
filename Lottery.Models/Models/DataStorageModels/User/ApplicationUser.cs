﻿using System;
using System.Collections.Generic;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.LotteryWiner;
using Lottery.Models.Models.DataStorageModels.Roles;
using Microsoft.AspNetCore.Identity;

namespace Lottery.Models.Models.DataStorageModels.User
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Address { get; set; }
        public UserRole Role { get; set; }
        public string BlockReason { get; set; }
        public string OldPassword { get; set; }
        public string OldPassword2 { get; set; }
        public bool TwoFactorEmailEnabled { get; set; }
        public List<LotteryUsers> LotteryUsers { get; set; }
        public List<LotteryWinners> LotteryWiners { get; set; }
    }
}
