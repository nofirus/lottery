﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Lottery.Models.Models.DataStorageModels.LotteryQuiz
{
    public class Answer
    {
        [Key]
        public int Id { get; set; }
        public string Text { get; set; }
        public List<QuestionAnswers> QuestionAnswers { get; set; }
    }
}
