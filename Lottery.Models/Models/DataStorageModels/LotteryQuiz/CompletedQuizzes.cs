﻿using System.ComponentModel.DataAnnotations;
using Lottery.Models.Models.DataStorageModels.User;

namespace Lottery.Models.Models.DataStorageModels.LotteryQuiz
{
    public class CompletedQuizzes
    {
        [Key]
        public int Id { get; set; }
        public int QuizId { get; set; }
        public LotteryQuiz.Quiz Quiz { get; set; }
        public int QuestionId { get; set; }
        public Question Question { get; set; }
        public string AnswerText { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        public override string ToString()
        {
            return Question.Text + ":" + AnswerText;
        }
    }
}
