﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Lottery.Models.Models.DataStorageModels.Lottery;

namespace Lottery.Models.Models.DataStorageModels.LotteryQuiz
{
    public class Quiz
    {
        [Key]
        public int Id { get; set; }
        public string Description { get; set; }
        public List<LotteryQuizzes> LotteryQuizzes { get; set; }
        public List<QuizQuestions> QuizQuestions { get; set; }
    }
}
