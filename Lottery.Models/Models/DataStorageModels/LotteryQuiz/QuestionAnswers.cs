﻿using System.ComponentModel.DataAnnotations;

namespace Lottery.Models.Models.DataStorageModels.LotteryQuiz
{
    public class QuestionAnswers
    {
        [Key]
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public Question Question { get; set; }
        public int AnswerId { get; set; }
        public Answer Answer { get; set; }
    }
}
