﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Lottery.Models.Models.DataStorageModels.LotteryQuiz
{
    public class Question
    {
        [Key]
        public int Id { get; set; }
        public string Text { get; set; }
        public QuestionType Type { get; set; }
        public List<QuizQuestions> QuizQuestions { get; set; }
        public List<QuestionAnswers> QuestionAnswers { get; set; }
    }
}
