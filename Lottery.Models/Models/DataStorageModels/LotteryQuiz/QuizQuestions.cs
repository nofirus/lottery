﻿using System.ComponentModel.DataAnnotations;

namespace Lottery.Models.Models.DataStorageModels.LotteryQuiz
{
    public class QuizQuestions
    {
        [Key]
        public int Id { get; set; }

        public int QuizId { get; set; }
        public LotteryQuiz.Quiz Quiz { get; set; }

        public int QuestionId { get; set; }
        public Question Question { get; set; }
    }
}
