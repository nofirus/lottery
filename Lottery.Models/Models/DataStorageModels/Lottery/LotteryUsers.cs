﻿using System.ComponentModel.DataAnnotations;
using Lottery.Models.Models.DataStorageModels.User;

namespace Lottery.Models.Models.DataStorageModels.Lottery
{
    public class LotteryUsers
    {
        [Key]
        public int Id { get; set; }
        public int LotteryId { get; set; }
        public LotteryEntity Lottery { get; set; }
        public string UserId { get; set; }
        public ApplicationUser ApplicationUser {get;set;}
    }
}
