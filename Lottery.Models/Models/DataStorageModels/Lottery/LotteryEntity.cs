﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Lottery.Models.Models.DataStorageModels.LotteryWiner;
using Lottery.Models.Models.DataStorageModels.Prize;
using Lottery.Models.Models.DataStorageModels.Types;

namespace Lottery.Models.Models.DataStorageModels.Lottery
{
    public class LotteryEntity
    {
        [Key]
        public int Id { get; set; }
        public LotteryType LotteryType { get; set; }
        public int AmountOfWinners { get; set; }
        public int AmountOfParticipants { get; set; }
        public Picture.Picture Picture { get; set; }
        public string Description { get; set; }
        public DateTime RegistrationEndDate { get; set; }
        public DateTime LotteryDate { get; set; }
        public OutputType OutputType { get; set; }
        public int Delay { get; set; }
        public int AmountOfPrizeGroups { get; set; }
        public LotteryKind LotteryKind { get; set; }
        [NotMapped]
        public bool Selected { get; set; }
        public List<LotteryUsers> LotteryUsers { get; set; }
        public List<LotteryPrizeList> LotteryPrizies { get; set; }
        public List<LotteryWinners> LotteryWiners { get; set; }
        public List<LotteryQuizzes> LotteryQuizzes { get; set; }

       
    }
}
