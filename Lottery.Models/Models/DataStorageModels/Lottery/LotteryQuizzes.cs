﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Lottery.Models.Models.DataStorageModels.Lottery
{
    public class LotteryQuizzes
    {
        [Key]
        public int Id { get; set; }
        public int LotteryId { get; set; }
        public LotteryEntity Lottery { get; set; }
        public int QuizId { get; set; }
        public LotteryQuiz.Quiz Quiz { get; set; }
    }
}
