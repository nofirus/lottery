﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.User;

namespace Lottery.Models.Models.DataStorageModels
{
    public class ExcludedFromTheLotteryUsers
    {
        [Key]
        public int Id { get; set; }

        //public int LotteryId { get; set; }
        public LotteryEntity Lottery { get; set; }

        //public int UserId { get; set; }
        public ApplicationUser User { get; set; }

        public string RemoveReason { get; set; }
    }
}
