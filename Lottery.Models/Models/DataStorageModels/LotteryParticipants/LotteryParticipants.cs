﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.User;

namespace Lottery.Models.Models.DataStorageModels.LotteryParticipants
{
    public class LotteryParticipants
    {
        [Key]
        public int Id { get; set; }
        public LotteryEntity Lottery { get; set; }
        public ApplicationUser Participants { get; set; }
    }
}
