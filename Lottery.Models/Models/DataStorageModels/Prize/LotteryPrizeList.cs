﻿using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.User;

namespace Lottery.Models.Models.DataStorageModels.Prize
{
    public class LotteryPrizeList
    {
        public int Id { get; set; }
        public int PrizeId { get; set; }
        public LotteryPrize Prize { get; set; }
        public int LotteryId { get; set; }
        public LotteryEntity Lottery { get; set; }
        public int Place { get; set; }
    }
}
