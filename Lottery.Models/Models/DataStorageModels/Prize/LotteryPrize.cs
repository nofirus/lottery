﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Lottery.Models.Models.DataStorageModels.Prize
{
    public class LotteryPrize
    {
        [Key]
        public int Id { get; set; }
        public string Description { get; set; }
        public ICollection<LotteryPrizeList> LotteryPrizies { get; set; }
    }
}
