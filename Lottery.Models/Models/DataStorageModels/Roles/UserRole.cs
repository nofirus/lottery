﻿using Microsoft.AspNetCore.Identity;

namespace Lottery.Models.Models.DataStorageModels.Roles
{
    public class UserRole: IdentityRole
    {
        public UserRole(string name) : base(name) { }
        public string Description { get; set; }

        public UserRole()
        {

        }
    }
}
