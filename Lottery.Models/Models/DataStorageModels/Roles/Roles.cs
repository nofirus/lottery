﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lottery.Models.Models.DataStorageModels.Roles
{
    public enum Roles
    {
        Admin,
        Manager,
        User
    }
}
