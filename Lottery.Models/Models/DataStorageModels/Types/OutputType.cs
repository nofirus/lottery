﻿namespace Lottery.Models.Models.DataStorageModels.Types
{
    public enum OutputType
    {
        AllWinners,
        IndividualWinners
    }
}
