﻿using System;
using System.ComponentModel.DataAnnotations;
using Lottery.Models.Models.DataStorageModels.Lottery;
using Lottery.Models.Models.DataStorageModels.User;

namespace Lottery.Models.Models.DataStorageModels.LotteryWiner
{
    public class LotteryWinners
    {
        [Key]
        public int Id { get; set; }
        public int LotteryId { get; set; }
        public LotteryEntity Lottery { get; set; }  
        public string UserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public int Place { get; set; }
        public bool Equals(LotteryWinners other)
        {
            return LotteryId.Equals(other.LotteryId) && UserId.Equals(other.UserId);
        }
    }
}
