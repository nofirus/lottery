﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Lottery.Models.Models.DataStorageModels.LotteryQuiz;

namespace Lottery.Models.Models.QuestionViewModel
{
    public class QuestionCreateViewModel
    {
        
        public int Id { get; set; }
        public int AmountOfAnswers { get; set; }
        [Required]
        public QuestionType QuestionType { get; set; }

        public IEnumerable<string> Answers { get; set; }

        [Required]
        public string Question { get; set; }
    }
}
